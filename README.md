# tuxBoard

This is tuxBoard, a simple drawing and writing application for Linux (or other OS) to be used in combination with pen tablets such as those from Wacom (I personally use a Wacom Intuos M and this is the only pen tablet I have tested this software with so far).

Very often for my work I need a tool for simple drawing on a board. A valid alternative in Linux is Xournal, but I wouldn't always find it as easy to use as Microsoft Whiteboard. So, inspired by the latter, I decided to create this Linux alternative.

It offers an infinite drawing board (this was a feature I wouldn't have in Xournal), and it currently offers configurable pens, eraser and selector for move and resize operation. In order to increase performance, drawing on screen is powered by OpenGL 3.2.

Also, it can save boards into a software specific format or export it as SVG or PDF (althogh these functionalities still need improvement).

This software can be compiled against Qt 5.15 or Qt 6.1, thus it could be compiled also under Windows or MacOs. I don't have a particular need to do so, but if someone does, I would appreciate knowing if it works properly. In the release folder, the AppImages and deb package per each version are loaded. I hope to be able to provide a rpm package as well in the future.

I hope someone will want to test it and use it and feedbacks for improvement are very welcome.

Last but not least, the tuxBoard logo is gently provided by Sara Schäfer. Thank you very much :)


**Compile Instructions**

In order to compile tuxBoard, you can choose between Qt5 and Qt6. Following, the instruction for compiling in Arch (thanks to Moreno Razzoli, a.k.a. Morrolinux), in case Qt6 is chosen.


_Install Qt 6_

`sudo pacman -S qt6`


_Compile tuxBoard_

`cd src && mkdir build && cd build`

`qmake6 ../tuxBoard.pro -spec linux-g++`

`make -j $(nproc)`



**Features**

tuxBoard allows hand drawing over an "infinite" board by means of a pen tablet (such as Wacom devices). It can handle multiple boards that can be opened with the shortcut **Ctrl+N**. 

Each board consists of a drawing board and a toolbox.

The toolbox allows selecting the tool to be used (a pen, the eraser or the select tool). The toolbox is typically hidden during hand drawing and can be highlighted by moving the cursor (with mouse or pen) over the little rectangle at the bottom center of the board.
By right clicking on a pen, its colour can be changed. Also, when clicking (via mouse or pen) on a pen tool or on the eraser tool and dragging up and down will change the size of the respective tool. Also, a popup text will appear at the upper side of the board indicating the currently selected size. It is also possible to move the position of each tool according to the user's taste through the context menu that can be activated on the toolbox with the mouse right click.
It is possible to add new user defined pens, or removing those that are not needed. The main idea was to make the toolbox as customizable by the user as possible to satisfy everybody's needs. 

The mouse operation on the board are limited to shift and zoom. By clicking and dragging the board will move, while using **Ctrl + Mouse wheel** will zoom in and out and a popup text will show the current zoom. Also, only **Mouse wheel** will move the board vertically, **Shift + Mouse wheel** will move it horizontally. On laptops, the two fingers **touchpad scrolling** will allow moving in any direction.

The tablet pen, instead, can perform all operations.
According to the selected tool, drawing, erasing and selecting/moving/resizing can be easily accomplished.

**Shift + Pen**: moving the board

**Ctrl + Pen**: zooming the board

**Ctrl + Shift + Pen**: restore to initial view

When the eraser is the active tool, **Ctrl + Meta + Eraser** deletes the whole line instead of points.

The context menu of the board (mouse right click) will allow the user to save the board (and load) to (from) file. Also, exporting to SVG and PDF formats are enabled. Moreover, a preference dialog is available to define the user configuration. This configuration can be loaded on startup by saving the preferences to a file "tuxboard.conf" to be located in the tuxBoard executable directory.

Some other shortcuts are available:

**Ctrl + Z**: undo

**Ctrl + Shift + Z**: redo

**Ctrl + G**: toggles the grid

**Ctrl + P**: toggles the application of pen pressure

**Ctrl + F**: toggle fullscreen

**Del**: deletes the selected area
