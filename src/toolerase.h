/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#ifndef TOOLERASE_H
#define TOOLERASE_H

#include <QObject>
#include <QPainter>
#include <QPainterPath>
#include <QRect>
#include <QTimer>
#include <QVariant>

#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>

#include <QMatrix4x4>
#include <QVector4D>

#include <QDebug>

#include "toolobject.h"
#include "common.h"

class toolErase : public toolobject
{
    Q_OBJECT
public:
    toolErase(board_properties p);

    void paint(QPainter &painter, QPoint topleft) override;
    void paintGL(QOpenGLFunctions *f, QRect screen, QPoint topleft, float devicePixelRatio) override;
    QRect get_tool_rect() override { return toolRect; };

    tools getToolType() override { return ERASE; }
    QVariant getData() override { QVariant v("Eraser"); return v; }
};

#endif // TOOLERASE_H
