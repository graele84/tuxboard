/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#ifndef TOOLOBJECT_H
#define TOOLOBJECT_H

#include <QObject>
#include <QPainter>
#include <QPainterPath>
#include <QRect>
#include <QTimer>
#include <QVariant>

#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>

#include <QDebug>

#include "common.h"

typedef enum
{
    NORMAL = 0x00,
    HIGHLIGHTED = 0x01,
    INCREASING = 0x02,
    DECREASING = 0x03
} tool_status;

class toolobject : public QObject
{
    Q_OBJECT
public:
    explicit toolobject(board_properties prop, QObject *parent = nullptr);

    void initializeGL();
    virtual void paint(QPainter &painter, QPoint topleft) { Q_UNUSED(painter); Q_UNUSED(topleft); };
    virtual void paintGL(QOpenGLFunctions *f, QRect screen, QPoint topleft, float devicePixelRatio) { Q_UNUSED(f); Q_UNUSED(screen); Q_UNUSED(topleft); Q_UNUSED(devicePixelRatio); }
    virtual QRect get_tool_rect() { return toolRect; }

    void setProperties(board_properties p);

    void set_rect(QRect r) { toolRect = r; }
    void set_color(QColor col) { color = col; }

    bool isSelected() { return selected; }
    void select() { selected = true; color.setAlphaF(1.0f); emit updated(); }
    void deselect() { selected = false; color.setAlphaF(0.3f); emit updated(); }

    virtual tools getToolType() { return PEN; };
    virtual QVariant getData() { QVariant v("Empty"); return v; };

    void setMinObjectSize(int v) { object_min_size = v; }
    void setMaxObjectSize(int v) { object_max_size = v; }
    void setObjectSize(int v);

    int getObjectSize() { return object_size; }

    void highlight();
    void normal();

signals:
    void updated();

private slots:
    void animationTriggered();

protected:
    board_properties properties;
    QRect toolRect;
    QColor color;

    bool selected;

    bool gl_initialized;

    tool_status status;

    QTimer animationTimer;
    float animationValue;  //from 0.0 to 1.0
    float xm1, x;

    float highlighted_scale;  //typically higher than 1.0
    QSize normalSize;

    int object_min_size;
    int object_max_size;
    int object_size;

/*    QOpenGLShaderProgram *m_circleProgram;
    QOpenGLVertexArrayObject *m_circleVao;
    QOpenGLBuffer *m_circle_Vbo;
    int m_circle_in_color_loc;
    int m_circle_line_size_loc;
    int m_circle_transfMatrix_loc;
    int m_circle_pixelratio_loc;

    QOpenGLShaderProgram *m_rectProgram;
    QOpenGLVertexArrayObject *m_rectVao;
    QOpenGLBuffer *m_line_x_Vbo;
    int m_rect_in_color_loc;
    int m_rect_line_size_loc;
    int m_rect_transfMatrix_loc;
    int m_rect_pixelratio_loc;*/

    QOpenGLShaderProgram *m_rectESProgram;
    QOpenGLVertexArrayObject *m_rectESVao;
    QOpenGLBuffer *m_rect_ES_x_Vbo;
    QOpenGLBuffer *m_rect_ES_y_Vbo;
    int m_rect_ES_in_color_loc;
    int m_rect_ES_in_params_loc;
    int m_rect_ES_transfMatrix_loc;
    int m_rect_ES_pixelratio_loc;
    int m_rect_ES_ext_points_loc;

    QOpenGLShaderProgram *m_circleESProgram;
    QOpenGLVertexArrayObject *m_circleESVao;
    QOpenGLBuffer *m_circle_ES_x_Vbo;
    QOpenGLBuffer *m_circle_ES_y_Vbo;
    int m_circle_ES_in_color_loc;
    int m_circle_ES_in_params_loc;
    int m_circle_ES_transfMatrix_loc;
    int m_circle_ES_pixelratio_loc;
    int m_circle_ES_ext_points_loc;

    void renderRectGL(QOpenGLFunctions *f, QRect r, QColor col, int line_size, int radius, QRect screen, float devicePixelRatio);
    void renderCircleGL(QOpenGLFunctions *f, QRect r, QColor col, int line_size, QRect screen, float devicePixelRatio, float shade);
    QRect checkQRect(QRect r);

    void update_animation(int ms);

};

#endif // TOOLOBJECT_H
