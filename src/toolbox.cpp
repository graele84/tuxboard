/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#include "toolbox.h"

toolBox::toolBox(board_properties prop)
{
    properties = prop;
    status = HIDDEN;

    toolsRect.setTopLeft(QPoint(0,0)); toolsRect.setBottomRight(toolsRect.topLeft());
    add_pen(Qt::black);
    add_pen(Qt::red);
    add_pen(Qt::green);
    add_pen(Qt::blue);
    add_pen(Qt::yellow);

    add_eraser();

    add_select();

    pens[0].tool->select();
    for (int i = 1; i < pens.count(); i++)
        pens[i].tool->deselect();

    dragging = false;

    animationTimer.setInterval(16);
    connect(&animationTimer, &QTimer::timeout, this, &toolBox::animationTriggered);
    animationValue = 0.0f;  //it's hidden
    x = 0.0f; xm1 = 0.0f;

    if (status == HIDDEN)
        toolsRect.setHeight(properties.hidden_height);

    addPenAct = new QAction;
    addPenAct->setText("Add pen...");
    connect(addPenAct, &QAction::triggered, this, &toolBox::addPenTriggered);

    remPenAct = new QAction;
    remPenAct->setText("Remove pen...");
    connect(remPenAct, &QAction::triggered, this, &toolBox::remPenTriggered);

    changeToolAct = new QAction;
    changeToolAct->setText("Move tool position");
    connect(changeToolAct, &QAction::triggered, this, &toolBox::changeToolPositionTriggered);

    changeColAct = new QAction;
    changeColAct->setText("Change pen's color");
    connect(changeColAct, &QAction::triggered, this, &toolBox::changeColTriggered);

    popup = new QMenu;
    popup->addAction(addPenAct);
    popup->addAction(remPenAct);
    popup->addSeparator();
    popup->addAction(changeColAct);
    popup->addAction(changeToolAct);

    gl_initialized = false;
}

void toolBox::initializeGL()
{
    m_rectESProgram = new QOpenGLShaderProgram;
    if (m_rectESProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/Shaders/Rect_ES.vsh") == false)
        qDebug("Error compiling Vertex Shader");
    if (m_rectESProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/Shaders/Rect_ES.fsh") == false)
        qDebug("Error compiling Fragment Shader");
    if (m_rectESProgram->link() == false)
        qDebug("Shaders were not linked correctly");
    if (m_rectESProgram->bind() == false)
        qDebug("Shaders not correctly bound");
    m_rect_ES_in_color_loc = m_rectESProgram->uniformLocation("in_color");
    m_rect_ES_in_params_loc = m_rectESProgram->uniformLocation("in_params");
    m_rect_ES_transfMatrix_loc = m_rectESProgram->uniformLocation("transf");
    m_rect_ES_pixelratio_loc = m_rectESProgram->uniformLocation("pixelratio");
    m_rect_ES_ext_points_loc = m_rectESProgram->uniformLocation("ext_points");

    m_rectESVao = new QOpenGLVertexArrayObject;
    if (m_rectESVao->create() == false)
        qDebug("VAO error!\n");

    m_rect_ES_x_Vbo = new QOpenGLBuffer;
    m_rect_ES_x_Vbo->create();
    m_rect_ES_y_Vbo = new QOpenGLBuffer;
    m_rect_ES_y_Vbo->create();

    m_rectESVao->release();
    m_rectESProgram->release();
    /*
    m_rectProgram = new QOpenGLShaderProgram;
    if (m_rectProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/Shaders/Rect.vsh") == false)
        qDebug("Error compiling Vertex Shader");
    if (m_rectProgram->addShaderFromSourceFile(QOpenGLShader::Geometry, ":/shaders/Shaders/Rect.gsh") == false)
        qDebug("Error compiling Geometry Shader");
    if (m_rectProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/Shaders/Rect.fsh") == false)
        qDebug("Error compiling Fragment Shader");

    if (m_rectProgram->link() == false)
        qDebug("Shaders were not linked correctly");
    if (m_rectProgram->bind() == false)
        qDebug("Shaders not correctly bound");

    m_rect_in_color_loc = m_rectProgram->uniformLocation("in_color");
    m_rect_line_size_loc = m_rectProgram->uniformLocation("in_params");
    m_rect_transfMatrix_loc = m_rectProgram->uniformLocation("transf");
    m_rect_pixelratio_loc = m_rectProgram->uniformLocation("pixelratio");

    m_rectVao = new QOpenGLVertexArrayObject;
    if (m_rectVao->create() == false)
        qDebug("VAO error!\n");

    m_line_x_Vbo = new QOpenGLBuffer;
    m_line_x_Vbo->create();

    m_rectVao->release();
    m_rectProgram->release();*/

    for (int i = 0; i < pens.count(); i++)
        if (pens[i].type == ERASE)
            pens[i].tool->initializeGL();
}

void toolBox::paint(QPainter &painter, QRect rect)
{    
    if (status == HIDDEN)
    {
        toolsRect.setHeight(properties.hidden_height);
        calculateRectToParent(rect);
        QPainterPath path;
        path.addRoundedRect(toolsRectToParent, 10, 10);
        painter.setPen(Qt::NoPen);
        painter.setBrush(properties.hiddenColor);
        painter.drawPath(path);
        return;
    }

    toolsRect.setHeight(properties.full_height);
    calculateRectToParent(rect);
    for (int i = 0; i < pens.count(); i++)
    {
        QPoint p;
        p = toolsRectToParent.topLeft();
        QRect pen_rect = pens[i].tool->get_tool_rect();
        p.setY(p.y() + toolsRectToParent.height() - pen_rect.height());
        //apply the animation value by changing the y coordinate of top left
        p.setY(p.y() + (1.0f - animationValue) * toolsRectToParent.height());
        p.setX(p.x() + pens[i].x_pos);
//        pens[i].pen->set_color(pens[i].col);
        pens[i].tool->paint(painter, p);
        calculateRect();
    }
}

void toolBox::paintGL(QOpenGLFunctions *f, QRect rect, float devicePixelRatio)
{
    if (gl_initialized == false)
    {
        initializeGL();
        gl_initialized = true;
    }
    if (status == HIDDEN)
    {
        toolsRect.setHeight(properties.hidden_height);
        calculateRectToParent(rect);
        renderRectGL(f, toolsRectToParent, properties.hiddenColor, -1, 10, rect, devicePixelRatio);
        return;
    }
    toolsRect.setHeight(properties.full_height);
    calculateRectToParent(rect);
    for (int i = 0; i < pens.count(); i++)
    {
        QPoint p;
        p = toolsRectToParent.topLeft();
        QRect pen_rect = pens[i].tool->get_tool_rect();
        p.setY(p.y() + toolsRectToParent.height() - pen_rect.height());
        //apply the animation value by changing the y coordinate of top left
        p.setY(p.y() + (1.0f - animationValue) * toolsRectToParent.height());
        p.setX(p.x() + pens[i].x_pos);
        pens[i].tool->paintGL(f, rect, p, devicePixelRatio);
        calculateRect();
    }
}

bool toolBox::handleMouseEvent(QMouseEvent *event)
{
    if ((toolsRectToParent.contains(event->pos()) || (dragging == true)))
    {
        if (status != SHOWN)
            show();
    }
    else
    {
        if (status != HIDDEN)
            hide();
        return false;
    }

    //now we check if the mouse is on a pen object
    for (int i = 0; i < pens.count(); i++)
    {
        QPoint p; p.setX(pens[i].x_pos + toolsRectToParent.left()); p.setY(toolsRectToParent.topLeft().y());
        QRect r = pens[i].tool->get_tool_rect();
        r.setTopLeft(r.topLeft() + p); r.setBottomRight(r.bottomRight() + p);
        if ((r.contains(event->pos())) || ((dragging == true) && (i == drag_tool_index)))
        {
            if ((dragging == true) && (i != drag_tool_index))
                pens[i].tool->normal();
            else
                pens[i].tool->highlight();
        }
        else
            pens[i].tool->normal();
    }

    if (event->type() == QEvent::MouseButtonPress)
    {
        if (event->buttons() == Qt::LeftButton)
        {
            //check if it was pressed on a tool
            for (int i = 0; i < pens.count(); i++)
            {
                QPoint p; p.setX(pens[i].x_pos + toolsRectToParent.left()); p.setY(toolsRectToParent.topLeft().y());
                QRect r = pens[i].tool->get_tool_rect();
                r.setTopLeft(r.topLeft() + p); r.setBottomRight(r.bottomRight() + p);
                if (r.contains(event->pos()))
                {
                    pens[i].tool->select();
                    tools type = pens[i].tool->getToolType();
                    QVariant data = pens[i].tool->getData();
                    int size = pens[i].tool->getObjectSize();
                    emit toolChanged(type, data, size);
                }
                else
                    pens[i].tool->deselect();
            }
        }
        if (event->buttons() == Qt::RightButton)
        {
            right_click_position = event->pos();
            popup->popup(event->globalPos());
        }
    }

    if ((event->type() == QEvent::MouseMove) && (event->buttons() == Qt::LeftButton))
    {
        if (dragging ==  false)
        {
            //check if it was pressed on a tool
            for (int i = 0; i < pens.count(); i++)
            {
                QPoint p; p.setX(pens[i].x_pos + toolsRectToParent.left()); p.setY(toolsRectToParent.topLeft().y());
                QRect r = pens[i].tool->get_tool_rect();
                r.setTopLeft(r.topLeft() + p); r.setBottomRight(r.bottomRight() + p);
                if ((r.contains(event->pos()) && (dragging == false)))
                {
                    dragging = true;
                    drag_position = event->pos().y();
                    drag_tool_index = i;
                    start_size = pens[i].tool->getObjectSize();
                }
            }
        }
        else
        {
            int diff = event->pos().y() - drag_position;
            pens[drag_tool_index].tool->setObjectSize(start_size - diff / 10);
            tools type = pens[drag_tool_index].tool->getToolType();
            QVariant data = pens[drag_tool_index].tool->getData();
            int size = pens[drag_tool_index].tool->getObjectSize();
            emit toolChanged(type, data, size);
            emit updated();
        }
    }

    if ((event->type() == QEvent::MouseButtonRelease) && (event->button() == Qt::LeftButton))
        if (dragging)
            dragging = false;

    return true;
}

bool toolBox::handleMouseEvent(QTabletEvent *event)
{
#if QT_VERSION_MAJOR == 6
    QMouseEvent e(event->type(), event->position(), event->button(), event->buttons(), event->modifiers());
#else
    QMouseEvent e(event->type(), event->posF(), event->button(), event->buttons(), event->modifiers());
#endif
    return handleMouseEvent(&e);
}

void toolBox::setProperties(board_properties p)
{
    properties = p;
    for (int i = 0; i < pens.count(); i++)
        pens[i].tool->setProperties(p);
}

void toolBox::select_tool(tools t)
{
    //look for the next request tool of this type
    int start_idx = -1;
    bool found = false; int idx = -1;    
    int i;

    for (i = 0; i < pens.count(); i++)
        if (pens[i].tool->isSelected() == true)
        {
            found = true; start_idx = i;
        }
    if (found == false)
        return;

    i = start_idx + 1; found = false;
    while((found == false) && (i < pens.count()))
    {
        if (pens[i].tool->getToolType() == t)
        {
            found = true;
            idx = i;
        }
        i++;
    }
    if (found == false)
    {
        i = 0;
        while((found == false) && (i <= start_idx))
        {
            if (pens[i].tool->getToolType() == t)
            {
                found = true;
                idx = i;
            }
            i++;
        }
    }
    if (found)
    {
        for (int i = 0; i < pens.count(); i++)
        {
            if (i == idx)
            {
                pens[i].tool->select();
                emit toolChanged(t, pens[i].tool->getData(), pens[i].tool->getObjectSize());
            }
            else
                pens[i].tool->deselect();
        }
    }
}

void toolBox::select_tool(int idx)
{
    for (int i = 0; i < pens.count(); i++)
        pens[i].tool->deselect();
    pens[idx].tool->select();
    emit toolChanged(pens[idx].tool->getToolType(), pens[idx].tool->getData(), pens[idx].tool->getObjectSize());
}

int toolBox::get_selectedToolIndex()
{
    int idx = -1;
    for (int i = 0; i < pens.count(); i++)
        if (pens[i].tool->isSelected() == true)
            idx = i;
    return idx;
}

toolobject* toolBox::get_selectedToolObject()
{
    int idx = get_selectedToolIndex();
    if (idx == -1)
        return nullptr;
    else
        return pens[idx].tool;
}

void toolBox::show()
{
    if ((status == SHOWING) || (status == SHOWN))
        return;

    status = SHOWING;
    animationTimer.stop();
    animationTimer.start();
}

void toolBox::hide()
{
    if ((status == HIDING) || (status == HIDDEN))
        return;

    status = HIDING;
    animationTimer.stop();
    animationTimer.start();
}

void toolBox::animationTriggered()
{
    update_animation(animationTimer.interval());

    if ((status == SHOWING) & (animationValue >= 0.97f))
    {
        animationValue = 1.0f;
        status = SHOWN;
        animationTimer.stop();
    }

    if ((status == HIDING) & (animationValue <= 0.03f))
    {
        animationValue = 0.0f;
        status = HIDDEN;
        animationTimer.stop();
    }

    emit updated();
}

void toolBox::toolUpdated()
{
    emit updated();
}

void toolBox::addPenTriggered()
{
    QColor col = QColorDialog::getColor(Qt::black, nullptr, "Choose the pen's color", QColorDialog::ShowAlphaChannel);
    if (col.isValid() == false)
        return;
    add_pen(col);
}

void toolBox::remPenTriggered()
{
    for (int i = 0; i < pens.count(); i++)
    {
        QPoint p; p.setX(pens[i].x_pos + toolsRectToParent.left()); p.setY(toolsRectToParent.topLeft().y());
        QRect r = pens[i].tool->get_tool_rect();
        r.setTopLeft(r.topLeft() + p); r.setBottomRight(r.bottomRight() + p);
        if (r.contains(right_click_position))
        {
            if (pens[i].tool->getToolType() != PEN)
                return;
            else
            {
                QMessageBox::StandardButton answer = QMessageBox::question(nullptr, "Removing pen", "Are you sure you want to remove this pen?");
                if (answer == QMessageBox::StandardButton::Yes)
                {
                    pens.removeAt(i);
                    calculateRect();
                    emit updated();
                }
            }
        }
    }
}

void toolBox::changeColTriggered()
{
    for (int i = 0; i < pens.count(); i++)
    {
        QPoint p; p.setX(pens[i].x_pos + toolsRectToParent.left()); p.setY(toolsRectToParent.topLeft().y());
        QRect r = pens[i].tool->get_tool_rect();
        r.setTopLeft(r.topLeft() + p); r.setBottomRight(r.bottomRight() + p);
        if (r.contains(right_click_position))
        {
            if (pens[i].tool->getToolType() != PEN)
                return;
            else
            {
                QColor col = QColorDialog::getColor(QColor::fromRgba(pens[i].tool->getData().toUInt()), nullptr, "Choose the pen's color", QColorDialog::ShowAlphaChannel);
                if (col.isValid() == false)
                    return;
                pens[i].tool->set_color(col);
                emit updated();
            }
        }
    }
}

void toolBox::changeToolPositionTriggered()
{
    for (int i = 0; i < pens.count(); i++)
    {
        QPoint p; p.setX(pens[i].x_pos + toolsRectToParent.left()); p.setY(toolsRectToParent.topLeft().y());
        QRect r = pens[i].tool->get_tool_rect();
        r.setTopLeft(r.topLeft() + p); r.setBottomRight(r.bottomRight() + p);
        if (r.contains(right_click_position))
        {
            bool ok;

            int ret = QInputDialog::getInt(nullptr, "Change tool position", "Enter the position number: ", i + 1, 1, pens.count(), 1, &ok);
            if (ok == false)
                return;
            if (ret == i + 1)
                return;
            toolbox_type swap;
            ret -= 1;
            swap = pens[i];
            pens[i] = pens[ret];
            pens[ret] = swap;
            calculateRect();
            emit updated();
        }
    }
}

void toolBox::add_pen(QColor color)
{
    toolbox_type pen;
    pen.tool = new toolPen(properties, color);
    connect(pen.tool, &toolobject::updated, this, &toolBox::toolUpdated);
    pen.data = color.rgba();

    pens.append(pen);

    calculateRect();
}

void toolBox::add_eraser()
{
    toolbox_type eraser;
    eraser.tool = new toolErase(properties);
    connect(eraser.tool, &toolobject::updated, this, &toolBox::toolUpdated);
    eraser.data = "Eraser";

    pens.append(eraser);

    calculateRect();
}

void toolBox::add_select()
{
    toolbox_type select;
    select.tool = new toolSelect(properties);
    connect(select.tool, &toolobject::updated, this, &toolBox::toolUpdated);
    select.data = "Select";

    pens.append(select);

    calculateRect();
}

void toolBox::update_animation(int ms)
{
    float tau = 0.05f;
    float T = (float)ms / 1000.0f;

    float A = tau * 2.0f / T;
    float B = A + 1.0f;
    float C = 1.0f - A;

    xm1 = x;
    if (status == SHOWING)
        x = 1.0f;
    else
        x = 0.0f;

    animationValue = (1.0f/B) * ((x + xm1) - (C * animationValue));
}

void toolBox::calculateRectToParent(QRect rect)
{
    toolsRectToParent = toolsRect;
    QPoint topleft;
    topleft.setX((rect.width() - toolsRect.width()) / 2);
    topleft.setY(rect.height() - toolsRect.height());
    toolsRectToParent.setTopLeft(toolsRectToParent.topLeft() + topleft);
    toolsRectToParent.setBottomRight(toolsRectToParent.bottomRight() + topleft);
}

void toolBox::calculateRect()
{
    QRect r;
    int spacing = 20;

    properties.full_height = 0;

    toolsRect.setWidth(0); toolsRect.setHeight(0);
    for (int i = 0; i < pens.count(); i++)
    {
        r = pens[i].tool->get_tool_rect();
        if (r.height() > properties.full_height)
            properties.full_height = r.height();
        pens[i].x_pos = toolsRect.right() + spacing;
        toolsRect.setRight(toolsRect.right() + spacing + r.width());
    }
}

void toolBox::renderRectGL(QOpenGLFunctions *f, QRect r, QColor col, int line_size, int radius, QRect screen, float devicePixelRatio)
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    QVector<float> point_data;
    r = checkQRect(r);
    point_data.append(r.left()); point_data.append(r.top());
    point_data.append(r.right()); point_data.append(r.bottom());
    QVector4D ext_points(r.left(), r.top(), r.right(), r.bottom());
    QVector<float> x_buff, y_buff;
    x_buff.append(r.left()); x_buff.append(r.left()); x_buff.append(r.right()); x_buff.append(r.right());
    y_buff.append(r.top()); y_buff.append(r.bottom()); y_buff.append(r.top()); y_buff.append(r.bottom());

    m_rectESProgram->bind();
    m_rectESVao->bind();

    m_rect_ES_x_Vbo->bind();
    m_rect_ES_x_Vbo->allocate(x_buff.data(), x_buff.count() * sizeof(float));
    f->glEnableVertexAttribArray(0);
    f->glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, sizeof(float), reinterpret_cast<void*>(0));

    m_rect_ES_y_Vbo->bind();
    m_rect_ES_y_Vbo->allocate(y_buff.data(), y_buff.count() * sizeof(float));
    f->glEnableVertexAttribArray(1);
    f->glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(float), reinterpret_cast<void*>(0));

    m_rectESProgram->setUniformValue(m_rect_ES_in_color_loc, col);
    m_rectESProgram->setUniformValue(m_rect_ES_in_params_loc, QVector4D(line_size, screen.width(), screen.height(), radius));
    QMatrix4x4 id; id.setToIdentity();
    m_rectESProgram->setUniformValue(m_rect_ES_transfMatrix_loc, id);
    m_rectESProgram->setUniformValue(m_rect_ES_pixelratio_loc, devicePixelRatio);
    m_rectESProgram->setUniformValue(m_rect_ES_ext_points_loc, ext_points);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    m_rect_ES_x_Vbo->release();
    m_rect_ES_y_Vbo->release();
    m_rectESVao->release();
    m_rectESProgram->release();
    /*
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    QVector<float> point_data;
    r = checkQRect(r);
    point_data.append(r.left()); point_data.append(r.top());
    point_data.append(r.right()); point_data.append(r.bottom());

    m_rectProgram->bind();
    m_rectVao->bind();

    m_line_x_Vbo->bind();
    m_line_x_Vbo->allocate(point_data.data(), point_data.count() * sizeof(float));
    f->glEnableVertexAttribArray(0);
    f->glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), reinterpret_cast<void*>(0));

    m_rectProgram->setUniformValue(m_rect_in_color_loc, col);
    m_rectProgram->setUniformValue(m_rect_line_size_loc, QVector4D(line_size, screen.width(), screen.height(), radius));
    QMatrix4x4 id; id.setToIdentity();
    m_rectProgram->setUniformValue(m_rect_transfMatrix_loc, id);
    m_rectProgram->setUniformValue(m_rect_pixelratio_loc, devicePixelRatio);
    glDrawArrays(GL_LINES, 0, 2);

    m_line_x_Vbo->release();
    m_rectVao->release();
    m_rectProgram->release();*/
}

QRect toolBox::checkQRect(QRect r)
{
    QRect o;
    o = r;
    if ((r.width() < 0) && (r.height() < 0))
    {
        o.setTopLeft(r.bottomRight());
        o.setBottomRight(r.topLeft());
    }
    if (o.width() < 0)
    {
        o.setLeft(r.right());
        o.setRight(r.left());
        o.setTop(r.top());
        o.setBottom(r.bottom());
    }
    if (o.height() < 0)
    {
        o.setBottom(r.top());
        o.setTop(r.bottom());
        o.setLeft(r.left());
        o.setRight(r.right());
    }
    return o;
}
