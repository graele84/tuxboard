/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{    
    load_preferences_from_local_file();

    prepareWindow();

    setMinimumSize(QSize(1900, 800));
}

MainWindow::~MainWindow()
{
    qDebug() << "Saving preferences";
    save_preferences_to_local_file();
}

void MainWindow::newBoardRequested()
{
    glBoard *board = new glBoard(properties);
    connect(board, &glBoard::newBoardRequest, this, &MainWindow::newBoardRequested);
    connect(board, &glBoard::closeRequested, this, &MainWindow::boardClosed);
    connect(board, &glBoard::propertiesChanged, this, &MainWindow::properties_changed);

    boards.append(board);

    board->resize(1920, 1080);
    board->show();
    board->setFocus();
}

void MainWindow::boardClosed(glBoard *pointer)
{
    bool found = false; int idx;
    for (int i = 0; i < boards.count(); i++)
        if (boards[i] == pointer)
        {
            found = true; idx = i;
        }
    if (found == true)
    {
        disconnect(boards[idx], &glBoard::newBoardRequest, this, &MainWindow::newBoardRequested);
        disconnect(boards[idx], &glBoard::closeRequested, this, &MainWindow::boardClosed);
        disconnect(boards[idx], &glBoard::propertiesChanged, this, &MainWindow::properties_changed);
        delete boards[idx];
        boards.removeAt(idx);
    }
}

void MainWindow::prepareWindow()
{
    newBoardRequested();
}

void MainWindow::load_preferences_from_local_file()
{
    prefmanager manager;

    //we check if a file called "esplot.conf" exists in the current directory
    if (QFileInfo::exists(QApplication::applicationDirPath() + "/tuxboard.conf")) //file exists => it can be loaded from the preference manager
    {
        if (manager.loadFromFile(QApplication::applicationDirPath() + "/tuxboard.conf") == 0)
        {
            qDebug() << "Loading settings from local file";
            properties = manager.get_properties();
        }
        else
        {
            qDebug() << "Loading default settings";
            properties = manager.load_default();
        }
    }
    else
    {
        qDebug() << "Loading default settings";
        properties = manager.load_default();
    }
}

void MainWindow::save_preferences_to_local_file()
{
    prefmanager manager;
    QString filename = QApplication::applicationDirPath() + "/tuxboard.conf";
    manager.set_properties(properties);
    if (manager.saveOnFile(filename) == -1)
        qDebug() << "Error while saving to " + filename;
}

