/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#ifndef PREFMANAGER_H
#define PREFMANAGER_H

#include <QFile>
#include <QString>
#include <QTextStream>

#include <QDebug>

#include "common.h"

class prefmanager
{
public:
    prefmanager();

    int saveOnFile(QString filename);
    int loadFromFile(QString filename);

    board_properties load_default();

    board_properties get_properties() { return properties; };
    void set_properties(board_properties p) { properties = p; }

private:
    board_properties properties;

    QStringList convertPreferencesToStringList();
    bool convertArgumentToColor(QString arg, QColor *c);
    bool convertArgumentToBool(QString arg, bool *out);
    bool convertArgumentToFloat(QString arg, float min, float max, float *v);
    bool convertArgumentToInt(QString arg, int min, int max, int *v);
    bool convertArgumentToPenStyle(QString arg, Qt::PenStyle *p);
    bool convertArgumentToToolType(QString arg, tools *t);

    QString convertColorToString(QColor c);
    QString convertBoolToString(bool v);
    QString convertPenStyleToString(Qt::PenStyle p);

    bool getArgumentsFromLine(QString line, QString *arg1, QString *arg2);
    void importFromParamLines(QStringList params);
    void processParameterLine(QString arg1, QString arg2);
};

#endif // PREFMANAGER_H
