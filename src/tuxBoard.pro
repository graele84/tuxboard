QT       += core gui widgets svg

greaterThan(QT_MAJOR_VERSION, 5): QT += openglwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Dialogs/infodialog.cpp \
    glboard.cpp \
    main.cpp \
    mainwindow.cpp \
    Dialogs/prefdialog.cpp \
    prefmanager.cpp \
    textpopup.cpp \
    toolbox.cpp \
    toolerase.cpp \
    toolobject.cpp \
    toolpen.cpp \
    toolselect.cpp

HEADERS += \
    Dialogs/infodialog.h \
    common.h \
    glboard.h \
    mainwindow.h \
    Dialogs/prefdialog.h \
    prefmanager.h \
    textpopup.h \
    toolbox.h \
    toolerase.h \
    toolobject.h \
    toolpen.h \
    toolselect.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

#set this if you want to use OpenGL ES, e.g. on Raspberry Pi
#DEFINES += OPENGL_ES

RESOURCES += \
    resources.qrc
