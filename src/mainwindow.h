/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QApplication>

#include <QFileInfo>

#include <QKeyEvent>
#include <QDebug>

#include "common.h"
#include "prefmanager.h"
#include "glboard.h"


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    void keyPressEvent(QKeyEvent *event) override { Q_UNUSED(event); }

private slots:
    void newBoardRequested();
    void boardClosed(glBoard *pointer);
    void properties_changed(board_properties prop) { properties = prop; }

private:
    QVector<glBoard*> boards;

    board_properties properties;

    void prepareWindow();

    void load_preferences_from_local_file();
    void save_preferences_to_local_file();
};
#endif // MAINWINDOW_H
