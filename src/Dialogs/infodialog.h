#ifndef INFODIALOG_H
#define INFODIALOG_H

#include <QDialog>
#include <QPixmap>
#include <QLabel>
#include <QVBoxLayout>
#include <QDialogButtonBox>

class infodialog : public QDialog
{
public:
    infodialog();

private:
    QPixmap logo;
    QLabel *logoLbl;
    QLabel *txtLbl;
    QDialogButtonBox *bttBox;

    void prepareDialog();
};

#endif // INFODIALOG_H
