#include "infodialog.h"

infodialog::infodialog()
{
    prepareDialog();
    resize(sizeHint());
}

void infodialog::prepareDialog()
{
    logo.load(":/icons/Resources/tuxboard_logo.png");
    logo.scaled(150, 150, Qt::KeepAspectRatio);
    logoLbl = new QLabel(this);
    logoLbl->setPixmap(logo);
    logoLbl->setBackgroundRole(QPalette::Base);
    logoLbl->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    logoLbl->setScaledContents(false);
    logoLbl->resize(logo.size());

    txtLbl = new QLabel(this);
    QString text;
    text = "tuxBoard is a free software released under AGPL 3.0 license.\n";
    text += "It is based on Qt 6.1. Full code of Qt 6.1 can be found at: \n";
    text += "https://download.qt.io/archive/qt/6.1/ \n";
    text += "OpenGL 3.2 accelerated.\n\n";
    text += "\nDeveloped by Emanuele Grasso";
    txtLbl->setText(text);

    bttBox = new QDialogButtonBox(QDialogButtonBox::Ok);
    connect(bttBox, &QDialogButtonBox::accepted, this, &QDialog::accept);

    QVBoxLayout *lay = new QVBoxLayout;
    lay->addWidget(logoLbl);
    lay->addWidget(txtLbl);
    lay->addWidget(bttBox);

    setLayout(lay);

    setWindowTitle("tuxBoard Info");
}
