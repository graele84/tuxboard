/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#include "prefdialog.h"

prefdialog::prefdialog(board_properties prop, QWidget *parent) : QDialog(parent)
{
    properties = prop;
    old_properties = prop;

    prepareDialog();

    setWindowTitle("Preferences");
}

void prefdialog::middle_button_slot(int idx)
{
    if (idx == 0)
        properties.middleButtonTool = ERASE;
    if (idx == 1)
        properties.middleButtonTool = SELECT;
    emit propertiesChanged(properties);
    updateData();
}

void prefdialog::eraser_mode_slot(int idx)
{
    if (idx == 0)
        properties.eraser_default_mode = false;
    if (idx == 1)
        properties.eraser_default_mode = true;
    emit propertiesChanged(properties);
    updateData();
}

void prefdialog::load_slot()
{
    QString filename = QFileDialog::getOpenFileName(this, "Load settings", "", "Tux Board Setting (*.conf);; All Files (*)");
    if (filename.isEmpty())
        return;

    prefmanager *manager = new prefmanager;
    if (manager->loadFromFile(filename) != 0)
        QMessageBox::warning(this, "Error", "Error while loading settings file");
    else
        properties = manager->get_properties();

    updateData();

    delete manager;
}

void prefdialog::save_slot()
{
    QString filename = QFileDialog::getSaveFileName(this, "Save settings", "", "Tux Board Setting (*.conf);; All files (*)");
    if (filename.isEmpty())
        return;

    prefmanager *manager = new prefmanager;
    manager->set_properties(properties);
    if (manager->saveOnFile(filename) != 0)
        QMessageBox::warning(this, "Error", "Error while saving settings file");

    delete manager;
}

void prefdialog::prepareDialog()
{
    stylusGB = new QGroupBox("Pen configuration");

    pressureCB = new QCheckBox;
    pressureCB->setCheckable(true);
    pressureCB->setChecked(properties.apply_pen_pressure);
    connect(pressureCB, &QCheckBox::stateChanged, this, &prefdialog::pressure_slot);

    middleButtonCB = new QComboBox;
    middleButtonCB->addItem("ERASER"); middleButtonCB->addItem("SELECTOR");
    if (properties.middleButtonTool == ERASE)
        middleButtonCB->setCurrentIndex(0);
    if (properties.middleButtonTool == SELECT)
        middleButtonCB->setCurrentIndex(1);
#if QT_VERSION_MAJOR == 6
    connect(middleButtonCB, &QComboBox::currentIndexChanged, this, &prefdialog::middle_button_slot);
#else
    connect(middleButtonCB, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &prefdialog::middle_button_slot);
#endif

    eraserModeCB = new QComboBox;
    eraserModeCB->addItem("POINTS"); eraserModeCB->addItem("LINES");
    if (properties.eraser_default_mode == false)
        eraserModeCB->setCurrentIndex(0);
    if (properties.eraser_default_mode == true)
        eraserModeCB->setCurrentIndex(1);
#if QT_VERSION_MAJOR == 6
    connect(eraserModeCB, &QComboBox::currentIndexChanged, this, &prefdialog::eraser_mode_slot);
#else
    connect(eraserModeCB, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &prefdialog::eraser_mode_slot);
#endif

    QHBoxLayout *stylusLayH1 = new QHBoxLayout; stylusLayH1->addWidget(new QLabel("Apply pen pressure")); stylusLayH1->addWidget(pressureCB);
    QHBoxLayout *stylusLayH2 = new QHBoxLayout; stylusLayH2->addWidget(new QLabel("Middle button tool")); stylusLayH2->addWidget(middleButtonCB);
    QHBoxLayout *stylusLayH3 = new QHBoxLayout; stylusLayH3->addWidget(new QLabel("Default eraser mode")); stylusLayH3->addWidget(eraserModeCB);
    QVBoxLayout *stylusLayV = new QVBoxLayout;
    stylusLayV->addLayout(stylusLayH1);
    stylusLayV->addLayout(stylusLayH2);
    stylusLayV->addLayout(stylusLayH3);
    stylusGB->setLayout(stylusLayV);

    gridBoxGB = new QGroupBox("Grid");

    backgroundPB = new QPushButton;
    backgroundPB->setAutoFillBackground(true); backgroundPB->setText("...");
    backgroundPB->update();
    connect(backgroundPB, &QPushButton::clicked, this, &prefdialog::background_slot);

    gridColorPB = new QPushButton;
    gridColorPB->setAutoFillBackground(true); gridColorPB->setText("..."); gridColorPB->update();
    gridColorPB->update();
    connect(gridColorPB, &QPushButton::clicked, this, &prefdialog::gridcolor_slot);

    gridStepSB = new QSpinBox;
    gridStepSB->setMinimum(1); gridStepSB->setMaximum(10000);
#if QT_VERSION_MAJOR == 6
    connect(gridStepSB, &QSpinBox::valueChanged, this, &prefdialog::gridstep_slot);
#else
    connect(gridStepSB, QOverload<int>::of(&QSpinBox::valueChanged), this, &prefdialog::gridstep_slot);
#endif

    gridLineSizeSB = new QSpinBox;
    gridLineSizeSB->setMinimum(1); gridLineSizeSB->setMaximum(100);
#if QT_VERSION_MAJOR == 6
    connect(gridLineSizeSB, &QSpinBox::valueChanged, this, &prefdialog::gridline_slot);
#else
    connect(gridLineSizeSB, QOverload<int>::of(&QSpinBox::valueChanged), this, &prefdialog::gridline_slot);
#endif

    showGridCB = new QCheckBox;
    showGridCB->setCheckable(true);
    connect(showGridCB, &QCheckBox::stateChanged, this, &prefdialog::showgrid_slot);

    QHBoxLayout *gridLayH1 = new QHBoxLayout; gridLayH1->addWidget(new QLabel("Background color")); gridLayH1->addWidget(backgroundPB);
    gridLayH1->addWidget(new QLabel("Show grid:")); gridLayH1->addWidget(showGridCB);
    QHBoxLayout *gridLayH2 = new QHBoxLayout; gridLayH2->addWidget(new QLabel("Grid line color")); gridLayH2->addWidget(gridColorPB);
    QHBoxLayout *gridLayH3 = new QHBoxLayout; gridLayH3->addWidget(new QLabel("Grid step")); gridLayH3->addWidget(gridStepSB);
    QHBoxLayout *gridLayH4 = new QHBoxLayout; gridLayH4->addWidget(new QLabel("Grid line size")); gridLayH4->addWidget(gridLineSizeSB);

    QVBoxLayout *gridLayV = new QVBoxLayout;
    gridLayV->addLayout(gridLayH1); gridLayV->addLayout(gridLayH3); gridLayV->addLayout(gridLayH2); gridLayV->addLayout(gridLayH4); gridLayV->addStretch();

    gridBoxGB->setLayout(gridLayV);

    selectionBoxGB = new QGroupBox("Selection tool");

    selectColorPB = new QPushButton;
    selectColorPB->setAutoFillBackground(true); selectColorPB->setText("...");
    selectColorPB->update();
    connect(selectColorPB, &QPushButton::clicked, this, &prefdialog::selectcolor_slot);

    selectFillColorPB = new QPushButton;
    selectFillColorPB->setAutoFillBackground(true); selectFillColorPB->setText("...");
    selectFillColorPB->update();
    connect(selectFillColorPB, &QPushButton::clicked, this, &prefdialog::selectfill_slot);

    selectLineSizeSB = new QSpinBox;
    selectLineSizeSB->setMinimum(1); selectLineSizeSB->setMaximum(100);
#if QT_VERSION_MAJOR == 6
    connect(selectLineSizeSB, &QSpinBox::valueChanged, this, &prefdialog::selectline_slot);
#else
    connect(selectLineSizeSB, QOverload<int>::of(&QSpinBox::valueChanged), this, &prefdialog::selectline_slot);
#endif

    QHBoxLayout *selectLayH1 = new QHBoxLayout; selectLayH1->addWidget(new QLabel("Selection line color")); selectLayH1->addWidget(selectColorPB);
    QHBoxLayout *selectLayH2 = new QHBoxLayout; selectLayH2->addWidget(new QLabel("Selection background color")); selectLayH2->addWidget(selectFillColorPB);
    QHBoxLayout *selectLayH3 = new QHBoxLayout; selectLayH3->addWidget(new QLabel("Selection line size")); selectLayH3->addWidget(selectLineSizeSB);
    QVBoxLayout *selectLayV = new QVBoxLayout;
    selectLayV->addLayout(selectLayH2); selectLayV->addLayout(selectLayH1); selectLayV->addLayout(selectLayH3);
    selectionBoxGB->setLayout(selectLayV);


    toolboxBoxGB = new QGroupBox("Toolbox");

    toolboxHiddenColorPB = new QPushButton;
    toolboxHiddenColorPB->setAutoFillBackground(true); toolboxHiddenColorPB->setText("...");
    toolboxHiddenColorPB->update();
    connect(toolboxHiddenColorPB, &QPushButton::clicked, this, &prefdialog::toolboxhiddencolor_slot);

    toolboxFullHeightSB = new QSpinBox;
    toolboxFullHeightSB->setMinimum(10); toolboxFullHeightSB->setMaximum(5000);
#if QT_VERSION_MAJOR == 6
    connect(toolboxFullHeightSB, &QSpinBox::valueChanged, this, &prefdialog::toolboxfullheigth_slot);
#else
    connect(toolboxFullHeightSB, QOverload<int>::of(&QSpinBox::valueChanged), this, &prefdialog::toolboxfullheigth_slot);
#endif

    toolboxFullWidthSB = new QSpinBox;
    toolboxFullWidthSB->setMinimum(10); toolboxFullWidthSB->setMaximum(5000);
#if QT_VERSION_MAJOR == 6
    connect(toolboxFullWidthSB, &QSpinBox::valueChanged, this, &prefdialog::toolboxfullwidth_slot);
#else
    connect(toolboxFullWidthSB, QOverload<int>::of(&QSpinBox::valueChanged), this, &prefdialog::toolboxfullwidth_slot);
#endif

    toolboxHiddenHeightSB = new QSpinBox;
    toolboxHiddenHeightSB->setMinimum(1); toolboxHiddenHeightSB->setMaximum(5000);
#if QT_VERSION_MAJOR == 6
    connect(toolboxHiddenHeightSB, &QSpinBox::valueChanged, this, &prefdialog::toolboxhiddenheight_slot);
#else
    connect(toolboxHiddenHeightSB, QOverload<int>::of(&QSpinBox::valueChanged), this, &prefdialog::toolboxhiddenheight_slot);
#endif

    QHBoxLayout *toolboxLayH1 = new QHBoxLayout; toolboxLayH1->addWidget(new QLabel("Color when hidden")); toolboxLayH1->addWidget(toolboxHiddenColorPB);
    QHBoxLayout *toolboxLayH2 = new QHBoxLayout; toolboxLayH2->addWidget(new QLabel("Height of the hidden box")); toolboxLayH2->addWidget(toolboxHiddenHeightSB);
    QHBoxLayout *toolboxLayH3 = new QHBoxLayout; toolboxLayH3->addWidget(new QLabel("Height of the tools")); toolboxLayH3->addWidget(toolboxFullHeightSB);
    toolboxLayH3->addWidget(new QLabel("Width of the tools")); toolboxLayH3->addWidget(toolboxFullWidthSB);
    QVBoxLayout *toolboxLayV = new QVBoxLayout;
    toolboxLayV->addLayout(toolboxLayH1); toolboxLayV->addLayout(toolboxLayH2); toolboxLayV->addLayout(toolboxLayH3);
    toolboxBoxGB->setLayout(toolboxLayV);

    buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

    loadPB = new QPushButton("Load...");
    savePB = new QPushButton("Save...");
    connect(loadPB, &QPushButton::clicked, this, &prefdialog::load_slot);
    connect(savePB, &QPushButton::clicked, this, &prefdialog::save_slot);
    QHBoxLayout *buttonLayH = new QHBoxLayout;
    buttonLayH->addWidget(loadPB); buttonLayH->addWidget(savePB);
    buttonLayH->addStretch(); buttonLayH->addWidget(buttonBox);

    QVBoxLayout *dialogLay = new QVBoxLayout;
    dialogLay->addWidget(stylusGB);
    dialogLay->addWidget(gridBoxGB);
    dialogLay->addWidget(toolboxBoxGB);
    dialogLay->addWidget(selectionBoxGB);
    dialogLay->addLayout(buttonLayH);

    setLayout(dialogLay);

    updateData();
}

void prefdialog::updateData()
{
    QColor back; back = Qt::gray;
    backgroundPB->setStyleSheet(QString("background-color: %1; color: %2").arg(properties.background.name()).arg(back.name()));

    gridColorPB->setStyleSheet(QString("background-color: %1; color: %2").arg(properties.grid_color.name()).arg(back.name()));
    gridStepSB->setValue(properties.grid_step);
    gridLineSizeSB->setValue(properties.grid_line_size);
    showGridCB->setChecked(properties.showGrid);

    selectColorPB->setStyleSheet(QString("background-color: %1; color: %2").arg(properties.selectColor.name()).arg(back.name()));
    selectFillColorPB->setStyleSheet(QString("background-color: %1; color: %2").arg(properties.selectFillColor.name()).arg(back.name()));
    selectLineSizeSB->setValue(properties.selectLineSize);

    toolboxHiddenColorPB->setStyleSheet(QString("background-color: %1; color: %2").arg(properties.hiddenColor.name()).arg(back.name()));
    toolboxFullHeightSB->setValue(properties.full_height);
    toolboxFullWidthSB->setValue(properties.full_width);
    toolboxHiddenHeightSB->setValue(properties.hidden_height);
}

QColor prefdialog::getColor(QString text, QColor startCol)
{
    QColor col;

    col = QColorDialog::getColor(startCol, this, text, QColorDialog::ColorDialogOption::DontUseNativeDialog | QColorDialog::ColorDialogOption::ShowAlphaChannel);

    if (col.isValid())
        return col;
    else
        return startCol;
}
