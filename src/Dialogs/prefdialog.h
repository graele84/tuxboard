/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#ifndef PREFDIALOG_H
#define PREFDIALOG_H

#include <QWidget>
#include <QDialog>
#include <QGroupBox>
#include <QColorDialog>
#include <QPushButton>
#include <QSpinBox>
#include <QComboBox>
#include <QCheckBox>
#include <QLabel>
#include <QDialogButtonBox>
#include <QFileDialog>
#include <QMessageBox>

#include <QHBoxLayout>
#include <QVBoxLayout>

#include "prefmanager.h"

class prefdialog : public QDialog
{
    Q_OBJECT
public:
    explicit prefdialog(board_properties prop, QWidget *parent = nullptr);

    board_properties get_properties() { return properties; }
    board_properties get_old_properties() { return old_properties; }

signals:
    void propertiesChanged(board_properties p);

private slots:
    void pressure_slot() { properties.apply_pen_pressure = pressureCB->isChecked(); emit propertiesChanged(properties); updateData(); }
    void middle_button_slot(int idx);
    void eraser_mode_slot(int idx);

    void background_slot() { properties.background = getColor("Choose the background color", properties.background); emit propertiesChanged(properties); updateData(); }
    void gridcolor_slot() { properties.grid_color = getColor("Choose the color of the grid line", properties.grid_color); emit propertiesChanged(properties); updateData(); }
    void gridstep_slot() { properties.grid_step = gridStepSB->value(); emit propertiesChanged(properties); }
    void gridline_slot() { properties.grid_line_size = gridLineSizeSB->value(); emit propertiesChanged(properties);}
    void showgrid_slot() { properties.showGrid = showGridCB->isChecked(); emit propertiesChanged(properties); }

    void selectcolor_slot() { properties.selectColor = getColor("Choose the select line color", properties.selectColor); emit propertiesChanged(properties); updateData(); }
    void selectfill_slot() { properties.selectFillColor = getColor("Choose the select fill color", properties.selectFillColor); emit propertiesChanged(properties); updateData(); }
    void selectline_slot() { properties.selectLineSize = selectLineSizeSB->value(); emit propertiesChanged(properties); }
    void toolboxhiddencolor_slot() { properties.hiddenColor = getColor("Choose the toolbox hidden color", properties.hiddenColor); emit propertiesChanged(properties); updateData(); }
    void toolboxhiddenheight_slot() { properties.hidden_height = toolboxHiddenHeightSB->value(); emit propertiesChanged(properties); }
    void toolboxfullheigth_slot() { properties.full_height = toolboxFullHeightSB->value(); emit propertiesChanged(properties); }
    void toolboxfullwidth_slot() { properties.full_width = toolboxFullWidthSB->value(); emit propertiesChanged(properties); }

    void load_slot();
    void save_slot();

private:
    board_properties properties;
    board_properties old_properties;

    QDialogButtonBox *buttonBox;
    QPushButton *loadPB;
    QPushButton *savePB;

    QGroupBox *stylusGB;
    QGroupBox *gridBoxGB;
    QGroupBox *selectionBoxGB;
    QGroupBox *toolboxBoxGB;

    QCheckBox *pressureCB;
    QComboBox *middleButtonCB;
    QComboBox *eraserModeCB;

    QPushButton *backgroundPB;

    QPushButton *gridColorPB;
    QSpinBox *gridStepSB;
    QSpinBox *gridLineSizeSB;
    QCheckBox *showGridCB;

    QPushButton *selectColorPB;
    QPushButton *selectFillColorPB;
    QSpinBox *selectLineSizeSB;

    QPushButton *toolboxHiddenColorPB;
    QSpinBox *toolboxFullHeightSB;
    QSpinBox *toolboxHiddenHeightSB;
    QSpinBox *toolboxFullWidthSB;

    void prepareDialog();
    void updateData();
    QColor getColor(QString text, QColor startCol);
};

#endif // PREFDIALOG_H
