/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#ifndef TEXTPOPUP_H
#define TEXTPOPUP_H

#include <QObject>
#include <QPainter>
#include <QPainterPath>
#include <QRect>
#include <QTimer>

#include <QDebug>

#include "common.h"

class textPopup : public QObject
{
    Q_OBJECT
public:
    explicit textPopup(QObject *parent = nullptr);

    void paint(QPainter &painter, QRect rect);
    QRect get_tool_rect() { return popupRectToParent; }

    void popup() { x = 1.0f; popupcounter = 0; waiting = false; decreasing = false; shown = true; animationTimer.stop(); animationTimer.start(); }
    void setText(QString t) { text = t; }

signals:
    void updated();

private slots:
    void animationTriggered();

private:
    QTimer animationTimer;
    float animationValue;  //from 0.0 to 1.0
    float xm1, x;

    bool shown;
    bool decreasing;
    bool waiting;

    QRect popupRect;
    QRect popupRectToParent;

    int popuptime;
    int popupcounter;
    int refreshTime;

    QString text;
    QColor textColor;
    QColor backgroundColor;
    QColor rectColor;
    int rectLineSize;
    Qt::PenStyle rectPenStyle;
    int textSize;
    QFont textFont;

    void load_default_values();
    void calculateRectToParent(QRect rect);
    void update_animation(int ms);
};

#endif // TEXTPOPUP_H
