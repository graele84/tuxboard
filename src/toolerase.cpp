/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#include "toolerase.h"

toolErase::toolErase(board_properties p) : toolobject(p)
{
    color = Qt::white;
    color.setAlphaF(0.3f);

    object_min_size = 5;
    object_max_size = 50;
    object_size = 10;

    emit updated();
}

void toolErase::paint(QPainter &painter, QPoint topleft)
{
    //we paint the rect
    toolRect.setSize(normalSize * (1 + (animationValue)*(highlighted_scale - 1.0f)));
    painter.setBrush(Qt::NoBrush);
    painter.setPen(QPen(QBrush(color), 2, Qt::SolidLine));
    QRect rect;
    rect.setTopLeft(toolRect.topLeft() + topleft);
    rect.setBottomRight(toolRect.bottomRight() + topleft);
    QPainterPath path;
    path.addRoundedRect(rect, 10, 10);
    painter.drawPath(path);

    //we simply paint a rect
    QRect circle;
    int max_radius = (int)((float)toolRect.width() / 1.61803398875f / 2.0f);
    int min_radius = (int)((float)toolRect.width() / 1.61803398875f / 2.0f) / 2;
    int r = min_radius + object_size * (max_radius - min_radius) / (object_max_size - object_min_size);

    circle.setTopLeft(QPoint(toolRect.topLeft().x() + (toolRect.width() / 2) - r + topleft.x(), toolRect.topLeft().y() + (toolRect.height() / 2) - r + topleft.y()));
    circle.setBottomRight(QPoint(toolRect.topLeft().x() + (toolRect.width() / 2) + r + topleft.x(), toolRect.topLeft().y() + (toolRect.height() / 2) + r + topleft.y()));

    painter.setBrush(color);
    painter.drawRect(circle);
}

void toolErase::paintGL(QOpenGLFunctions *f, QRect screen, QPoint topleft, float devicePixelRatio)
{
    if (gl_initialized == false)
    {
        initializeGL();
        gl_initialized = true;
    }
    //we paint the rect
    toolRect.setSize(normalSize * (1 + (animationValue)*(highlighted_scale - 1.0f)));
    QRect rect;
    rect.setTopLeft(toolRect.topLeft() + topleft);
    rect.setBottomRight(toolRect.bottomRight() + topleft);
    renderRectGL(f, rect, color, 2, 10, screen, devicePixelRatio);

    //we simply paint a rect
    QRect circle;
    int max_radius = (int)((float)toolRect.width() / 1.61803398875f / 2.0f);
    int min_radius = (int)((float)toolRect.width() / 1.61803398875f / 2.0f) / 2;
    int r = min_radius + object_size * (max_radius - min_radius) / (object_max_size - object_min_size);

    circle.setTopLeft(QPoint(toolRect.topLeft().x() + (toolRect.width() / 2) - r + topleft.x(), toolRect.topLeft().y() + (toolRect.height() / 2) - r + topleft.y()));
    circle.setBottomRight(QPoint(toolRect.topLeft().x() + (toolRect.width() / 2) + r + topleft.x(), toolRect.topLeft().y() + (toolRect.height() / 2) + r + topleft.y()));

    renderRectGL(f, circle, color, -1, -1, screen, devicePixelRatio);
}

