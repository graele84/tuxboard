﻿/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#ifndef TOOLBOX_H
#define TOOLBOX_H

#include <QObject>
#include <QRect>
#include <QPainter>

#include <QMouseEvent>
#include <QTabletEvent>
#include <QMenu>
#include <QAction>

#include <QColorDialog>
#include <QMessageBox>
#include <QInputDialog>

#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>

#include <QMatrix4x4>
#include <QVector4D>

#include "toolpen.h"
#include "toolerase.h"
#include "toolselect.h"
#include "common.h"

#include <QTimer>

typedef struct
{
    toolobject *tool;
    tools type;
    QVariant data;
    int x_pos;
} toolbox_type;

typedef enum
{
    HIDDEN = 0x00,
    SHOWN = 0x01,
    SHOWING = 0x02,
    HIDING = 0x03
} toolbox_status;

class toolBox : public QObject
{
    Q_OBJECT

public:
    toolBox(board_properties prop);

    void initializeGL();
    void paint(QPainter &painter, QRect rect);
    void paintGL(QOpenGLFunctions *f, QRect rect, float devicePixelRatio);
    QRect get_toolbox_rect() { return toolsRectToParent; }
    bool handleMouseEvent(QMouseEvent *event);
    bool handleMouseEvent(QTabletEvent *event);

    void setProperties(board_properties p);
    board_properties getProperties() { return properties; }

    bool isDragging() { return dragging; }

    void select_tool(tools t);
    void select_tool(int idx);
    int get_selectedToolIndex();
    toolobject* get_selectedToolObject();

    void show();
    void hide();

signals:
    void updated();
    void toolChanged(tools toolType, QVariant data, int size);

private slots:
    void animationTriggered();
    void toolUpdated();

    void addPenTriggered();
    void remPenTriggered();
    void changeColTriggered();
    void changeToolPositionTriggered();

private:
    board_properties properties;
    toolbox_status status;

    bool gl_initialized;

/*    QOpenGLShaderProgram *m_rectProgram;
    QOpenGLVertexArrayObject *m_rectVao;
    QOpenGLBuffer *m_line_x_Vbo;
    int m_rect_in_color_loc;
    int m_rect_line_size_loc;
    int m_rect_transfMatrix_loc;
    int m_rect_pixelratio_loc;*/

    QOpenGLShaderProgram *m_rectESProgram;
    QOpenGLVertexArrayObject *m_rectESVao;
    QOpenGLBuffer *m_rect_ES_x_Vbo;
    QOpenGLBuffer *m_rect_ES_y_Vbo;
    int m_rect_ES_in_color_loc;
    int m_rect_ES_in_params_loc;
    int m_rect_ES_transfMatrix_loc;
    int m_rect_ES_pixelratio_loc;
    int m_rect_ES_ext_points_loc;

    QRect toolsRect;
    QRect toolsRectToParent;
    QTimer animationTimer;
    float animationValue;  //from 0.0 to 1.0
    float xm1, x;

    int drag_position;
    int drag_tool_index;
    int start_size;
    bool dragging;

    QVector<toolbox_type> pens;

    QMenu *popup;
    QAction *addPenAct;
    QAction *remPenAct;
    QAction *changeToolAct;
    QAction *changeColAct;

    QPoint right_click_position;

    void add_pen(QColor color);
    void add_eraser();
    void add_select();

    void update_animation(int ms);
    void calculateRectToParent(QRect rect);
    void calculateRect();

    void renderRectGL(QOpenGLFunctions *f, QRect r, QColor col, int line_size, int radius, QRect screen, float devicePixelRatio);
    QRect checkQRect(QRect r);
};

#endif // TOOLBOX_H
