/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#include "toolobject.h"

toolobject::toolobject(board_properties prop, QObject *parent) : QObject(parent)
{
    properties = prop;
    status = NORMAL;

    normalSize = QSize(properties.full_width, properties.full_height);
    toolRect.setSize(normalSize);

    color.setAlphaF(0.3f);

    animationTimer.setInterval(16);
    connect(&animationTimer, &QTimer::timeout, this, &toolobject::animationTriggered);
    animationValue = 0.0f;  //it's hidden
    x = 0.0f; xm1 = 0.0f;
    highlighted_scale = 1.2f;

    object_min_size = 1;
    object_max_size = 20;
    object_size = 4;

    gl_initialized = false;
}

void toolobject::initializeGL()
{
    m_rectESProgram = new QOpenGLShaderProgram;
    if (m_rectESProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/Shaders/Rect_ES.vsh") == false)
        qDebug("Error compiling Vertex Shader");
    if (m_rectESProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/Shaders/Rect_ES.fsh") == false)
        qDebug("Error compiling Fragment Shader");
    if (m_rectESProgram->link() == false)
        qDebug("Shaders were not linked correctly");
    if (m_rectESProgram->bind() == false)
        qDebug("Shaders not correctly bound");
    m_rect_ES_in_color_loc = m_rectESProgram->uniformLocation("in_color");
    m_rect_ES_in_params_loc = m_rectESProgram->uniformLocation("in_params");
    m_rect_ES_transfMatrix_loc = m_rectESProgram->uniformLocation("transf");
    m_rect_ES_pixelratio_loc = m_rectESProgram->uniformLocation("pixelratio");
    m_rect_ES_ext_points_loc = m_rectESProgram->uniformLocation("ext_points");

    m_rectESVao = new QOpenGLVertexArrayObject;
    if (m_rectESVao->create() == false)
        qDebug("VAO error!\n");

    m_rect_ES_x_Vbo = new QOpenGLBuffer;
    m_rect_ES_x_Vbo->create();
    m_rect_ES_y_Vbo = new QOpenGLBuffer;
    m_rect_ES_y_Vbo->create();

    m_rectESVao->release();
    m_rectESProgram->release();

    m_circleESProgram = new QOpenGLShaderProgram;
    if (m_circleESProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/Shaders/Circle_ES.vsh") == false)
        qDebug("Error compiling Vertex Shader");
    if (m_circleESProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/Shaders/Circle_ES.fsh") == false)
        qDebug("Error compiling Fragment Shader");
    if (m_circleESProgram->link() == false)
        qDebug("Shaders were not linked correctly");
    if (m_circleESProgram->bind() == false)
        qDebug("Shaders not correctly bound");
    m_circle_ES_in_color_loc = m_circleESProgram->uniformLocation("in_color");
    m_circle_ES_in_params_loc = m_circleESProgram->uniformLocation("in_params");
    m_circle_ES_transfMatrix_loc = m_circleESProgram->uniformLocation("transf");
    m_circle_ES_pixelratio_loc = m_circleESProgram->uniformLocation("pixelratio");
    m_circle_ES_ext_points_loc = m_circleESProgram->uniformLocation("ext_points");

    m_circleESVao = new QOpenGLVertexArrayObject;
    if (m_circleESVao->create() == false)
        qDebug("VAO error!\n");

    m_circle_ES_x_Vbo = new QOpenGLBuffer;
    m_circle_ES_x_Vbo->create();
    m_circle_ES_y_Vbo = new QOpenGLBuffer;
    m_circle_ES_y_Vbo->create();

    m_circleESVao->release();
    m_circleESProgram->release();


/*    m_rectProgram = new QOpenGLShaderProgram;
    if (m_rectProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/Shaders/Rect.vsh") == false)
        qDebug("Error compiling Vertex Shader");
    if (m_rectProgram->addShaderFromSourceFile(QOpenGLShader::Geometry, ":/shaders/Shaders/Rect.gsh") == false)
        qDebug("Error compiling Geometry Shader");
    if (m_rectProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/Shaders/Rect.fsh") == false)
        qDebug("Error compiling Fragment Shader");

    if (m_rectProgram->link() == false)
        qDebug("Shaders were not linked correctly");
    if (m_rectProgram->bind() == false)
        qDebug("Shaders not correctly bound");

    m_rect_in_color_loc = m_rectProgram->uniformLocation("in_color");
    m_rect_line_size_loc = m_rectProgram->uniformLocation("in_params");
    m_rect_transfMatrix_loc = m_rectProgram->uniformLocation("transf");
    m_rect_pixelratio_loc = m_rectProgram->uniformLocation("pixelratio");

    m_rectVao = new QOpenGLVertexArrayObject;
    if (m_rectVao->create() == false)
        qDebug("VAO error!\n");

    m_line_x_Vbo = new QOpenGLBuffer;
    m_line_x_Vbo->create();

    m_rectVao->release();
    m_rectProgram->release();

    m_circleProgram = new QOpenGLShaderProgram;
    if (m_circleProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/Shaders/Circle.vsh") == false)
        qDebug("Error compiling Vertex Shader");
    if (m_circleProgram->addShaderFromSourceFile(QOpenGLShader::Geometry, ":/shaders/Shaders/Circle.gsh") == false)
        qDebug("Error compiling Geometry Shader");
    if (m_circleProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/Shaders/Circle.fsh") == false)
        qDebug("Error compiling Fragment Shader");

    if (m_circleProgram->link() == false)
        qDebug("Shaders were not linked correctly");
    if (m_circleProgram->bind() == false)
        qDebug("Shaders not correctly bound");

    m_circle_in_color_loc = m_circleProgram->uniformLocation("in_color");
    m_circle_line_size_loc = m_circleProgram->uniformLocation("in_params");
    m_circle_transfMatrix_loc = m_circleProgram->uniformLocation("transf");
    m_circle_pixelratio_loc = m_circleProgram->uniformLocation("pixelratio");

    m_circleVao = new QOpenGLVertexArrayObject;
    if (m_circleVao->create() == false)
        qDebug("VAO error!\n");

    m_circle_Vbo = new QOpenGLBuffer;
    m_circle_Vbo->create();

    m_circleVao->release();
    m_circleProgram->release();*/
}

void toolobject::setProperties(board_properties p)
{
    properties = p;
    normalSize.setWidth(properties.full_width);
    normalSize.setHeight(properties.full_height);
}

void toolobject::setObjectSize(int v)
{
    if (v > object_max_size)
        v = object_max_size;
    if (v < object_min_size)
        v = object_min_size;

    object_size = v;
}

void toolobject::highlight()
{
    if ((status == INCREASING) || (status == HIGHLIGHTED))
        return;

    status = INCREASING;
    animationTimer.stop();
    animationTimer.start();
}

void toolobject::normal()
{
    if ((status == DECREASING) || (status == NORMAL))
        return;

    status = DECREASING;
    animationTimer.stop();
    animationTimer.start();
}

void toolobject::animationTriggered()
{
    update_animation(animationTimer.interval());

    if (status == INCREASING)
        color.setAlphaF(1.0f);
    else
        color.setAlphaF(0.3f);

    if ((status == INCREASING) & (animationValue >= 0.97f))
    {
        animationValue = 1.0f;
        status = HIGHLIGHTED;
        animationTimer.stop();
    }

    if ((status == DECREASING) & (animationValue <= 0.03f))
    {
        animationValue = 0.0f;
        status = NORMAL;
        animationTimer.stop();
    }

    emit updated();
}

void toolobject::renderRectGL(QOpenGLFunctions *f, QRect r, QColor col, int line_size, int radius, QRect screen, float devicePixelRatio)
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    QVector<float> point_data;
    r = checkQRect(r);
    point_data.append(r.left()); point_data.append(r.top());
    point_data.append(r.right()); point_data.append(r.bottom());
    QVector4D ext_points(r.left(), r.top(), r.right(), r.bottom());
    QVector<float> x_buff, y_buff;
    x_buff.append(r.left()); x_buff.append(r.left()); x_buff.append(r.right()); x_buff.append(r.right());
    y_buff.append(r.top()); y_buff.append(r.bottom()); y_buff.append(r.top()); y_buff.append(r.bottom());

    m_rectESProgram->bind();
    m_rectESVao->bind();

    m_rect_ES_x_Vbo->bind();
    m_rect_ES_x_Vbo->allocate(x_buff.data(), x_buff.count() * sizeof(float));
    f->glEnableVertexAttribArray(0);
    f->glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, sizeof(float), reinterpret_cast<void*>(0));

    m_rect_ES_y_Vbo->bind();
    m_rect_ES_y_Vbo->allocate(y_buff.data(), y_buff.count() * sizeof(float));
    f->glEnableVertexAttribArray(1);
    f->glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(float), reinterpret_cast<void*>(0));

    m_rectESProgram->setUniformValue(m_rect_ES_in_color_loc, col);
    m_rectESProgram->setUniformValue(m_rect_ES_in_params_loc, QVector4D(line_size, screen.width(), screen.height(), radius));
    QMatrix4x4 id; id.setToIdentity();
    m_rectESProgram->setUniformValue(m_rect_ES_transfMatrix_loc, id);
    m_rectESProgram->setUniformValue(m_rect_ES_pixelratio_loc, devicePixelRatio);
    m_rectESProgram->setUniformValue(m_rect_ES_ext_points_loc, ext_points);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    m_rect_ES_x_Vbo->release();
    m_rect_ES_y_Vbo->release();
    m_rectESVao->release();
    m_rectESProgram->release();

/*
    m_rectProgram->bind();
    m_rectVao->bind();

    m_line_x_Vbo->bind();
    m_line_x_Vbo->allocate(point_data.data(), point_data.count() * sizeof(float));
    f->glEnableVertexAttribArray(0);
    f->glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), reinterpret_cast<void*>(0));

    m_rectProgram->setUniformValue(m_rect_in_color_loc, col);
    m_rectProgram->setUniformValue(m_rect_line_size_loc, QVector4D(line_size, screen.width(), screen.height(), radius));
    QMatrix4x4 id; id.setToIdentity();
    m_rectProgram->setUniformValue(m_rect_transfMatrix_loc, id);
    m_rectProgram->setUniformValue(m_rect_pixelratio_loc, devicePixelRatio);
    glDrawArrays(GL_LINES, 0, 2);

    m_line_x_Vbo->release();
    m_rectVao->release();
    m_rectProgram->release();*/
}

void toolobject::renderCircleGL(QOpenGLFunctions *f, QRect r, QColor col, int line_size, QRect screen, float devicePixelRatio, float shade)
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    QVector<float> point_data;
    r = checkQRect(r);
    point_data.append(r.left()); point_data.append(r.top());
    point_data.append(r.right()); point_data.append(r.bottom());
    QVector4D ext_points(r.left(), r.top(), r.right(), r.bottom());
    QVector<float> x_buff, y_buff;
    x_buff.append(r.left()); x_buff.append(r.left()); x_buff.append(r.right()); x_buff.append(r.right());
    y_buff.append(r.top()); y_buff.append(r.bottom()); y_buff.append(r.top()); y_buff.append(r.bottom());

    m_circleESProgram->bind();
    m_circleESVao->bind();

    m_circle_ES_x_Vbo->bind();
    m_circle_ES_x_Vbo->allocate(x_buff.data(), x_buff.count() * sizeof(float));
    f->glEnableVertexAttribArray(0);
    f->glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, sizeof(float), reinterpret_cast<void*>(0));

    m_circle_ES_y_Vbo->bind();
    m_circle_ES_y_Vbo->allocate(y_buff.data(), y_buff.count() * sizeof(float));
    f->glEnableVertexAttribArray(1);
    f->glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(float), reinterpret_cast<void*>(0));

    m_circleESProgram->setUniformValue(m_circle_ES_in_color_loc, col);
    m_circleESProgram->setUniformValue(m_circle_ES_in_params_loc, QVector4D(line_size, screen.width(), screen.height(), shade));
    QMatrix4x4 id; id.setToIdentity();
    m_circleESProgram->setUniformValue(m_circle_ES_transfMatrix_loc, id);
    m_circleESProgram->setUniformValue(m_circle_ES_pixelratio_loc, devicePixelRatio);
    m_circleESProgram->setUniformValue(m_circle_ES_ext_points_loc, ext_points);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    m_circle_ES_x_Vbo->release();
    m_circle_ES_y_Vbo->release();
    m_circleESVao->release();
    m_circleESProgram->release();

    /*
    m_circleProgram->bind();
    m_circleVao->bind();

    m_circle_Vbo->bind();
    m_circle_Vbo->allocate(point_data.data(), point_data.count() * sizeof(float));
    f->glEnableVertexAttribArray(0);
    f->glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), reinterpret_cast<void*>(0));

    m_circleProgram->setUniformValue(m_circle_in_color_loc, col);
    m_circleProgram->setUniformValue(m_circle_line_size_loc, QVector4D(line_size, screen.width(), screen.height(), shade));
    QMatrix4x4 id; id.setToIdentity();
    m_circleProgram->setUniformValue(m_circle_transfMatrix_loc, id);
    m_circleProgram->setUniformValue(m_circle_pixelratio_loc, devicePixelRatio);
    glDrawArrays(GL_LINES, 0, 2);

    m_circle_Vbo->release();
    m_circleVao->release();
    m_circleProgram->release();*/
}

QRect toolobject::checkQRect(QRect r)
{
    QRect o;
    o = r;
    if ((r.width() < 0) && (r.height() < 0))
    {
        o.setTopLeft(r.bottomRight());
        o.setBottomRight(r.topLeft());
    }
    if (o.width() < 0)
    {
        o.setLeft(r.right());
        o.setRight(r.left());
        o.setTop(r.top());
        o.setBottom(r.bottom());
    }
    if (o.height() < 0)
    {
        o.setBottom(r.top());
        o.setTop(r.bottom());
        o.setLeft(r.left());
        o.setRight(r.right());
    }
    return o;
}

void toolobject::update_animation(int ms)
{
    float tau = 0.05f;
    float T = (float)ms / 1000.0f;

    float A = tau * 2.0f / T;
    float B = A + 1.0f;
    float C = 1.0f - A;

    xm1 = x;
    if (status == INCREASING)
        x = 1.0f;
    else
        x = 0.0f;

    animationValue = (1.0f/B) * ((x + xm1) - (C * animationValue));
}
