/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#include "prefmanager.h"

prefmanager::prefmanager()
{
    load_default();
}

int prefmanager::saveOnFile(QString filename)
{
    QStringList parameters;

    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return -1;

    QTextStream stream(&file);  //ready to write

    parameters = convertPreferencesToStringList();
    for (int i = 0; i < parameters.count(); i++)
        stream << parameters[i];

    file.close();

    return 0;
}

int prefmanager::loadFromFile(QString filename)
{
    load_default();
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "Error loading...";
        return -1;
    }

    QStringList paramLines;

    QTextStream stream(&file);  //ready to read
    while (!stream.atEnd())
    {
        QString tmp = stream.readLine();
        paramLines.append(tmp);
    }

    file.close();

    importFromParamLines(paramLines);

    return 0;
}

board_properties prefmanager::load_default()
{
    properties.background = Qt::lightGray;

    properties.middleButtonTool = ERASE;
    properties.apply_pen_pressure = true;
    properties.eraser_default_mode = false;

    properties.grid_color = Qt::gray;
    properties.grid_line_size = 1;
    properties.grid_step = 50;
    properties.grid_style = Qt::DotLine;

    properties.showGrid = true;

    properties.selectColor = Qt::black;
    properties.selectFillColor = Qt::blue; properties.selectFillColor.setAlphaF(0.2f);
    properties.selectLineSize = 1;;
    properties.selectLineStyle = Qt::DotLine;

    properties.hiddenColor = Qt::gray; properties.hiddenColor.setAlphaF(0.5);
    properties.hidden_height = 20;
    properties.full_height = 150;
    properties.full_width = 50;

    return properties;
}

QStringList prefmanager::convertPreferencesToStringList()
{
    QStringList listParameters;
    QString temp;
    QString toolMiddleButton;

    if (properties.middleButtonTool == ERASE)
        toolMiddleButton = "ERASER";
    if (properties.middleButtonTool == SELECT)
        toolMiddleButton = "SELECTOR";

    listParameters.append("MIDDLE_BUTTON_TOOL = " + toolMiddleButton + ";");
    listParameters.append("APPLY_PEN_PRESSURE + " + convertBoolToString(properties.apply_pen_pressure) + ";");
    listParameters.append("DEFAULT_ERASE_POINTS = " + convertBoolToString(properties.eraser_default_mode) + ";");

    listParameters.append("BACKGROUND_COLOR = " + convertColorToString(properties.background) + ";");
    listParameters.append("GRID_COLOR = " + convertColorToString(properties.grid_color) + ";");
    listParameters.append("GRID_STEP = " + QString::number(properties.grid_step) + ";");
    listParameters.append("GRID_LINE_SIZE = " + QString::number(properties.grid_line_size) + ";");
    listParameters.append("GRID_PEN_STYLE = " + convertPenStyleToString(properties.grid_style) + ";");
    listParameters.append("SHOW_GRID = " + convertBoolToString(properties.showGrid) + ";");

    listParameters.append("SELECTION_COLOR = " + convertColorToString(properties.selectColor) + ";");
    listParameters.append("SELECTION_FILL_COLOR = " + convertColorToString(properties.selectFillColor) + ";");
    listParameters.append("SELECTION_LINE_SIZE = " + QString::number(properties.selectLineSize) + ";");
    listParameters.append("SELECTION_PEN_STYLE = " + convertPenStyleToString(properties.selectLineStyle) + ";");

    listParameters.append("TOOLBOX_HIDDEN_COLOR = " + convertColorToString(properties.hiddenColor) + ";");
    listParameters.append("TOOLBOX_FULL_HEIGHT = " + QString::number(properties.full_height) + ";");
    listParameters.append("TOOLBOX_FULL_WIDTH = " + QString::number(properties.full_width) + ";");
    listParameters.append("TOOLBOX_HIDDEN_SIZE = " + QString::number(properties.hidden_height) + ";");

    //Adds end of line characters
    for (int i = 0; i < listParameters.count(); i++)
    {
        temp = listParameters[i];
        temp = temp.insert(temp.length(), QChar::CarriageReturn);
        temp = temp.insert(temp.length(), QChar::LineFeed);
        listParameters[i] = temp;
    }

    return listParameters;
}

bool prefmanager::convertArgumentToColor(QString arg, QColor *c)
{
    bool ok;

    //a valid argument has 9 characters, starting with #
    if (arg.length() != 9)
        return false;

    if (arg.at(0) != '#')
        return false;

    arg = arg.mid(1, arg.length() - 1);

    uint32_t value = arg.toUInt(&ok, 16);
    if (ok == false)
        return false;

    uint8_t r, g, b, a;
    r = (uint8_t)(uint32_t)((value & 0xFF000000) >> 24);
    g = (uint8_t)(uint32_t)((value & 0x00FF0000) >> 16);
    b = (uint8_t)(uint32_t)((value & 0x0000FF00) >> 8);
    a = (uint8_t)(uint32_t)(value & 0x000000FF);

    QColor color;
    color.setRed(r);
    color.setGreen(g);
    color.setBlue(b);
    color.setAlpha(a);

    *c = color;

    return true;
}

bool prefmanager::convertArgumentToBool(QString arg, bool *out)
{
    if (arg == "FALSE")
    {
        *out = false;
        return true;
    }
    if (arg == "TRUE")
    {
        *out = true;
        return true;
    }
    return false;
}

bool prefmanager::convertArgumentToFloat(QString arg, float min, float max, float *v)
{
    bool ok;

    float value = arg.toFloat(&ok);
    if (ok == false)
        return false;
    if ((value < min) || (value > max))
        return false;

    *v = value;

    return true;
}

bool prefmanager::convertArgumentToInt(QString arg, int min, int max, int *v)
{
    bool ok;

    int value = arg.toInt(&ok);
    if (ok == false)
        return false;
    if ((value < min) || (value > max))
        return false;

    *v = value;

    return true;
}

bool prefmanager::convertArgumentToPenStyle(QString arg, Qt::PenStyle *p)
{
    Qt::PenStyle style;

    style = Qt::PenStyle::NoPen;

    if (QString::compare(arg, "SOLID_LINE"))
        style = Qt::PenStyle::SolidLine;
    if (QString::compare(arg, "DOT_LINE"))
        style = Qt::PenStyle::SolidLine;
    if (QString::compare(arg, "DASH_LINE"))
        style = Qt::PenStyle::SolidLine;
    if (QString::compare(arg, "DASH_DOT_LINE"))
        style = Qt::PenStyle::SolidLine;
    if (QString::compare(arg, "DASH_DOT_DOT_LINE"))
        style = Qt::PenStyle::SolidLine;

    if (style != Qt::PenStyle::NoPen)
    {
        *p = style;
        return true;
    }
    else
        return false;
}

bool prefmanager::convertArgumentToToolType(QString arg, tools *t)
{
    tools type;

    type = ERASE;

    if (QString::compare(arg, "ERASER"))
        type = ERASE;
    if (QString::compare(arg, "SELECTOR"))
        type = SELECT;

    *t = type;
    return true;
}

QString prefmanager::convertColorToString(QColor c)
{
    uint8_t r,g,b,a;
    r = c.red(); g = c.green(); b = c.blue(); a = c.alpha();

    QString ret = "#" + QString("%1").arg(r,2,16,QChar('0')) + QString("%1").arg(g,2,16,QChar('0')) + QString("%1").arg(b,2,16,QChar('0')) + QString("%1").arg(a,2,16,QChar('0'));
    ret = ret.toUpper();

    return ret;
}

QString prefmanager::convertBoolToString(bool v)
{
    if (v == true)
        return "TRUE";
    else
        return "FALSE";
}

QString prefmanager::convertPenStyleToString(Qt::PenStyle p)
{
    QString out = "";
    switch (p)
    {
    case Qt::PenStyle::SolidLine:
        out = "SOLID_LINE";
        break;
    case Qt::PenStyle::DotLine:
        out = "DOT_LINE";
        break;
    case Qt::PenStyle::DashLine:
        out = "DASH_LINE";
        break;
    case Qt::PenStyle::DashDotLine:
        out = "DASH_DOT_LINE";
        break;
    case Qt::PenStyle::DashDotDotLine:
        out = "DASH_DOT_DOT_LINE";
        break;
    default:
        out ="UNKNOWN";
        break;
    }

    return out;
}

bool prefmanager::getArgumentsFromLine(QString line, QString *arg1, QString *arg2)
{
    //out of a line finds and returns the two arguments
    //search for the equal sign
    QString a1, a2;

    bool found = false; int equal_idx = -1;
    for (int i = 0; i < line.length(); i++)
        if (line.at(i) == '=')
        {
            found = true;
            equal_idx = i;
        }
    if ((found == false) || (equal_idx == 0))
        return false;

    a1 = line.mid(0, equal_idx);  //the equal sign is not included in the argument string

    //search for the semicolon sign
    found = false; int end_idx = -1;
    for (int i = equal_idx; i < line.length(); i++)
        if (line.at(i)  == ';')
        {
            found = true;
            end_idx = i;
        }
    if ((found == false) || (end_idx == equal_idx))
        return false;

    a2 = line.mid(equal_idx + 1, end_idx - equal_idx - 1);  //the semicolon sign is not included in the argument string

    //removes spaces
    a1 = a1.replace(" ", "");
    a2 = a2.replace(" ", "");

    //check if they are empty strings
    if ((a1.length() == 0) || (a2.length() == 0))
        return false;

    *arg1 = a1;
    *arg2 = a2;

    return true;
}

void prefmanager::importFromParamLines(QStringList params)
{
    QString arg1, arg2;

    for (int i = 0; i < params.length(); i++)
        if (getArgumentsFromLine(params[i], &arg1, &arg2) == true)
            processParameterLine(arg1, arg2);
}

void prefmanager::processParameterLine(QString arg1, QString arg2)
{
    QColor tmp_col; int tmp_int; bool tmp_bool; Qt::PenStyle tmp_style; tools type;
    arg1 = arg1.toUpper(); arg2 = arg2.toUpper();

    if (arg1 == "BACKGROUND_COLOR")
        if (convertArgumentToColor(arg2, &tmp_col) == true)
            properties.background = tmp_col;
    if (arg1 == "MIDDLE_BUTTON_TOOL")
        if (convertArgumentToToolType(arg2, &type) == true)
            properties.middleButtonTool = type;
    if (arg1 == "DEFAULT_ERASE_POINTS")
        if (convertArgumentToBool(arg2, &tmp_bool) == true)
            properties.eraser_default_mode = tmp_bool;

    if (arg1 == "APPLY_PEN_PRESSURE")
        if (convertArgumentToBool(arg2, &tmp_bool) == true)
            properties.apply_pen_pressure = tmp_bool;
    if (arg1 == "GRID_COLOR")
        if (convertArgumentToColor(arg2, &tmp_col) == true)
            properties.grid_color = tmp_col;
    if (arg1 == "GRID_STEP")
        if (convertArgumentToInt(arg2, 1, 10000, &tmp_int) == true)
            properties.grid_step = tmp_int;
    if (arg1 == "GRID_LINE_SIZE")
        if (convertArgumentToInt(arg2, 1, 100, &tmp_int) == true)
            properties.grid_line_size = tmp_int;
    if (arg1 == "GRID_PEN_STYLE")
        if (convertArgumentToPenStyle(arg2, &tmp_style) == true)
            properties.grid_style = tmp_style;
    if (arg1 == "SHOW_GRID")
        if (convertArgumentToBool(arg2, &tmp_bool) == true)
            properties.showGrid = tmp_bool;

    if (arg1 == "SELECTION_COLOR")
        if (convertArgumentToColor(arg2, &tmp_col) == true)
            properties.selectColor = tmp_col;
    if (arg1 == "SELECTION_FILL_COLOR")
        if (convertArgumentToColor(arg2, &tmp_col) == true)
            properties.selectFillColor = tmp_col;
    if (arg1 == "SELECTION_LINE_SIZE")
        if (convertArgumentToInt(arg2, 1, 100, &tmp_int) == true)
            properties.selectLineSize = tmp_int;
    if (arg1 == "SELECTION_PEN_STYLE")
        if (convertArgumentToPenStyle(arg2, &tmp_style) == true)
            properties.selectLineStyle = tmp_style;

    if (arg1 == "TOOLBOX_HIDDEN_COLOR")
        if (convertArgumentToColor(arg2, &tmp_col) == true)
            properties.hiddenColor = tmp_col;
    if (arg1 == "TOOLBOX_FULL_HEIGHT")
        if (convertArgumentToInt(arg2, 10, 5000, &tmp_int) == true)
            properties.full_height = tmp_int;
    if (arg1 == "TOOLBOX_FULL_WIDTH")
        if (convertArgumentToInt(arg2, 10, 5000, &tmp_int) == true)
            properties.full_width = tmp_int;
    if (arg1 == "TOOLBOX_HIDDEN_SIZE")
        if (convertArgumentToInt(arg2, 1, 5000, &tmp_int) == true)
            properties.hidden_height = tmp_int;
}
