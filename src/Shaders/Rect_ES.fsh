#version 310 es

#ifdef GL_ES
// Set default precision to highp
precision highp int;
precision highp float;
#endif

in vec4 frag_color;
in float margin;
in float radius;
in vec4 frag_points;
in float frag_pixelratio;
in vec4 frag_params;

out vec4 fragmentColor;

void main() {
    float left = frag_points.x;
    float top = frag_points.y;
    float right = frag_points.z;
    float bottom = frag_points.w;
    float x, y;
    float width, height;
    float a, b, c, d;

    height = frag_params.z * frag_pixelratio;

    x = gl_FragCoord.x / frag_pixelratio; y = (height - gl_FragCoord.y) / frag_pixelratio;

    if (margin != -1.0f)
    {
        if (radius >= 0.0f) // => bounded rectangle
        {
            //center along x
            width = abs(right - left);
            height = abs(bottom - top);
            x = x - left - (width / 2.0f);
            y = y - top - (height / 2.0f);

            left = -width / 2.0f; right = width / 2.0f;
            top = -height / 2.0f; bottom = height / 2.0f;
            a = abs(right - radius);
            b = abs(bottom - radius);
            c = radius;
            d = radius - margin;

            //equation of rounded rectangle
            if (((pow(max(abs(x)-a,0.0f),2.0f) + pow(max(abs(y)-b,0.0f),2.0f)) >= pow(c,2.0f)) ||
                ((pow(max(abs(x)-a,0.0f),2.0f) + pow(max(abs(y)-b,0.0f),2.0f)) <= pow(d,2.0f)))
                discard;
            else
                fragmentColor = frag_color;
        }
        else  // => empty rectangle
        {
            if ((x > (left + margin)) && (x < (right - margin)) && (y > (top + margin)) && (y < (bottom - margin)))
            {
                discard;
            }
            fragmentColor = frag_color;
        }
    }
    else  // => full rectangle
    {
        if (radius >= 0.0f) // => bounded rectangle
        {
            //center along x
            width = abs(right - left);
            height = abs(bottom - top);
            x = x - left - (width / 2.0f);
            y = y - top - (height / 2.0f);

            left = -width / 2.0f; right = width / 2.0f;
            top = -height / 2.0f; bottom = height / 2.0f;
            a = abs(right - radius);
            b = abs(bottom - radius);
            c = radius;

            if (((max(abs(x)-a,0.0f)*max(abs(x)-a,0.0f)) + (max(abs(y)-b,0.0f)*max(abs(y)-b,0.0f))) > (c*c))
                discard;
            else
                fragmentColor = frag_color;
        }
        else  // => empty rectangle
        {
            fragmentColor = frag_color;
        }
    }
}
