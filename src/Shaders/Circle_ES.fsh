/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#version 310 es

#ifdef GL_ES
// Set default precision to highp
precision highp int;
precision highp float;
#endif

in highp vec4 frag_color;
in highp float margin;
in highp vec4 frag_points;
in highp float frag_pixelratio;
in highp float shade;
in highp vec4 frag_params;

out highp vec4 fragmentColor;

void main() {
    float x, y;
    float left = frag_points.x;
    float top = frag_points.y;
    float right = frag_points.z;
    float bottom = frag_points.w;
    float radius;
    float distance;
    float alfa_scale;
    vec4 col;
    float height;

    float center_x, center_y;

    center_x = (left + right) / 2.0f;
    center_y = (top + bottom) / 2.0f;
    radius = (right - left) / 2.0f;

    height = frag_params.z * frag_pixelratio;
    x = gl_FragCoord.x / frag_pixelratio; y = (height - gl_FragCoord.y) / frag_pixelratio;

    if (margin != -1.0f)  //hollow circle
    {
        if (((((x - center_x)*(x - center_x)) + ((y - center_y)*(y - center_y))) <= (radius * radius)) &&
                ((((x - center_x)*(x - center_x)) + ((y - center_y)*(y - center_y))) >= ((radius - margin) * (radius - margin))))
            fragmentColor = frag_color;
    }
    else  // => full circle
    {
        if ((((x - center_x)*(x - center_x)) + ((y - center_y)*(y - center_y))) <= (radius * radius))
        {
            if (shade > 0.0f)
            {
                //calculate distance from center
                distance = ((x - center_x)*(x - center_x)) + ((y - center_y)*(y - center_y));
                if ((distance / (radius * radius)) < 0.5f)
                    distance = 0.0f;
                alfa_scale = 1.0f - (distance / (radius * radius) + 0.3f);
                col = frag_color; col.a = col.a * alfa_scale;
                fragmentColor = col;
            }
            else
                fragmentColor = frag_color;
        }
        else
            discard;
    }
}
