#version 310 es
layout(location = 0) in float x_vertex;
layout(location = 1) in float y_vertex;

uniform highp vec4 in_color;
uniform highp vec3 in_params;  //[0]: width, [1]: heigth, [2]: dotted
uniform highp mat4 transf;

out vec4 frag_color;

void main(void)
{
    float width, height;
    vec4 tmp;

    width = in_params.x;
    height = in_params.y;

    tmp = transf * vec4(x_vertex, y_vertex, 0.0f, 1.0f);
    gl_Position = vec4(tmp.x * 2.0f / width - 1.0f, 1.0f - tmp.y * 2.0f / height, 0.0f, 1.0f);

    frag_color = in_color;
}
