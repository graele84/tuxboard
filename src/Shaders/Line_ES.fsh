#version 310 es

#ifdef GL_ES
// Set default precision to highp
precision highp int;
precision highp float;
#endif

in vec4 frag_color;

out vec4 fragmentColor;

void main(void)
{
    fragmentColor = frag_color;
}
