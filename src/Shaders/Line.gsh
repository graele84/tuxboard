/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#version 330
layout(lines) in;
layout(triangle_strip, max_vertices = 4) out;
in vec4 geom_color[];
in vec4 geom_params[];
in vec2 geom_startx[];

out vec4 frag_color;
out float dotted;
out vec3 start_x; //[0]: start x, [1]: start y, [2]: line size
out vec2 delta;

void main(){
   float alfa, deltax, deltay, LWX0, LWY0, LWX1, LWY1;
   float screen_width = geom_params[0].y;
   float screen_height = geom_params[0].z;
   vec2 delta_screen;

   LWX0 = geom_params[0].x / screen_width;
   LWY0 = geom_params[0].x / screen_height;
   LWX1 = geom_params[1].x / screen_width;
   LWY1 = geom_params[1].x / screen_height;
   deltax = gl_in[1].gl_Position.x - gl_in[0].gl_Position.x;
   deltay = gl_in[1].gl_Position.y - gl_in[0].gl_Position.y;
   if (deltax == 0)
      alfa = 0;
   else
      alfa = 1.5707963f - atan(deltay, deltax);

   //calculate the deltax and deltay in screen coordinates
   delta_screen.x = screen_width/2.0*(deltax);
   delta_screen.y = -screen_height/2.0*(deltay);

   float dx0 = LWX0 * cos(alfa);
   float dy0 = LWY0 * sin(alfa);
   float dx1 = LWX1 * cos(alfa);
   float dy1 = LWY1 * sin(alfa);
   gl_Position = vec4(gl_in[0].gl_Position.x - dx0, gl_in[0].gl_Position.y + dy0, 0.0, 1.0);
   frag_color = geom_color[0];
   dotted = geom_params[0].w;
   start_x.xy = geom_startx[0];
   start_x.z = geom_params[0].x;
   delta = delta_screen;
   EmitVertex();
   gl_Position = vec4(gl_in[0].gl_Position.x + dx0, gl_in[0].gl_Position.y - dy0, 0.0, 1.0);
   frag_color = geom_color[0];
   dotted = geom_params[0].w;
   start_x.xy = geom_startx[0];
   start_x.z = geom_params[0].x;
   delta = delta_screen;
   EmitVertex();
   gl_Position = vec4(gl_in[1].gl_Position.x - dx1, gl_in[1].gl_Position.y + dy1, 0.0, 1.0);
   frag_color = geom_color[1];
   dotted = geom_params[0].w;
   start_x.xy = geom_startx[0];
   start_x.z = geom_params[0].x;
   delta = delta_screen;
   EmitVertex();
   gl_Position = vec4(gl_in[1].gl_Position.x + dx1, gl_in[1].gl_Position.y - dy1, 0.0, 1.0);
   frag_color = geom_color[1];
   dotted = geom_params[0].w;
   start_x.xy = geom_startx[0];
   start_x.z = geom_params[0].x;
   delta = delta_screen;
   EmitVertex();
   EndPrimitive();
}
