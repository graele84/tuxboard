#version 310 es

#ifdef GL_ES
// Set default precision to highp
precision highp int;
precision highp float;
#endif

in vec4 frag_color;
in vec4 points;
in vec3 params;
in float scale;

out vec4 fragmentColor;

void main(void)
{
    float dash;
    float gap;
    float x, y;
    float dist;
    vec2 point;
    vec2 projectionpoint;
    float q, m1, m2;
    float x1, y1, x3, y3;
    float dotted;
    vec2 delta;
    vec2 start_x;
    float height;

    height = params.y;

    dotted = params.z;
    delta.x = points.x - points.z;
    delta.y = points.y - points.w;
    start_x.x = points.x;
    start_x.y = points.y;

    x = gl_FragCoord.x / scale;
    y = (height - gl_FragCoord.y) / scale;
    point.x = x; point.y = y;

    dash = params.z * 2.0f;
    gap = params.z * 2.0f;

    if (dotted > 0.0f)
    {
        //calculate the projection point
        if ((delta.x != 0.0f) && (delta.y != 0.0f))
        {
            m1 = delta.y / delta.x;
            m2 = -1.0f/m1;
            x1 = start_x.x; y1 = start_x.y;
            x3 = x; y3 = y;
            q = y3 - m2*x3;
            projectionpoint.x = (q - y1 + m1*x1) / (m1 - m2);
            projectionpoint.y = m1*projectionpoint.x - m1*x1 + y1;
        }
        if (delta.x == 0.0f)  //Vertical line
        {
            projectionpoint = point.xy;
            projectionpoint.x = start_x.x;
        }
        if (delta.y == 0.0f)  //Horizontal line
        {
            projectionpoint = point.xy;
            projectionpoint.y = start_x.y;
        }

        dist = length(projectionpoint.xy - start_x.xy);
        if (fract(dist / (dash + gap)) > dash/(dash + gap))
            discard;
        else
            fragmentColor = frag_color;
    }
    else
    {
        fragmentColor = frag_color;
    }
}
