/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#version 330
layout(origin_upper_left) in vec4 gl_FragCoord;
in vec4 frag_color;
in float dotted;
in vec3 start_x;
in vec2 delta;

uniform float scale;

out vec4 fragmentColor;

void main() {
    float dash;
    float gap;
    float x, y;
    float dist;
    vec2 point;
    vec2 projectionpoint;
    float q, m1, m2;
    float x1, y1, x3, y3;

    x = gl_FragCoord.x / scale;
    y = gl_FragCoord.y / scale;
    point.x = x; point.y = y;

    dash = start_x.z;
    gap = start_x.z;

    if (dotted > 0)
    {
        //calculate the projection point
        if ((delta.x != 0) && (delta.y != 0))
        {
            m1 = delta.y / delta.x;
            m2 = -1/m1;
            x1 = start_x.x; y1 = start_x.y;
            x3 = x; y3 = y;
            q = y3 - m2*x3;
            projectionpoint.x = (q - y1 + m1*x1) / (m1 - m2);
            projectionpoint.y = m1*projectionpoint.x - m1*x1 + y1;
        }
        if (delta.x == 0)  //Vertical line
        {
            projectionpoint = point.xy;
            projectionpoint.x = start_x.x;
        }
        if (delta.y == 0)  //Horizontal line
        {
            projectionpoint = point.xy;
            projectionpoint.y = start_x.y;
        }

        dist = length(projectionpoint.xy - start_x.xy);
        if (fract(dist / (dash + gap)) > dash/(dash + gap))
            discard;
        else
            fragmentColor = frag_color;
    }
    else
    {
        fragmentColor = frag_color;
    }
}
