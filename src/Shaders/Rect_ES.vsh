#version 310 es
layout(location = 0) in float x_vertex;
layout(location = 1) in float y_vertex;

uniform highp vec4 in_color;
uniform highp vec4 in_params;  //[0]: linesize, [1]: width, [2]: height, [3]: radius
uniform highp mat4 transf;
uniform highp float pixelratio;
uniform highp vec4 ext_points;

out vec4 frag_color;
out float margin;
out float radius;
out vec4 frag_points;
out float frag_pixelratio;
out vec4 frag_params;

void main(void)
{
    float width, height;
    vec4 tmp;

    width = in_params.y;
    height = in_params.z;

    tmp = transf * vec4(x_vertex, y_vertex, 0.0f, 1.0f);
    tmp.z = 0.0f;
    gl_Position = vec4(tmp.x * 2.0f / width - 1.0f, 1.0f - tmp.y * 2.0f / height, 0.0f, 1.0f);

    frag_color = in_color;
    margin = in_params.x;
    radius = in_params.w;
    frag_pixelratio = pixelratio;
    frag_points = ext_points;
    frag_params = in_params;
}
