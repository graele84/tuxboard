/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#version 330
layout(origin_upper_left) in vec4 gl_FragCoord;
in vec4 frag_color;
in float margin;
in float radius;
in vec4 frag_points;
in float frag_pixelratio;

out vec4 fragmentColor;

void main() {
    float left = frag_points.x;
    float top = frag_points.y;
    float right = frag_points.z;
    float bottom = frag_points.w;
    float x, y;
    float width, height;
    float a, b, c, d;

    x = gl_FragCoord.x / frag_pixelratio; y = gl_FragCoord.y / frag_pixelratio;

    if (margin != -1)
    {        
        if (radius >= 0) // => bounded rectangle
        {
            //center along x
            width = abs(right - left);
            height = abs(bottom - top);
            x = x - left - (width / 2);
            y = y - top - (height / 2);

            left = -width / 2; right = width / 2;
            top = -height / 2; bottom = height / 2;
            a = abs(right - radius);
            b = abs(bottom - radius);
            c = radius;
            d = radius - margin;

            //equation of rounded rectangle
            if (((pow(max(abs(x)-a,0),2) + pow(max(abs(y)-b,0),2)) >= pow(c,2)) ||
                ((pow(max(abs(x)-a,0),2) + pow(max(abs(y)-b,0),2)) <= pow(d,2)))
                discard;
            else
                fragmentColor = frag_color;
        }
        else  // => empty rectangle
        {
            if ((x > (left + margin)) && (x < (right - margin)) && (y > (top + margin)) && (y < (bottom - margin)))
            {
                discard;
            }
            fragmentColor = frag_color;
        }
    }
    else  // => full rectangle
    {
        if (radius >= 0) // => bounded rectangle
        {
            //center along x
            width = abs(right - left);
            height = abs(bottom - top);
            x = x - left - (width / 2);
            y = y - top - (height / 2);

            left = -width / 2; right = width / 2;
            top = -height / 2; bottom = height / 2;
            a = abs(right - radius);
            b = abs(bottom - radius);
            c = radius;

            if (((max(abs(x)-a,0)*max(abs(x)-a,0)) + (max(abs(y)-b,0)*max(abs(y)-b,0))) > (c*c))
                discard;
            else
                fragmentColor = frag_color;
        }
        else  // => empty rectangle
        {
            fragmentColor = frag_color;
        }
    }
}
