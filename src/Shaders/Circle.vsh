/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#version 330
layout(location = 0) in vec2 point;

uniform highp vec4 in_color;
uniform highp vec4 in_params;  //[0]: linesize, [1]: width, [2]: height, [3]: shade
uniform highp float pixelratio;
uniform highp mat4 transf;

out vec4 geom_color;
out vec4 geom_params;
out float geom_pixelratio;

void main() {
   gl_Position = transf * vec4(point.x, point.y, 0.0, 1.0);  //bring the coordinates to screen

   gl_Position.z = 0.0;

   geom_color = in_color;
   geom_params = in_params;
   geom_pixelratio = pixelratio;
}
