/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#version 330
layout(location = 0) in float x_vertex;
layout(location = 1) in float y_vertex;
layout(location = 2) in float pressure;

uniform highp vec4 in_color;
uniform highp vec3 line_size;  //after contains width and height
uniform highp mat4 transf;
uniform float apply_pressure;

out vec4 geom_color;
out vec4 geom_params;

void main() {
   float width, height;
   vec4 pass_params;

   float scale; //this can be easily taken from the first element of the transformation matrix
   scale = transf[0][0];

   gl_Position = transf * vec4(x_vertex, y_vertex, 0.0, 1.0);  //bring the coordinates to screen

   pass_params = vec4(line_size, scale);
   if (apply_pressure > 0)
       pass_params.x = pass_params.x + (pass_params.x * pressure * 2.0);  //pass the right line_size already
   else
       pass_params.x = pass_params.x;

   gl_Position.z = 0.0;
   geom_color = in_color;
   geom_params = pass_params;
}
