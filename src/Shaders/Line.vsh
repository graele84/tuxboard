/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#version 330
layout(location = 0) in float x_vertex;
layout(location = 1) in float y_vertex;
layout(location = 2) in float pressure;

uniform highp vec4 in_color;
uniform highp vec4 line_size;  //[0]: linesize, [1]: width, [2]: heigth, [3]: dotted
uniform highp mat4 transf;
uniform float apply_pressure;

out vec4 geom_color;
out vec4 geom_params;
out vec2 geom_startx;

void main() {
   float width, height;
   vec4 pass_params;
   vec4 tmp;
   float scale; //this can be easily taken from the first element of the transformation matrix

   width = line_size.y;
   height = line_size.z;
   scale = transf[0][0];
   tmp = transf * vec4(x_vertex, y_vertex, 0.0, 1.0);
   gl_Position = vec4(tmp.x * 2.0 / width - 1.0, 1 - tmp.y * 2.0 / height, 0.0, 1.0);

   pass_params = line_size;
   if (apply_pressure > 0)
       pass_params.x = scale * (pass_params.x + (pass_params.x * pressure * 2.0));
   else
       pass_params.x = scale * pass_params.x;

   gl_Position.z = 0.0;
   geom_color = in_color;
   geom_params = pass_params;
   geom_startx.xy = tmp.xy;
}
