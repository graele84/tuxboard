/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#version 330
layout(lines) in;
layout(triangle_strip, max_vertices = 4) out;
in vec4 geom_color[];
in vec4 geom_params[];
in float geom_pixelratio[];

out vec4 frag_color;
out float margin;
out vec4 frag_points;
out float shade;
out float frag_pixelratio;

void main(){
   float width;
   float height;
   float x, y;
   vec4 ext_points;

   ext_points.x = gl_in[0].gl_Position.x;
   ext_points.y = gl_in[0].gl_Position.y;
   ext_points.z = gl_in[1].gl_Position.x;
   ext_points.w = gl_in[1].gl_Position.y;

   width = geom_params[0].y;
   height = geom_params[0].z;

   x = gl_in[0].gl_Position.x;
   y = gl_in[0].gl_Position.y;
   gl_Position = vec4(x * 2.0 / width - 1.0, 1 - y * 2.0 / height, 0.0, 1.0);
   frag_color = geom_color[0];
   margin = geom_params[0].x;
   shade = geom_params[0].w;
   frag_pixelratio = geom_pixelratio[0];
   frag_points = ext_points;
   EmitVertex();

   x = gl_in[0].gl_Position.x;
   y = gl_in[1].gl_Position.y;
   gl_Position = vec4(x * 2.0 / width - 1.0, 1 - y * 2.0 / height, 0.0, 1.0);
   frag_color = geom_color[0];
   margin = geom_params[0].x;
   shade = geom_params[0].w;
   frag_pixelratio = geom_pixelratio[0];
   frag_points = ext_points;
   EmitVertex();

   x = gl_in[1].gl_Position.x;
   y = gl_in[0].gl_Position.y;
   gl_Position = vec4(x * 2.0 / width - 1.0, 1 - y * 2.0 / height, 0.0, 1.0);
   frag_color = geom_color[0];
   margin = geom_params[0].x;
   shade = geom_params[0].w;
   frag_pixelratio = geom_pixelratio[0];
   frag_points = ext_points;
   EmitVertex();

   x = gl_in[1].gl_Position.x;
   y = gl_in[1].gl_Position.y;
   gl_Position = vec4(x * 2.0 / width - 1.0, 1 - y * 2.0 / height, 0.0, 1.0);
   frag_color = geom_color[0];
   margin = geom_params[0].x;
   shade = geom_params[0].w;
   frag_pixelratio = geom_pixelratio[0];
   frag_points = ext_points;
   EmitVertex();

   EndPrimitive();
}
