/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#version 330
layout(points) in;
layout(triangle_strip, max_vertices = 30) out;
in vec4 geom_color[];
in vec4 geom_params[];

out vec4 frag_color;

void main(){
   float radius;
   float width;
   float height;
   int i;
   float x, y;
   float scale;
   int step;

   radius = geom_params[0].x / 2;
   width = geom_params[0].y;
   height = geom_params[0].z;
   scale = geom_params[0].w;
   radius = radius * scale;
   step = 36;

   for (i = 0; i < 10; i++)
   {
       //emit the center
       x = gl_in[0].gl_Position.x;
       y = gl_in[0].gl_Position.y;
       gl_Position = vec4(x * 2.0 / width - 1.0, 1 - y * 2.0 / height, 0.0, 1.0);
       frag_color = geom_color[0];
       EmitVertex();

       //emit first point
       x = radius * cos(i * step / 360.0 * 2.0 * 3.14159265359) + gl_in[0].gl_Position.x;
       y = radius * sin(i * step / 360.0 * 2.0 * 3.14159265359) + gl_in[0].gl_Position.y;
       gl_Position = vec4(x * 2.0 / width - 1.0, 1 - y * 2.0 / height, 0.0, 1.0);
       frag_color = geom_color[0];
       EmitVertex();

       //emit second point
       x = radius * cos((i + 1) * step / 360.0 * 2.0 * 3.14159265359) + gl_in[0].gl_Position.x;
       y = radius * sin((i + 1) * step / 360.0 * 2.0 * 3.14159265359) + gl_in[0].gl_Position.y;
       gl_Position = vec4(x * 2.0 / width - 1.0, 1 - y * 2.0 / height, 0.0, 1.0);
       frag_color = geom_color[0];
       EmitVertex();
   }
   EndPrimitive();
}
