/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#include "mainwindow.h"

#include <QApplication>
#include <QPalette>
#include <QPixmap>
#include <QSplashScreen>
#include <QTime>

void delay(int n);

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QApplication::setWindowIcon(QIcon(":/icons/Resources/tuxboard_logo.png"));
    QCoreApplication::setApplicationName("tuxBoard v1.0");

    QSurfaceFormat fmt;
    fmt.setDepthBufferSize(24);
#ifndef OPENGL_ES
    fmt.setVersion(3, 1);
    fmt.setProfile(QSurfaceFormat::CoreProfile);
#else
    fmt.setVersion(2, 0);
#endif
    fmt.setSwapInterval(0);
    fmt.setSamples(16);
    fmt.setRenderableType(QSurfaceFormat::OpenGLES);
    QSurfaceFormat::setDefaultFormat(fmt);    

    //Load splash screen
    QPixmap logo(":/icons/Resources/tuxboard_logo.png");
    logo = logo.scaled(250, 250, Qt::KeepAspectRatio);
    QSplashScreen loadWdw(logo);
    loadWdw.setWindowFlags(loadWdw.windowFlags() | Qt::WindowStaysOnTopHint);
    loadWdw.show();
    loadWdw.showMessage("Loading...", Qt::AlignBottom);

    delay(1000);
    a.processEvents();
    loadWdw.close();

    //start application
    MainWindow w;

    return a.exec();
}

void delay(int n)
{
    QTime dieTime = QTime::currentTime().addMSecs(n);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}
