/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#include "textpopup.h"

textPopup::textPopup(QObject *parent) : QObject(parent)
{    
    shown = false;

    load_default_values();

    popupRect.setSize(QSize(150, 100));

    animationTimer.setInterval(5);
    connect(&animationTimer, &QTimer::timeout, this, &textPopup::animationTriggered);
    animationValue = 0.0f;  //it's hidden
    x = 0.0f; xm1 = 0.0f;
}

void textPopup::paint(QPainter &painter, QRect rect)
{
    if (shown == true)
    {
        painter.setFont(textFont);
        //QRectF br = painter.boundingRect(QRect(10,10,100,100), Qt::AlignVCenter | Qt::AlignCenter, text);
        QRectF br = QFontMetrics(textFont).boundingRect(text);
        float scale = 1.61803398875f;  //golden ratio
        float w, h;
        w = br.width() * scale;
        h = br.height() * scale;
        //remember => decreasing says if the animation is going up or down => when going down, instead of animationvalue, use 1-animation value so we get a slower disappear
        QPen pen(rectColor, rectLineSize, rectPenStyle);
        QBrush brush(backgroundColor);
        painter.setPen(pen);
        painter.setBrush(brush);
        QPainterPath path;
        popupRect = QRect(0,0,w,h);
        calculateRectToParent(rect);
        popupRectToParent.setHeight(h * animationValue);
        popupRectToParent.setWidth(w);
        path.addRoundedRect(popupRectToParent, 10, 10);
        painter.drawPath(path);

        painter.setPen(textColor);
        painter.drawText(popupRectToParent, Qt::AlignCenter | Qt::AlignVCenter, text);
    }
}

void textPopup::animationTriggered()
{
    if (waiting == true)
    {
        popupcounter++;
        if (popupcounter >= (popuptime / refreshTime))
        {
            waiting = false;
            decreasing = true;
            x = 0.0f;
            animationValue = 0.96f;
        }
    }
    if (waiting == false)
    {
        update_animation(animationTimer.interval());

        if (animationValue >= 0.97f)
        {
            x = 0.0f;
            waiting = true;
        }

        if (animationValue <= 0.03f)
        {
            x = 0.0f;
            animationValue = 0.0f;
            shown = false;
            animationTimer.stop();
        }
    }

    emit updated();
}

void textPopup::load_default_values()
{
    refreshTime = 15;
    popuptime = 1000;  //2 seconds
    text = " Test ";
    textColor = Qt::darkGray; textColor.setAlphaF(0.5f);
    backgroundColor = Qt::green; backgroundColor.setAlphaF(0.2f);
    rectColor = Qt::darkGreen; rectColor.setAlphaF(0.2f);
    rectLineSize = 2;
    rectPenStyle = Qt::PenStyle::DotLine;
    textSize = 32;
    textFont.setPixelSize(textSize);
}

void textPopup::calculateRectToParent(QRect rect)
{
    popupRectToParent = popupRect;
    int margin = 0;
    QPoint topleft;
    topleft.setX((rect.width() - popupRect.width()) / 2);
    topleft.setY(margin);  //it should be located at the top => or we could add a few pixels of margin
    popupRectToParent.setTopLeft(popupRectToParent.topLeft() + topleft);
    popupRectToParent.setBottomRight(popupRectToParent.bottomRight() + topleft);
}

void textPopup::update_animation(int ms)
{
    float tau;
    float T = (float)ms / 1000.0f;

    if (decreasing == true)
        tau = 0.02f;
    else
        tau = 0.02f;

    float A = tau * 2.0f / T;
    float B = A + 1.0f;
    float C = 1.0f - A;

    xm1 = x;

    animationValue = (1.0f/B) * ((x + xm1) - (C * animationValue));
}
