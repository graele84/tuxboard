/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#ifndef GLBOARD_H
#define GLBOARD_H

#include <QWindow>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>
#include <QMatrix2x2>
#include <QMatrix3x3>
#include <QMatrix4x4>
#include <QVector2D>
#include <QVector4D>

#include <QTabletEvent>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QCloseEvent>
#include <QEvent>
#include <QKeyEvent>
#include <QIODevice>
#include <QDataStream>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>

#include <QtMath>

#include <QPainter>
#include <QPaintEngine>
#include <QPainterPath>

#include <QScreen>

#include <QRegion>

#include <QVariant>

#include <QMenu>
#include <QAction>
#include <QContextMenuEvent>

#include <QtSvg/QSvgGenerator>
#include <QPdfWriter>

#include <QElapsedTimer>

#include "toolbox.h"
#include "textpopup.h"

#include "prefmanager.h"
#include "Dialogs/prefdialog.h"
#include "Dialogs/infodialog.h"
#include "common.h"

#include <QTimer>
#include <QElapsedTimer>
#include <QDebug>

#include <QRect>

//#define CUBIC_RENDER
//#define QUAD_RENDER
#define LINE_RENDER
//#define LINE_PATH
//#define ELLIPSE_RENDER

typedef struct segment_type
{
    QVector<float> points_x;
    QVector<float> points_y;
} segment_t;

typedef struct line_type
{
    QVector<float> points_x;  //all points composing a line with respect to viewRect
    QVector<float> points_y;  //not stored as QPointF so that it's better for bringing data to OpenGL buffer
    QVector<segment_t> rend_segments;  //contains all the points for the rendering
    QVector<float> rend_points_x;
    QVector<float> rend_points_y;
    QVector<float> pressures;  //all the pressures applied
    QVector<qint64> times;  //saves the time instants of those points in ns => needed if gesture speed is to be calculated
    bool apply_pressure;  //determines if pressure is to be applied while rendering
    int line_size;
    QVector<QColor> colors;  //all the colors (consider that some effects might change it along the line => a vector)
    QRectF rect;  //rect containing the line
    QMatrix4x4 zoom_shift_transf;
    bool zoom_shift_applied;
} line_t;

typedef enum mouse_location_enum
{
    SCENE = 0x00,
    TOOLBOX = 0x01,
    SELECTION_MOVE = 0x02,
    SELECTION_UP = 0x03,
    SELECTION_DOWN = 0x04,
    SELECTION_LEFT = 0x05,
    SELECTION_RIGHT = 0x06,
    SELECTION_TOPLEFT = 0x07,
    SELECTION_TOPRIGHT = 0x08,
    SELECTION_BOTTOMLEFT = 0x09,
    SELECTION_BOTTOMRIGHT = 0x0A
} mouse_location_enum;

typedef enum operation_t
{
    NONE_OP = 0x00,
    PEN_OP = 0x01,
    ERASE_OP = 0x02,
    MOVE_OP = 0x04,
    RESIZE_OP = 0x08,
    SELECT_OP = 0x10,
    PASTE_OP = 0x20,
} operation;

typedef struct scene_type
{
    QVector<line_t> lines;  //here are all the lines contained in the scene
} scene_t;

typedef struct history_freeze_t
{
    scene_t scene;
    operation event_type;
    QRectF selectArea;
    QVector<line_t> selectedLines;
    QVector<int> selectedLineIndexes;
    bool selected;
} history_freeze;

class glBoard : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

public:
    glBoard(board_properties prop, QWidget *parent = 0);
    ~glBoard();

    board_properties get_properties() { return properties; }

protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int w, int h) override;

    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;
    void leaveEvent(QEvent *event) override;

    void tabletEvent(QTabletEvent *event) override;

    void keyPressEvent(QKeyEvent *event) override;

    void contextMenuEvent(QContextMenuEvent *event) override;

    void closeEvent(QCloseEvent *event) override;

signals:
    void newBoardRequest();
    void closeRequested(glBoard *pointer);
    void propertiesChanged(board_properties prop);

private slots:
    void saveTuxTriggered();
    void loadTuxTriggered();
    void saveSVGTriggered();
    void savePDFTriggered();
    void gridActTriggered() { properties.showGrid = !properties.showGrid; gridAct->setChecked(properties.showGrid); repaint(); }
    void pressureActTriggered() { apply_pressure = !apply_pressure; pressureAct->setChecked(apply_pressure); }
    void preferenceActTriggered();
    void undoActTriggered();
    void redoActTriggered();
    void infoDlgTriggered();

    void toolboxUpdated();
    void toolChanged(tools type, QVariant data, int size);

    void propertiesUpdated(board_properties p);

    void glCleanup();

private:
    board_properties properties;
    prefmanager *preferences;

    QVector<history_freeze> history;  //contains all the scenes created since the start
    int history_index;

    //OPENGL SECTION
/*    QOpenGLContext *m_context;
    QOpenGLShaderProgram *m_lineProgram;
    QOpenGLVertexArrayObject *m_lineVao;
    QOpenGLBuffer *m_line_x_Vbo;
    QOpenGLBuffer *m_line_y_Vbo;
    QOpenGLBuffer *m_line_p_Vbo;
    QOpenGLShaderProgram *m_roundProgram;
    QOpenGLVertexArrayObject *m_roundVao;
    QOpenGLShaderProgram *m_rectProgram;
    QOpenGLVertexArrayObject *m_rectVao;*/

    QOpenGLShaderProgram *m_lineESProgram;
    QOpenGLVertexArrayObject *m_lineESVao;
    QOpenGLBuffer *m_lineES_x_Vbo;
    QOpenGLBuffer *m_lineES_y_Vbo;
    int m_ES_in_color_loc;
    int m_ES_in_params_loc;
    int m_ES_transfMatrix_loc;

    QOpenGLShaderProgram *m_segmentESProgram;
    QOpenGLVertexArrayObject *m_segmentESVao;
    QOpenGLBuffer *m_segmentES_x_Vbo;
    QOpenGLBuffer *m_segmentES_y_Vbo;
    int m_ES_seg_in_color_loc;
    int m_ES_seg_in_params_loc;
    int m_ES_seg_transfMatrix_loc;
    int m_ES_seg_in_points_loc;

    QOpenGLShaderProgram *m_rectESProgram;
    QOpenGLVertexArrayObject *m_rectESVao;
    QOpenGLBuffer *m_rect_ES_x_Vbo;
    QOpenGLBuffer *m_rect_ES_y_Vbo;
    int m_rect_ES_in_color_loc;
    int m_rect_ES_in_params_loc;
    int m_rect_ES_transfMatrix_loc;
    int m_rect_ES_pixelratio_loc;
    int m_rect_ES_ext_points_loc;

    QOpenGLShaderProgram *m_circleESProgram;
    QOpenGLVertexArrayObject *m_circleESVao;
    QOpenGLBuffer *m_circle_ES_x_Vbo;
    QOpenGLBuffer *m_circle_ES_y_Vbo;
    int m_circle_ES_in_color_loc;
    int m_circle_ES_in_params_loc;
    int m_circle_ES_transfMatrix_loc;
    int m_circle_ES_pixelratio_loc;
    int m_circle_ES_ext_points_loc;

/*    QOpenGLShaderProgram *m_circleProgram;
    QOpenGLVertexArrayObject *m_circleVao;
    QOpenGLBuffer *m_circle_Vbo;
    int m_circle_in_color_loc;
    int m_circle_line_size_loc;
    int m_circle_transfMatrix_loc;
    int m_circle_pixelratio_loc;*/

    QSurface *m_surf;
/*    int m_in_color_loc;
    int m_line_size_loc;
    int m_transfMatrix_loc;
    int m_scale_loc;
    int m_apply_pressure_loc;

    int m_round_in_color_loc;
    int m_round_line_size_loc;
    int m_round_transfMatrix_loc;
    int m_round_apply_pressure_loc;

    int m_rect_in_color_loc;
    int m_rect_line_size_loc;
    int m_rect_transfMatrix_loc;
    int m_rect_pixelratio_loc;*/

    bool busy_on_scene;
    bool on_the_fly_operation;

    QMatrix4x4 transfMatrix;
    QMatrix4x4 transfMatrixInv;  //inverse of the transfMatrix
    float scale;

    toolBox *toolbox;
    textPopup *textpop;

    QColor current_color;
    int current_line_size;
    int current_erase_size;
    bool apply_pressure;

    QElapsedTimer nsTimer;

    mouse_location_enum mouse_loc;
    QPointF current_mouse_position;
    QPointF previous_mouse_position;
    bool mouse_on_widget;

    bool updategrid;
    QVector<segment_t> grid_segments;
    QVector<QPointF> grid_points1, grid_points2;

    bool selecting;
    bool selected;
    QRectF selectArea;
    QPointF selectStartPoint;
    QVector<line_t> selectedLines;
    QVector<int> selectedLineIndexes;
    QPointF selectionStartMove;
    bool selectionMoving;
    bool selectionResizing;
    mouse_location_enum selectionResizingType;
    QPointF selectionResizeStart;

    QVector<line_t> copyBuffer;  //contains all the copied lines

    tools current_tool;

    scene_t scene;

    QPointF startDrag;

    int previous_tool;
    tools middleButtonTool;

    QMenu *popup;
    QAction *newBoardAct;
    QAction *preferenceAct;
    QAction *saveTuxAct;
    QAction *loadTuxAct;
    QAction *saveSVGAct;
    QAction *savePDFAct;
    QAction *gridAct;
    QAction *pressureAct;
    QAction *undoAct;
    QAction *redoAct;
    QAction *infoAct;

    void loadDefaultValues();

    void prepareWidget();

    void renderGrid(QPainter &painter);
    void renderGridGL(QOpenGLFunctions *f);
    qint64 renderSceneGL(QOpenGLFunctions *f);
    void renderScene(QPainter &painter);
    void renderToolbox(QPainter &painter);
    void renderToolboxGL(QOpenGLFunctions *f);
    void renderCursor(QPainter &painter);
    void renderCursorGL(QOpenGLFunctions *f);
    void renderSelection(QPainter &painter);
    void renderSelectionGL(QOpenGLFunctions *f);

    void renderRectGL(QOpenGLFunctions *f, QRect r, QColor col, int line_size, int radius);
    void renderCircleGL(QOpenGLFunctions *f, QRect r, QColor col, int line_size, float shade);

    int calculate_line_size(int line_size, float pressure, bool applypressure);

    QPainterPath get_path(line_type line);

    QMatrix4x4 get_scale_matrix(QMatrix4x4 mat);

    void save_scene_to_file(QString filename = "");
    void save_scene_to_svg(QString filename = "");
    void save_scene_to_pdf(QString filename = "");
    void load_scene_from_file(QString filename = "");

    QRect get_scene_rect();

    QRectF calculate_line_rect(line_t line);
    void erase_points(QPointF position);
    void erase_lines(QPointF position);
    QVector<line_t> split_line(line_t line, QVector<bool> toerase);

    bool circle_contains(QRectF rect, QPointF point);

    bool rects_intersect(QRectF r1, QRectF r2);

    QRectF find_elements_in_selection();
    mouse_location_enum get_mouse_location(QPointF pos);
    void move_selected_lines(QPointF diff, bool update_points);
    void resize_selected_line(QPointF diff, mouse_location_enum edge, bool update_points);
    QRectF map_to_widget(QRectF r);
    QRectF map_to_scene(QRectF r);
    QRectF adjust_selection_area();
    void delete_selected_lines();
    QRect fromQRectFtoQRect(QRectF r);
    QSize fromQSizeFtoQSize(QSizeF s);
    QRect checkQRect(QRect r);

    void add_to_history(operation type);
    void restore_history_back();
    void restore_history_forward();
    QString get_tool_name(operation t);
    void get_undo_redo_strings(QString &undo, QString &redo, bool &undo_active, bool &redo_active);

    segment_t create_segment(QPointF p1, QPointF p2, float line_size, float pressure1, float pressure2, bool apply_pressure, bool rounded);
    void join_render_segments(QVector<segment_t> *segs, QVector<float> *x, QVector<float> *y, bool clear);
    void join_render_segments(QVector<segment_t> segs, QVector<float> *x, QVector<float> *y, bool clear);
    void generate_render_segments(line_t *input);

    //action functions
    void pen_function(QTabletEvent *event);
    void erase_function(QTabletEvent *event);
    void select_function(QTabletEvent *event);
    void zoom_shift_function(QTabletEvent *event);
    void zoom_shift_function(QWheelEvent *event);
};

#endif // GLBOARD_H
