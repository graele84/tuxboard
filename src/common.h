/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#ifndef COMMON_H
#define COMMON_H

#include <QColor>

typedef enum tools
{
    NONE = 0x00,
    PEN = 0x01,
    SELECT = 0x02,
    ERASE = 0x04
} tools;

typedef struct board_properties_t
{
    QColor background;

    tools middleButtonTool;
    bool apply_pen_pressure;
    bool eraser_default_mode;  //false: erase points; true: erase lines

    //GRID
    QColor grid_color;
    int grid_step;
    int grid_line_size;
    Qt::PenStyle grid_style;
    bool showGrid;

    //SELECTION
    QColor selectColor;
    QColor selectFillColor;
    int selectLineSize;
    Qt::PenStyle selectLineStyle;

    //TOOLBOX
    QColor hiddenColor;
    int hidden_height;
    int full_height;
    int full_width;
} board_properties;

#endif // COMMON_H
