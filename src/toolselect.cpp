/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#include "toolselect.h"

toolSelect::toolSelect(board_properties p) : toolobject(p)
{
    color = Qt::black;
    color.setAlphaF(0.3f);

    emit updated();
}

void toolSelect::paint(QPainter &painter, QPoint topleft)
{
    //we paint the rect
    toolRect.setSize(normalSize * (1 + (animationValue)*(highlighted_scale - 1.0f)));
    painter.setBrush(Qt::NoBrush);
    painter.setPen(QPen(QBrush(color), 2, Qt::SolidLine));
    QRect rect;
    rect.setTopLeft(toolRect.topLeft() + topleft);
    rect.setBottomRight(toolRect.bottomRight() + topleft);
    QPainterPath path;
    path.addRoundedRect(rect, 10, 10);
    painter.drawPath(path);

    //we simply paint a circle
    QRect circle;
    int r = (int)((float)toolRect.width() / 1.61803398875f / 2.0f);

    circle.setTopLeft(QPoint(toolRect.topLeft().x() + (toolRect.width() / 2) - r + topleft.x(), toolRect.topLeft().y() + (toolRect.height() / 2) - r + topleft.y()));
    circle.setBottomRight(QPoint(toolRect.topLeft().x() + (toolRect.width() / 2) + r + topleft.x(), toolRect.topLeft().y() + (toolRect.height() / 2) + r + topleft.y()));

    painter.setBrush(Qt::NoBrush);
    painter.setPen(QPen(color, 1, Qt::DashDotDotLine));
    painter.drawRect(circle);
}

void toolSelect::paintGL(QOpenGLFunctions *f, QRect screen, QPoint topleft, float devicePixelRatio)
{
    if (gl_initialized == false)
    {
        initializeGL();
        gl_initialized = true;
    }
    //we paint the rect
    toolRect.setSize(normalSize * (1 + (animationValue)*(highlighted_scale - 1.0f)));
    QRect rect;
    rect.setTopLeft(toolRect.topLeft() + topleft);
    rect.setBottomRight(toolRect.bottomRight() + topleft);
    renderRectGL(f, rect, color, 2, 10, screen, devicePixelRatio);

    //we simply paint a rect
    QRect circle;
    int r = (int)((float)toolRect.width() / 1.61803398875f / 2.0f);

    circle.setTopLeft(QPoint(toolRect.topLeft().x() + (toolRect.width() / 2) - r + topleft.x(), toolRect.topLeft().y() + (toolRect.height() / 2) - r + topleft.y()));
    circle.setBottomRight(QPoint(toolRect.topLeft().x() + (toolRect.width() / 2) + r + topleft.x(), toolRect.topLeft().y() + (toolRect.height() / 2) + r + topleft.y()));

    renderRectGL(f, circle, color, 2, -1, screen, devicePixelRatio);
}
