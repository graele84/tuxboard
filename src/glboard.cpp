/*
 *
 * This software is called tuxBoard and it provides a simple platform for
 * drawing with tablets in Linux.
 *
 * Copyright (C) 2021 Emanuele Grasso
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
*/

#include "glboard.h"

glBoard::glBoard(board_properties prop, QWidget *parent)
{
    setParent(parent);

    properties = prop;

    loadDefaultValues();

    prepareWidget();

    setTabletTracking(true);
    setMouseTracking(true);

    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
//    resize(QSize(200,200));
    showMaximized();
    updategrid = true;
}

glBoard::~glBoard()
{
    disconnect(context(), &QOpenGLContext::aboutToBeDestroyed, this, &glBoard::glCleanup);
    glCleanup();
}

void glBoard::initializeGL()
{
    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();

    initializeOpenGLFunctions();
    QColor transparent = Qt::transparent;
#if QT_VERSION_MAJOR == 6
    float backR, backG, backB, backA;
#else
    double backR, backG, backB, backA;
#endif
    transparent.getRgbF(&backR, &backG, &backB, &backA);
    f->glClearColor(static_cast<float>(backR), static_cast<float>(backG), static_cast<float>(backB), static_cast<float>(backA));

    connect(context(), &QOpenGLContext::aboutToBeDestroyed, this, &glBoard::glCleanup);

    //LINE_ES SHADER INIT
    m_lineESProgram = new QOpenGLShaderProgram;
    if (m_lineESProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/Shaders/Line_ES.vsh") == false)
        qDebug("Error compiling Vertex Shader");
    if (m_lineESProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/Shaders/Line_ES.fsh") == false)
        qDebug("Error compiling Fragment Shader");
    if (m_lineESProgram->link() == false)
        qDebug("Shaders were not linked correctly");
    if (m_lineESProgram->bind() == false)
        qDebug("Shaders not correctly bound");
    m_ES_in_color_loc = m_lineESProgram->uniformLocation("in_color");
    m_ES_in_params_loc = m_lineESProgram->uniformLocation("in_params");
    m_ES_transfMatrix_loc = m_lineESProgram->uniformLocation("transf");
    m_lineESVao = new QOpenGLVertexArrayObject;
    if (m_lineESVao->create() == false)
        qDebug("VAO error!\n");
    m_lineESVao->bind();
    m_lineES_x_Vbo = new QOpenGLBuffer;
    m_lineES_x_Vbo->create();
    m_lineES_x_Vbo->setUsagePattern(QOpenGLBuffer::DynamicDraw);
    m_lineES_y_Vbo = new QOpenGLBuffer;
    m_lineES_y_Vbo->create();
    m_lineES_y_Vbo->setUsagePattern(QOpenGLBuffer::DynamicDraw);
    m_lineESVao->release();
    m_lineESProgram->release();

    //SEGMENT ES SHADER INIT
    m_segmentESProgram = new QOpenGLShaderProgram;
    if (m_segmentESProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/Shaders/Segment_ES.vsh") == false)
        qDebug("Error compiling Vertex Shader");
    if (m_segmentESProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/Shaders/Segment_ES.fsh") == false)
        qDebug("Error compiling Fragment Shader");
    if (m_segmentESProgram->link() == false)
        qDebug("Shaders were not linked correctly");
    if (m_segmentESProgram->bind() == false)
        qDebug("Shaders not correctly bound");
    m_ES_seg_in_color_loc = m_segmentESProgram->uniformLocation("in_color");
    m_ES_seg_in_params_loc = m_segmentESProgram->uniformLocation("in_params");
    m_ES_seg_transfMatrix_loc = m_segmentESProgram->uniformLocation("transf");
    m_ES_seg_in_points_loc = m_segmentESProgram->uniformLocation("in_points");
    m_segmentESVao = new QOpenGLVertexArrayObject;
    if (m_segmentESVao->create() == false)
        qDebug("VAO error!\n");
    m_segmentESVao->bind();
    m_segmentES_x_Vbo = new QOpenGLBuffer;
    m_segmentES_x_Vbo->create();
    m_segmentES_x_Vbo->setUsagePattern(QOpenGLBuffer::DynamicDraw);
    m_segmentES_y_Vbo = new QOpenGLBuffer;
    m_segmentES_y_Vbo->create();
    m_segmentES_y_Vbo->setUsagePattern(QOpenGLBuffer::DynamicDraw);
    m_segmentESVao->release();
    m_segmentESProgram->release();

    //RECT ES SHADER INIT
    m_rectESProgram = new QOpenGLShaderProgram;
    if (m_rectESProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/Shaders/Rect_ES.vsh") == false)
        qDebug("Error compiling Vertex Shader");
    if (m_rectESProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/Shaders/Rect_ES.fsh") == false)
        qDebug("Error compiling Fragment Shader");
    if (m_rectESProgram->link() == false)
        qDebug("Shaders were not linked correctly");
    if (m_rectESProgram->bind() == false)
        qDebug("Shaders not correctly bound");
    m_rect_ES_in_color_loc = m_rectESProgram->uniformLocation("in_color");
    m_rect_ES_in_params_loc = m_rectESProgram->uniformLocation("in_params");
    m_rect_ES_transfMatrix_loc = m_rectESProgram->uniformLocation("transf");
    m_rect_ES_pixelratio_loc = m_rectESProgram->uniformLocation("pixelratio");
    m_rect_ES_ext_points_loc = m_rectESProgram->uniformLocation("ext_points");

    m_rectESVao = new QOpenGLVertexArrayObject;
    if (m_rectESVao->create() == false)
        qDebug("VAO error!\n");

    m_rect_ES_x_Vbo = new QOpenGLBuffer;
    m_rect_ES_x_Vbo->create();
    m_rect_ES_y_Vbo = new QOpenGLBuffer;
    m_rect_ES_y_Vbo->create();

    m_rectESVao->release();
    m_rectESProgram->release();

    //CIRCLE ES SHADER INIT
    m_circleESProgram = new QOpenGLShaderProgram;
    if (m_circleESProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/Shaders/Circle_ES.vsh") == false)
        qDebug("Error compiling Vertex Shader");
    if (m_circleESProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/Shaders/Circle_ES.fsh") == false)
        qDebug("Error compiling Fragment Shader");
    if (m_circleESProgram->link() == false)
        qDebug("Shaders were not linked correctly");
    if (m_circleESProgram->bind() == false)
        qDebug("Shaders not correctly bound");
    m_circle_ES_in_color_loc = m_circleESProgram->uniformLocation("in_color");
    m_circle_ES_in_params_loc = m_circleESProgram->uniformLocation("in_params");
    m_circle_ES_transfMatrix_loc = m_circleESProgram->uniformLocation("transf");
    m_circle_ES_pixelratio_loc = m_circleESProgram->uniformLocation("pixelratio");
    m_circle_ES_ext_points_loc = m_circleESProgram->uniformLocation("ext_points");

    m_circleESVao = new QOpenGLVertexArrayObject;
    if (m_circleESVao->create() == false)
        qDebug("VAO error!\n");

    m_circle_ES_x_Vbo = new QOpenGLBuffer;
    m_circle_ES_x_Vbo->create();
    m_circle_ES_y_Vbo = new QOpenGLBuffer;
    m_circle_ES_y_Vbo->create();

    m_circleESVao->release();
    m_circleESProgram->release();

    //LINE SHADER INIT
/*    m_lineProgram = new QOpenGLShaderProgram;
    if (m_lineProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/Shaders/Line.vsh") == false)
        qDebug("Error compiling Vertex Shader");
    if (m_lineProgram->addShaderFromSourceFile(QOpenGLShader::Geometry, ":/shaders/Shaders/Line.gsh") == false)
        qDebug("Error compiling Geometry Shader");
    if (m_lineProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/Shaders/Line.fsh") == false)
        qDebug("Error compiling Fragment Shader");

    if (m_lineProgram->link() == false)
        qDebug("Shaders were not linked correctly");
    if (m_lineProgram->bind() == false)
        qDebug("Shaders not correctly bound");

    m_in_color_loc = m_lineProgram->uniformLocation("in_color");
    m_line_size_loc = m_lineProgram->uniformLocation("line_size");
    m_transfMatrix_loc = m_lineProgram->uniformLocation("transf");
    m_scale_loc = m_lineProgram->uniformLocation("scale");
    m_apply_pressure_loc = m_lineProgram->uniformLocation("apply_pressure");

    m_lineVao = new QOpenGLVertexArrayObject;
    if (m_lineVao->create() == false)
        qDebug("VAO error!\n");
    m_lineVao->bind();

    m_line_x_Vbo = new QOpenGLBuffer;
    m_line_x_Vbo->create();
    m_line_x_Vbo->setUsagePattern(QOpenGLBuffer::DynamicDraw);

    m_line_y_Vbo = new QOpenGLBuffer;
    m_line_y_Vbo->create();
    m_line_y_Vbo->setUsagePattern(QOpenGLBuffer::DynamicDraw);

    m_line_p_Vbo = new QOpenGLBuffer;
    m_line_p_Vbo->create();
    m_line_p_Vbo->setUsagePattern(QOpenGLBuffer::DynamicDraw);

    m_lineVao->release();

    m_lineProgram->release();

    m_roundProgram = new QOpenGLShaderProgram;
    if (m_roundProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/Shaders/Round.vsh") == false)
        qDebug("Error compiling Vertex Shader");
    if (m_roundProgram->addShaderFromSourceFile(QOpenGLShader::Geometry, ":/shaders/Shaders/Round.gsh") == false)
        qDebug("Error compiling Geometry Shader");
    if (m_roundProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/Shaders/Round.fsh") == false)
        qDebug("Error compiling Fragment Shader");

    if (m_roundProgram->link() == false)
        qDebug("Shaders were not linked correctly");
    if (m_roundProgram->bind() == false)
        qDebug("Shaders not correctly bound");

    m_round_in_color_loc = m_roundProgram->uniformLocation("in_color");
    m_round_line_size_loc = m_roundProgram->uniformLocation("line_size");
    m_round_transfMatrix_loc = m_roundProgram->uniformLocation("transf");
    m_round_apply_pressure_loc = m_roundProgram->uniformLocation("apply_pressure");

    m_roundVao = new QOpenGLVertexArrayObject;
    if (m_roundVao->create() == false)
        qDebug("VAO error!\n");

    m_roundVao->release();
    m_roundProgram->release();

    m_rectProgram = new QOpenGLShaderProgram;
    if (m_rectProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/Shaders/Rect.vsh") == false)
        qDebug("Error compiling Vertex Shader");
    if (m_rectProgram->addShaderFromSourceFile(QOpenGLShader::Geometry, ":/shaders/Shaders/Rect.gsh") == false)
        qDebug("Error compiling Geometry Shader");
    if (m_rectProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/Shaders/Rect.fsh") == false)
        qDebug("Error compiling Fragment Shader");

    if (m_rectProgram->link() == false)
        qDebug("Shaders were not linked correctly");
    if (m_rectProgram->bind() == false)
        qDebug("Shaders not correctly bound");

    m_rect_in_color_loc = m_rectProgram->uniformLocation("in_color");
    m_rect_line_size_loc = m_rectProgram->uniformLocation("in_params");
    m_rect_transfMatrix_loc = m_rectProgram->uniformLocation("transf");
    m_rect_pixelratio_loc = m_rectProgram->uniformLocation("pixelratio");

    m_rectVao = new QOpenGLVertexArrayObject;
    if (m_rectVao->create() == false)
        qDebug("VAO error!\n");

    m_rectVao->release();
    m_rectProgram->release();

    m_circleProgram = new QOpenGLShaderProgram;
    if (m_circleProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/Shaders/Circle.vsh") == false)
        qDebug("Error compiling Vertex Shader");
    if (m_circleProgram->addShaderFromSourceFile(QOpenGLShader::Geometry, ":/shaders/Shaders/Circle.gsh") == false)
        qDebug("Error compiling Geometry Shader");
    if (m_circleProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/Shaders/Circle.fsh") == false)
        qDebug("Error compiling Fragment Shader");

    if (m_circleProgram->link() == false)
        qDebug("Shaders were not linked correctly");
    if (m_circleProgram->bind() == false)
        qDebug("Shaders not correctly bound");

    m_circle_in_color_loc = m_circleProgram->uniformLocation("in_color");
    m_circle_line_size_loc = m_circleProgram->uniformLocation("in_params");
    m_circle_transfMatrix_loc = m_circleProgram->uniformLocation("transf");
    m_circle_pixelratio_loc = m_circleProgram->uniformLocation("pixelratio");

    m_circleVao = new QOpenGLVertexArrayObject;
    if (m_circleVao->create() == false)
        qDebug("VAO error!\n");

    m_circle_Vbo = new QOpenGLBuffer;
    m_circle_Vbo->create();

    m_circleVao->release();
    m_circleProgram->release();*/
}

void glBoard::paintGL()
{
    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setBackground(QBrush(properties.background));
    painter.fillRect(rect(), QBrush(properties.background));

    painter.beginNativePainting();
    if (properties.showGrid)
        renderGridGL(f);
    renderSceneGL(f);
    renderToolboxGL(f);
    if (((selecting == true) || (selected == true)) && (current_tool == SELECT))
        renderSelectionGL(f);  //this is quick to draw with painter
    renderCursorGL(f);
    painter.endNativePainting();

    textpop->paint(painter, rect());
}

void glBoard::resizeGL(int w, int h)
{
    glViewport(0, 0, w, h);
    updategrid = true;
}

void glBoard::mousePressEvent(QMouseEvent *event)
{
    QPointF pos; bool isMouse;
#if QT_VERSION_MAJOR == 6
    pos = event->position();
    isMouse = (event->deviceType() != QInputDevice::DeviceType::Stylus);
#else
    pos = event->pos();
    isMouse = (event->source() == Qt::MouseEventNotSynthesized);
#endif
    current_mouse_position = pos;
    if (toolbox->handleMouseEvent(event) == true)
        return;
    if ((event->button() == Qt::LeftButton) && isMouse)
    {
        setFocus();
        startDrag = pos;
    }
    if ((event->button() == Qt::RightButton) && isMouse && (event->modifiers() == Qt::ControlModifier))
    {
        transfMatrix.setToIdentity(); transfMatrixInv = transfMatrix.inverted();
        scale = 1.0f;
        textpop->setText(QString::number((int)(scale * 100)) + "%");
        textpop->popup();
        update();
    }
}

void glBoard::mouseMoveEvent(QMouseEvent *event)
{
    QPointF pos; bool isMouse;
#if QT_VERSION_MAJOR == 6
    pos = event->position();
    isMouse = (event->deviceType() != QInputDevice::DeviceType::Stylus);
#else
    pos = event->pos();
    isMouse = (event->source() == Qt::MouseEventNotSynthesized);
#endif

    current_mouse_position = pos;
    mouse_on_widget = true;
    if (busy_on_scene == false)
    {
        if (toolbox->handleMouseEvent(event) == true)
        {
            mouse_loc = TOOLBOX;
            update();
            return;
        }
    }
    mouse_loc = get_mouse_location(pos);;
    if ((event->buttons() == Qt::LeftButton) && isMouse)
    {
        busy_on_scene = true;
        //we are moving the viewRect
        scale = transfMatrix.data()[0];
        QVector4D diff = 1.0f / scale * QVector4D(pos - startDrag);
        startDrag = event->pos();
        transfMatrix.translate(QVector3D(diff)); transfMatrixInv = transfMatrix.inverted();
        updategrid = true;
    }

    update();
}

void glBoard::mouseReleaseEvent(QMouseEvent *event)
{
    busy_on_scene = false;
    toolbox->handleMouseEvent(event);
}

void glBoard::wheelEvent(QWheelEvent *event)
{
    zoom_shift_function(event);
    update();
}

void glBoard::leaveEvent(QEvent *event)
{
    Q_UNUSED(event);
    mouse_on_widget = false;
    update();
}

void glBoard::tabletEvent(QTabletEvent *event)
{
    QPointF pos;
#if QT_VERSION_MAJOR == 6
    pos = event->position();
#else
    pos = event->posF();
#endif

    previous_mouse_position  = current_mouse_position;
    current_mouse_position = pos;
    mouse_on_widget = true;
    if (busy_on_scene == false)
    {
        if (toolbox->handleMouseEvent(event) == true)
        {
            mouse_loc = TOOLBOX;
            update();
            return;
        }
    }
    mouse_loc = get_mouse_location(pos);

    if ((busy_on_scene == false) && (on_the_fly_operation == false) && (event->type() == QTabletEvent::TabletPress) && (event->button() == Qt::MiddleButton))
    {
        previous_tool = toolbox->get_selectedToolIndex();
        on_the_fly_operation = true;
        toolbox->select_tool(middleButtonTool);
        event->accept();
        return;
    }
    if ((on_the_fly_operation == true) && (event->type() == QTabletEvent::TabletRelease) && (event->button() == Qt::MiddleButton))
    {
        toolbox->select_tool(previous_tool);
        on_the_fly_operation = false;
        event->accept();
        return;
    }

    if (current_tool == PEN)
        pen_function(event);

    if (current_tool == ERASE)
        erase_function(event);

    if (current_tool == SELECT)
        select_function(event);

    zoom_shift_function(event);

    update();
}

void glBoard::keyPressEvent(QKeyEvent *event)
{
    if ((event->key() == Qt::Key_G) && (event->modifiers() == Qt::ControlModifier))
        gridActTriggered();
    if ((event->key() == Qt::Key_P) && (event->modifiers() == Qt::ControlModifier))
        pressureActTriggered();
    if ((event->key() == Qt::Key_S) && (event->modifiers() == Qt::ControlModifier))
        save_scene_to_file();
    if ((event->key() == Qt::Key_L) && (event->modifiers() == Qt::ControlModifier))
        load_scene_from_file();
    if ((event->key() == Qt::Key_F) && (event->modifiers() == Qt::ControlModifier))
    {
        if (isFullScreen())
            showNormal();
        else
            showFullScreen();
    }
    if ((event->key() == Qt::Key_N) && (event->modifiers() == Qt::ControlModifier))
        emit newBoardRequest();
    if (event->key() == Qt::Key_Delete)
    {
        if (selected == true)  //delete all the selected lines
        {
            delete_selected_lines();
            repaint();
        }
    }
    if ((event->key() == Qt::Key_Z) && (event->modifiers() == Qt::ControlModifier))
    {
        restore_history_back();
    }
    if ((event->key() == Qt::Key_Z) && (event->modifiers() == (Qt::ControlModifier | Qt::ShiftModifier)))
    {
        restore_history_forward();
    }
    if ((event->key() == Qt::Key_C) && (event->modifiers() == Qt::ControlModifier))
    {
        if (selected == true)
        {
            copyBuffer.clear();
            copyBuffer = selectedLines;
        }
    }
    if ((event->key() == Qt::Key_V) && (event->modifiers() == Qt::ControlModifier))
    {
        //in this case, we check if there's anything in copybuffer. If so, add it as new lines
        //and select them
        if (copyBuffer.count() != 0)
        {
            selectedLines.clear(); selectedLineIndexes.clear();
            QRectF area;
            for (int i = 0; i < copyBuffer.count(); i++)
            {
                scene.lines.append(copyBuffer[i]);
                selectedLines.append(copyBuffer[i]);
                selectedLineIndexes.append(scene.lines.count() - 1);
                area = area.united(copyBuffer[i].rect);
            }
            selected = true; selectArea = area; selecting = false;
            add_to_history(PASTE_OP);
        }
    }

    if ((event->key() == Qt::Key_E) && (event->modifiers() == Qt::NoModifier))
    {
        //select ERASER
        toolbox->select_tool(ERASE);
    }
    if ((event->key() == Qt::Key_S) && (event->modifiers() == Qt::NoModifier))
    {
        //select SELECT
        toolbox->select_tool(SELECT);
    }
    if ((event->key() == Qt::Key_P) && (event->modifiers() == Qt::NoModifier))
    {
        //select ERASER
        toolbox->select_tool(PEN);
    }
}

void glBoard::contextMenuEvent(QContextMenuEvent *event)
{
    if (toolbox->get_toolbox_rect().contains(event->pos()))
        return;
    if (event->modifiers() == Qt::NoModifier)
    {
        QString undostring, redostring;
        bool undoactive, redoactive;
        get_undo_redo_strings(undostring, redostring, undoactive, redoactive);
        if (undoactive == true)
        {
            undoAct->setEnabled(true); undoAct->setText("Undo " + undostring);
        }
        else
        {
            undoAct->setEnabled(false); undoAct->setText("Undo");
        }
        if (redoactive == true)
        {
            redoAct->setEnabled(true); redoAct->setText("Redo " + redostring);
        }
        else
        {
            redoAct->setEnabled(false); redoAct->setText("Redo");
        }
        popup->popup(event->globalPos());
    }
}

void glBoard::closeEvent(QCloseEvent *event)
{
    unsetCursor();
    QMessageBox::StandardButton ret = QMessageBox::question(this, "Are you sure?", "Do you really want to close this board?");
    if (ret == QMessageBox::Yes)
    {
        emit closeRequested(this);
        event->accept();
    }
    else
    {
        event->ignore();
        repaint();
    }
}

void glBoard::saveTuxTriggered()
{
    save_scene_to_file();
}

void glBoard::loadTuxTriggered()
{
    load_scene_from_file();
}

void glBoard::saveSVGTriggered()
{
    save_scene_to_svg();
}

void glBoard::savePDFTriggered()
{
    save_scene_to_pdf();
}

void glBoard::preferenceActTriggered()
{
    prefdialog *p = new prefdialog(properties);
    connect(p, &prefdialog::propertiesChanged, this, &glBoard::propertiesUpdated);

    unsetCursor();

    p->exec();
    p->close();
    if (p->result() == QDialog::Accepted)
    {
        properties = p->get_properties();
        emit propertiesChanged(properties);
    }
    else
        properties = p->get_old_properties();

    delete p;

    repaint();
}

void glBoard::undoActTriggered()
{
    restore_history_back();
}

void glBoard::redoActTriggered()
{
    restore_history_forward();
}

void glBoard::infoDlgTriggered()
{
    this->unsetCursor();
    infodialog *dlg = new infodialog;
    dlg->exec();
    delete dlg;
    repaint();
}

void glBoard::toolboxUpdated()
{
    toolobject *t;
    if (current_tool == PEN)
    {
        t = toolbox->get_selectedToolObject();
        if (t != nullptr)
        {
            current_color = QColor::fromRgba(toolbox->get_selectedToolObject()->getData().toUInt());
            current_color.setAlphaF(1.0f);
        }
    }
    repaint();
}

void glBoard::toolChanged(tools type, QVariant data, int size)
{
    current_tool = type;

    if (type == PEN)
    {
        current_color = QColor::fromRgba(data.toUInt());
        current_line_size = size;
        textpop->setText("Pen size: " + QString::number(size));
        textpop->popup();
    }
    if (type == ERASE)
    {
        current_erase_size = size;
        textpop->setText("Erase size: " + QString::number(size));
        textpop->popup();
    }
    selectedLineIndexes.clear();
    selectedLines.clear();
    selected = false;
    selecting = false;
}

void glBoard::propertiesUpdated(board_properties p)
{
    properties = p;
    toolbox->setProperties(p);
    apply_pressure = properties.apply_pen_pressure;
    pressureAct->setChecked(apply_pressure);
    middleButtonTool = properties.middleButtonTool;
    repaint();
    unsetCursor();
}

void glBoard::glCleanup()
{
/*    if (m_lineProgram == nullptr)
        return;
    makeCurrent();
    if (m_line_x_Vbo != nullptr)
        m_line_x_Vbo->destroy();
    if (m_line_y_Vbo != nullptr)
        m_line_y_Vbo->destroy();
    if (m_line_p_Vbo != nullptr)
        m_line_p_Vbo->destroy();
    if (m_lineVao != nullptr)
        m_lineVao->destroy();
    m_lineProgram->deleteLater();*/
    doneCurrent();
}

void glBoard::loadDefaultValues()
{
    busy_on_scene = false;
    on_the_fly_operation = false;

    current_color = Qt::black;
    current_line_size = 4;
    current_erase_size = 10;
    current_tool = PEN;
    apply_pressure = properties.apply_pen_pressure;
    middleButtonTool = properties.middleButtonTool;

    transfMatrix.setToIdentity(); transfMatrixInv = transfMatrix.inverted();
    scale = 1.0f;

    selecting = false;
    selected = false;

    //this is the first scene
    history_index = 0;
    history_freeze tmp;
    tmp.event_type = NONE_OP;
    tmp.scene = scene;
    tmp.selected = selected;
    tmp.selectArea = QRect(0,0,0,0);
    tmp.selectedLineIndexes.clear();
    tmp.selectedLines.clear();
    history.append(tmp);
}

void glBoard::prepareWidget()
{
    toolbox = new toolBox(properties);
    connect(toolbox, &toolBox::updated, this, &glBoard::toolboxUpdated);
    connect(toolbox, &toolBox::toolChanged, this, &glBoard::toolChanged);

    textpop = new textPopup;
    connect(textpop, &textPopup::updated, this, &glBoard::toolboxUpdated);

    newBoardAct = new QAction;
    newBoardAct->setText("New board");;
    connect(newBoardAct, &QAction::triggered, this, &glBoard::newBoardRequest);

    preferenceAct = new QAction;
    preferenceAct->setText("Preferences...");
    connect(preferenceAct, &QAction::triggered, this, &glBoard::preferenceActTriggered);

    saveTuxAct = new QAction;
    saveTuxAct->setText("Save tuxBoard");
    connect(saveTuxAct, &QAction::triggered, this, &glBoard::saveTuxTriggered);

    loadTuxAct = new QAction;
    loadTuxAct->setText("Load tuxBoard");
    connect(loadTuxAct, &QAction::triggered, this, &glBoard::loadTuxTriggered);

    saveSVGAct = new QAction;
    saveSVGAct->setText("Export to SVG");
    connect(saveSVGAct, &QAction::triggered, this, &glBoard::saveSVGTriggered);

    savePDFAct = new QAction;
    savePDFAct->setText("Export to PDF");
    connect(savePDFAct, &QAction::triggered, this, &glBoard::savePDFTriggered);

    gridAct = new QAction;
    gridAct->setText("Grid"); gridAct->setCheckable(true); gridAct->setChecked(gridAct);
    connect(gridAct, &QAction::triggered, this, &glBoard::gridActTriggered);

    pressureAct = new QAction;
    pressureAct->setText("Apply pen pressure"); pressureAct->setCheckable(true); pressureAct->setChecked(apply_pressure);
    connect(pressureAct, &QAction::triggered, this, &glBoard::pressureActTriggered);

    undoAct = new QAction;
    undoAct->setText("Undo");
    connect(undoAct, &QAction::triggered, this, &glBoard::undoActTriggered);

    redoAct = new QAction;
    redoAct->setText("Redo");
    connect(redoAct, &QAction::triggered, this, &glBoard::redoActTriggered);

    infoAct = new QAction;
    infoAct->setText("Info...");
    connect(infoAct, &QAction::triggered, this, &glBoard::infoDlgTriggered);

    popup = new QMenu;
    popup->addAction(newBoardAct);
    popup->addSeparator();
    popup->addAction(gridAct);
    popup->addAction(pressureAct);
    popup->addSeparator();
    popup->addAction(undoAct);
    popup->addAction(redoAct);
    popup->addSeparator();
    popup->addAction(saveTuxAct);
    popup->addAction(loadTuxAct);
    popup->addSeparator();
    popup->addAction(saveSVGAct);
    popup->addAction(savePDFAct);
    popup->addSeparator();
    popup->addAction(preferenceAct);
    popup->addSeparator();
    popup->addAction(infoAct);
}

void glBoard::renderGrid(QPainter &painter)
{
    //used for rendering to SVG or PDF
    QVector<int> x_ticks, y_ticks;

    QPoint topleft, bottomright;
    topleft = get_scene_rect().topLeft();
    bottomright = get_scene_rect().bottomRight();
    float step = properties.grid_step;
    int start_x = ((int)(topleft.x() / step) - 1) * step;
    int start_y = ((int)(topleft.y() / step) - 1) * step;
    while (start_x < bottomright.x())
    {
        if (start_x > topleft.x())
            x_ticks.append(start_x);
        start_x += step;  //we added the coordinates transformed already to the widget coordinates
    }
    while (start_y < bottomright.y())
    {
        if (start_y > topleft.y())
            y_ticks.append(start_y);
        start_y += step;  //we added the coordinates transformed already to the widget coordinates
    }

    painter.setPen(QPen(properties.grid_color, properties.grid_line_size, properties.grid_style));

    QPoint p1, p2;
    for (int i = 0; i < x_ticks.count(); i++)
    {
        p1.setX(x_ticks[i]); p1.setY(topleft.y());
        p2.setX(x_ticks[i]); p2.setY(bottomright.y());
        p1 = transfMatrix.map(p1); p2 = transfMatrix.map(p2);
        painter.drawLine(p1, p2);
    }
    for (int i = 0; i < y_ticks.count(); i++)
    {
        p1.setX(topleft.x()); p1.setY(y_ticks[i]);
        p2.setX(bottomright.x()); p2.setY(y_ticks[i]);
        p1 = transfMatrix.map(p1); p2 = transfMatrix.map(p2);
        painter.drawLine(p1, p2);
    }
}

void glBoard::renderGridGL(QOpenGLFunctions *f)
{
    if (updategrid == true)
    {
        QVector<int> x_ticks, y_ticks;

        QPoint topleft, bottomright;
        topleft = transfMatrixInv.map(rect().topLeft());
        bottomright = transfMatrixInv.map(rect().bottomRight());
        float step = properties.grid_step;
        int start_x = ((int)(topleft.x() / step) - 1) * step;
        int start_y = ((int)(topleft.y() / step) - 1) * step;
        while (start_x < bottomright.x())
        {
            if (start_x > topleft.x())
                x_ticks.append(start_x);
            start_x += step;  //we added the coordinates transformed already to the widget coordinates
        }
        while (start_y < bottomright.y())
        {
            if (start_y > topleft.y())
                y_ticks.append(start_y);
            start_y += step;  //we added the coordinates transformed already to the widget coordinates
        }

        QVector<float> x_data, y_data;
        x_data.resize(2 * (x_ticks.count() + y_ticks.count()));
        y_data.resize(2 * (x_ticks.count() + y_ticks.count()));

        grid_segments.clear();
        grid_points1.clear();
        grid_points2.clear();

        QPointF p1, p2;
        for (int i = 0; i < x_ticks.count(); i++)
        {
            p1.setX(x_ticks[i]); p1.setY(topleft.y());
            p2.setX(x_ticks[i]); p2.setY(bottomright.y());
            grid_points1.append(p1); grid_points2.append(p2);
            grid_segments.append(create_segment(p1, p2, properties.grid_line_size, 0, 0, false, false));
        }
        for (int i = x_ticks.count(); i < x_ticks.count() + y_ticks.count(); i++)
        {
            p1.setX(topleft.x()); p1.setY(y_ticks[i - x_ticks.count()]);
            p2.setX(bottomright.x()); p2.setY(y_ticks[i - x_ticks.count()]);
            grid_points1.append(p1); grid_points2.append(p2);
            grid_segments.append(create_segment(p1, p2, properties.grid_line_size, 0, 0, false, false));
        }
    }

    glEnable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    for (int i = 0; i < grid_segments.count(); i++)
    {
        m_segmentESProgram->bind();
        m_segmentESVao->bind();

        m_segmentES_x_Vbo->bind();
        m_segmentES_x_Vbo->allocate(grid_segments[i].points_x.data(), grid_segments[i].points_x.count() * sizeof(float));
        f->glEnableVertexAttribArray(0);
        f->glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, sizeof(float), reinterpret_cast<void*>(0));

        m_segmentES_y_Vbo->bind();
        m_segmentES_y_Vbo->allocate(grid_segments[i].points_y.data(), grid_segments[i].points_y.count() * sizeof(float));
        f->glEnableVertexAttribArray(1);
        f->glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(GLfloat), reinterpret_cast<void*>(0));

        m_segmentESProgram->setUniformValue(m_ES_seg_in_color_loc, properties.grid_color);
        m_segmentESProgram->setUniformValue(m_ES_seg_in_params_loc, QVector3D(rect().width(), rect().height(), 1));
        m_segmentESProgram->setUniformValue(m_ES_seg_transfMatrix_loc, transfMatrix);
        m_segmentESProgram->setUniformValue(m_ES_seg_in_points_loc, QVector4D(grid_points1[i].x(), grid_points1[i].y(), grid_points2[i].x(), grid_points2[i].y()));
        glDrawArrays(GL_TRIANGLE_STRIP, 0, grid_segments[i].points_x.count());
        m_segmentESVao->release(); m_segmentESProgram->release();
    }
    updategrid = false;
}

qint64 glBoard::renderSceneGL(QOpenGLFunctions *f)
{
    QElapsedTimer timer;
    timer.start();
    glEnable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    //glEnable(GL_POLYGON_SMOOTH_HINT); glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glEnable(GL_POINT_SMOOTH); glEnable(GL_LINE_SMOOTH);

    QRectF screen_rect = map_to_scene(rect());
    for (int i = 0; i < scene.lines.count(); i++)
    {
        if ((rects_intersect(scene.lines[i].rect, screen_rect) == true) || (i == scene.lines.count() - 1))  //this doesn't work until the rect is calculated tho => so check for the case we are drawing
        {
            m_lineESProgram->bind();
            m_lineESVao->bind();

            QMatrix4x4 t;
            t = transfMatrix * scene.lines[i].zoom_shift_transf;

            m_lineES_x_Vbo->bind();
            m_lineES_x_Vbo->allocate(scene.lines[i].rend_points_x.data(), scene.lines[i].rend_points_x.count() * sizeof(float));
            f->glEnableVertexAttribArray(0);
            f->glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, sizeof(float), reinterpret_cast<void*>(0));

            m_lineES_y_Vbo->bind();
            m_lineES_y_Vbo->allocate(scene.lines[i].rend_points_y.data(), scene.lines[i].rend_points_y.count() * sizeof(float));
            f->glEnableVertexAttribArray(1);
            f->glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(float), reinterpret_cast<void*>(0));

            m_lineESProgram->setUniformValue(m_ES_in_color_loc, scene.lines[i].colors[0]);
            m_lineESProgram->setUniformValue(m_ES_in_params_loc, QVector3D(rect().width(), rect().height(), 0));
            m_lineESProgram->setUniformValue(m_ES_transfMatrix_loc, t);
            glDrawArrays(GL_TRIANGLE_STRIP, 0, scene.lines[i].rend_points_x.count());
            m_lineESVao->release(); m_lineESProgram->release();

            /*
            m_lineProgram->bind();
            m_lineVao->bind();

            m_line_x_Vbo->bind();
            m_line_x_Vbo->allocate(scene.lines[i].points_x.data(), scene.lines[i].points_x.count() * sizeof(float));
            f->glEnableVertexAttribArray(0);
            f->glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, sizeof(float), reinterpret_cast<void*>(0));

            m_line_y_Vbo->bind();
            m_line_y_Vbo->allocate(scene.lines[i].points_y.data(), scene.lines[i].points_y.count() * sizeof(GLfloat));
            f->glEnableVertexAttribArray(1);
            f->glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(GLfloat), reinterpret_cast<void*>(0));

            m_line_p_Vbo->bind();
            m_line_p_Vbo->allocate(scene.lines[i].pressures.data(), scene.lines[i].pressures.count() * sizeof(GLfloat));
            f->glEnableVertexAttribArray(2);
            f->glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(GLfloat), reinterpret_cast<void*>(0));

            m_lineProgram->setUniformValue(m_in_color_loc, scene.lines[i].colors[0]);
            m_lineProgram->setUniformValue(m_line_size_loc, QVector4D(scene.lines[i].line_size, rect().width(), rect().height(), 0));
            m_lineProgram->setUniformValue(m_transfMatrix_loc, transfMatrix);
            m_lineProgram->setUniformValue(m_scale_loc, (float)this->devicePixelRatio());
            if (scene.lines[i].apply_pressure == true)
                m_lineProgram->setUniformValue(m_apply_pressure_loc, (float)1.0);
            else
                m_lineProgram->setUniformValue(m_apply_pressure_loc, (float)-1.0);
            glDrawArrays(GL_LINE_STRIP, 0, scene.lines[i].points_x.count());
            m_lineVao->release(); m_lineProgram->release();

            m_roundProgram->bind(); m_roundVao->bind();
            m_line_x_Vbo->bind(); f->glEnableVertexAttribArray(0);
            f->glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, sizeof(GLfloat), reinterpret_cast<void*>(0));
            m_line_y_Vbo->bind(); f->glEnableVertexAttribArray(1);
            f->glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(GLfloat), reinterpret_cast<void*>(0));
            m_line_p_Vbo->bind(); f->glEnableVertexAttribArray(2);
            f->glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(GLfloat), reinterpret_cast<void*>(0));
            m_roundProgram->setUniformValue(m_round_in_color_loc, scene.lines[i].colors[0]);
            m_roundProgram->setUniformValue(m_round_line_size_loc, QVector3D(scene.lines[i].line_size, rect().width(), rect().height()));
            m_roundProgram->setUniformValue(m_round_transfMatrix_loc, transfMatrix);
            if (scene.lines[i].apply_pressure == true)
                m_roundProgram->setUniformValue(m_round_apply_pressure_loc, (float)1.0);
            else
                m_roundProgram->setUniformValue(m_round_apply_pressure_loc, (float)-1.0);
            glDrawArrays(GL_POINTS, 0, scene.lines[i].points_x.count());
            m_roundVao->release(); m_roundProgram->release();
            */
        }
    }
    return timer.nsecsElapsed();
}

void glBoard::renderScene(QPainter &painter)
{
    //used for rendering to SVG or PDF

    //different rendering methods
#if defined(CUBIC_RENDER) || defined(QUAD_RENDER) || (defined(LINE_RENDER) && defined(LINE_PATH))
    for (int i = 0; i < scene.lines.count(); i++)
    {
        QPen pen(QPen(scene.lines[i].colors[0], calculate_line_size(scene.lines[i].line_size, 0.0, false), Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
        painter.setPen(pen);
        painter.drawPath(get_path(scene.lines[i]));
    }
#endif

#if (defined(LINE_RENDER) && !defined(LINE_PATH))
    for (int i = 0; i < scene.lines.count(); i++)
    {
        painter.setPen(Qt::NoPen);
        for (int j = 0; j < scene.lines[i].points_x.count() - 1; j++)
        {
            QPen pen(QPen(scene.lines[i].colors[j], scene.lines[i].line_size, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
            if (scene.lines[i].apply_pressure == true)
                pen.setWidthF(scene.lines[i].line_size + scene.lines[i].pressures[j] * scene.lines[i].line_size * 2.0f);
            pen.setWidthF(scale * pen.widthF());
            painter.setPen(pen);
            QPointF p1, p2;
            p1 = QPointF(scene.lines[i].points_x[j], scene.lines[i].points_y[j]);
            p2 = QPointF(scene.lines[i].points_x[j + 1], scene.lines[i].points_y[j + 1]);
            painter.drawLine(transfMatrix.map(p1), transfMatrix.map(p2));
        }
    }
#endif

#if defined(ELLIPSE_RENDER)
    for (int i = 0; i < scene.lines.count(); i++)
    {
        painter.setPen(Qt::NoPen);
        for (int j = 0; j < scene.lines[i].points.count(); j++)
        {
            painter.setBrush(scene.lines[i].colors[j]);
            qreal radius = scene.lines[i].line_size;
            painter.drawEllipse(scene.lines[i].points[j], radius, radius);
        }
    }

#endif
}

void glBoard::renderToolbox(QPainter &painter)
{
    toolbox->paint(painter, rect());
}

void glBoard::renderToolboxGL(QOpenGLFunctions *f)
{
    toolbox->paintGL(f, rect(), this->screen()->devicePixelRatio());
}

void glBoard::renderCursor(QPainter &painter)
{
    QRect r; r.setTopLeft(mapToGlobal(rect().topLeft())); r.setBottomRight(mapToGlobal(rect().bottomRight()));
    if (!r.contains(cursor().pos()))
        return;
    if (mouse_loc != TOOLBOX)
    {
        setCursor(Qt::BlankCursor);
        if (current_tool == PEN)
        {
            painter.setBrush(QColor(current_color));
            QRectF out_circle;
            QPointF pos = current_mouse_position;
            out_circle.setTopLeft(QPointF(pos.x() - current_line_size, pos.y() - current_line_size));
            out_circle.setBottomRight(QPointF(pos.x() + current_line_size, pos.y() + current_line_size));
            painter.drawEllipse(out_circle);
        }
        if (current_tool == ERASE)
        {
            painter.setPen(QPen(QColor(Qt::darkGray), 1, Qt::DotLine));
            painter.setBrush(Qt::NoBrush);
            QRectF out_circle;
            QPointF pos = current_mouse_position;
            out_circle.setTopLeft(QPointF(pos.x() - (current_erase_size / 2), pos.y() - (current_erase_size / 2)));
            out_circle.setBottomRight(QPointF(pos.x() + (current_erase_size / 2), pos.y() + (current_erase_size / 2)));
            painter.drawEllipse(out_circle);
        }
        if (current_tool == SELECT)
        {
            mouse_location_enum loc;
            if ((selectionMoving == true) || (selectionResizing == true))
            {
                if (selectionMoving == true)
                    loc = SELECTION_MOVE;
                if (selectionResizing == true)
                    loc = selectionResizingType;
            }
            else
                loc = mouse_loc;

            if (loc == SCENE)
                setCursor(Qt::CursorShape::CrossCursor);
            if (loc == SELECTION_MOVE)
                setCursor(Qt::CursorShape::SizeAllCursor);
            if ((loc == SELECTION_UP) || (loc == SELECTION_DOWN))
                setCursor(Qt::CursorShape::SizeVerCursor);
            if ((loc == SELECTION_LEFT) || (loc == SELECTION_RIGHT))
                setCursor(Qt::CursorShape::SizeHorCursor);
            if ((loc == SELECTION_TOPLEFT) || (loc == SELECTION_BOTTOMRIGHT))
                setCursor(Qt::CursorShape::SizeFDiagCursor);
            if ((loc == SELECTION_BOTTOMLEFT) || (loc == SELECTION_TOPRIGHT))
                setCursor(Qt::CursorShape::SizeBDiagCursor);
        }
    }
    if (mouse_loc == TOOLBOX)
        setCursor(Qt::ArrowCursor);
}

void glBoard::renderCursorGL(QOpenGLFunctions *f)
{
    QRect r; r.setTopLeft(mapToGlobal(rect().topLeft())); r.setBottomRight(mapToGlobal(rect().bottomRight()));
    if (!r.contains(cursor().pos()))
        return;
    if (mouse_loc != TOOLBOX)
    {
        setCursor(Qt::BlankCursor);
        if (current_tool == PEN)
        {
            QRectF out_circle;
            QPointF pos = current_mouse_position;
            float size = (float)current_line_size * scale;
            if (size < 5.0f)
                size = 5.0f;
            out_circle.setTopLeft(QPointF(pos.x() - size, pos.y() - size));
            out_circle.setBottomRight(QPointF(pos.x() + size, pos.y() + size));
            renderCircleGL(f, fromQRectFtoQRect(out_circle), current_color, -1, 1.0f);
        }
        if (current_tool == ERASE)
        {
            QRectF out_circle;
            QPointF pos = current_mouse_position;
            float size = (float)current_erase_size / 2.0f * scale;
            if (size < 5.0f)
                size = 5.0f;
            out_circle.setTopLeft(QPointF(pos.x() - size, pos.y() - size));
            out_circle.setBottomRight(QPointF(pos.x() + size, pos.y() + size));
            renderCircleGL(f, fromQRectFtoQRect(out_circle), Qt::darkGray, 1, 1.0f);
        }
        if (current_tool == SELECT)
        {
            mouse_location_enum loc;
            if ((selectionMoving == true) || (selectionResizing == true))
            {
                if (selectionMoving == true)
                    loc = SELECTION_MOVE;
                if (selectionResizing == true)
                    loc = selectionResizingType;
            }
            else
                loc = mouse_loc;

            if (loc == SCENE)
                setCursor(Qt::CursorShape::CrossCursor);
            if (loc == SELECTION_MOVE)
                setCursor(Qt::CursorShape::SizeAllCursor);
            if ((loc == SELECTION_UP) || (mouse_loc == SELECTION_DOWN))
                setCursor(Qt::CursorShape::SizeVerCursor);
            if ((loc == SELECTION_LEFT) || (mouse_loc == SELECTION_RIGHT))
                setCursor(Qt::CursorShape::SizeHorCursor);
            if ((loc == SELECTION_TOPLEFT) || (mouse_loc == SELECTION_BOTTOMRIGHT))
                setCursor(Qt::CursorShape::SizeFDiagCursor);
            if ((loc == SELECTION_BOTTOMLEFT) || (mouse_loc == SELECTION_TOPRIGHT))
                setCursor(Qt::CursorShape::SizeBDiagCursor);
        }
    }
    if (mouse_loc == TOOLBOX)
        setCursor(Qt::ArrowCursor);
}

void glBoard::renderSelection(QPainter &painter)
{
    QPainterPath path;
    QRect s = fromQRectFtoQRect(adjust_selection_area());
//    s.setTopLeft(transfMatrix.map(s.topLeft()));
//    s.setBottomRight(transfMatrix.map(s.bottomRight()));
    path.addRoundedRect(s, 10, 10);
    painter.setPen(QPen(properties.selectColor, properties.selectLineSize, properties.selectLineStyle));
    painter.setBrush(properties.selectFillColor);
    painter.drawPath(path);
}

void glBoard::renderSelectionGL(QOpenGLFunctions *f)
{
    QRect s;
    if (selecting == true)
        s = fromQRectFtoQRect(map_to_widget(selectArea));
    else
        s = fromQRectFtoQRect(adjust_selection_area());
//    s.setTopLeft(transfMatrix.map(s.topLeft()));
//    s.setBottomRight(transfMatrix.map(s.bottomRight()));
    renderRectGL(f, s, properties.selectFillColor, -1, 10);
    renderRectGL(f, s, properties.selectColor, properties.selectLineSize, 10);
}

void glBoard::renderRectGL(QOpenGLFunctions *f, QRect r, QColor col, int line_size, int radius)
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    QVector<float> point_data;
    r = checkQRect(r);
    point_data.append(r.left()); point_data.append(r.top());
    point_data.append(r.right()); point_data.append(r.bottom());
    QVector4D ext_points(r.left(), r.top(), r.right(), r.bottom());
    QVector<float> x_buff, y_buff;
    x_buff.append(r.left()); x_buff.append(r.left()); x_buff.append(r.right()); x_buff.append(r.right());
    y_buff.append(r.top()); y_buff.append(r.bottom()); y_buff.append(r.top()); y_buff.append(r.bottom());

    m_rectESProgram->bind();
    m_rectESVao->bind();

    m_rect_ES_x_Vbo->bind();
    m_rect_ES_x_Vbo->allocate(x_buff.data(), x_buff.count() * sizeof(float));
    f->glEnableVertexAttribArray(0);
    f->glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, sizeof(float), reinterpret_cast<void*>(0));

    m_rect_ES_y_Vbo->bind();
    m_rect_ES_y_Vbo->allocate(y_buff.data(), y_buff.count() * sizeof(float));
    f->glEnableVertexAttribArray(1);
    f->glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(float), reinterpret_cast<void*>(0));

    m_rectESProgram->setUniformValue(m_rect_ES_in_color_loc, col);
    m_rectESProgram->setUniformValue(m_rect_ES_in_params_loc, QVector4D(line_size, rect().width(), rect().height(), radius));
    QMatrix4x4 id; id.setToIdentity();
    m_rectESProgram->setUniformValue(m_rect_ES_transfMatrix_loc, id);
    m_rectESProgram->setUniformValue(m_rect_ES_pixelratio_loc, (float)this->screen()->devicePixelRatio());
    m_rectESProgram->setUniformValue(m_rect_ES_ext_points_loc, ext_points);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    m_rect_ES_x_Vbo->release();
    m_rect_ES_y_Vbo->release();
    m_rectESVao->release();
    m_rectESProgram->release();
    /*
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    QVector<float> point_data;
    r = checkQRect(r);
    point_data.append(r.left()); point_data.append(r.top());
    point_data.append(r.right()); point_data.append(r.bottom());

    m_rectProgram->bind();
    m_rectVao->bind();

    m_line_x_Vbo->destroy();
    m_line_x_Vbo->create();
    m_line_x_Vbo->bind();
    m_line_x_Vbo->allocate(point_data.data(), point_data.count() * sizeof(float));
    f->glEnableVertexAttribArray(0);
    f->glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), reinterpret_cast<void*>(0));

    m_rectProgram->setUniformValue(m_rect_in_color_loc, col);
    m_rectProgram->setUniformValue(m_rect_line_size_loc, QVector4D(line_size, rect().width(), rect().height(), radius));
    QMatrix4x4 id; id.setToIdentity();
    m_rectProgram->setUniformValue(m_rect_transfMatrix_loc, id);
    m_rectProgram->setUniformValue(m_rect_pixelratio_loc, (float)this->screen()->devicePixelRatio());
    glDrawArrays(GL_LINES, 0, 2);

    m_line_x_Vbo->release();
    m_rectVao->release();
    m_rectProgram->release();*/
}

void glBoard::renderCircleGL(QOpenGLFunctions *f, QRect r, QColor col, int line_size, float shade)
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    QVector<float> point_data;
    r = checkQRect(r);
    point_data.append(r.left()); point_data.append(r.top());
    point_data.append(r.right()); point_data.append(r.bottom());
    QVector4D ext_points(r.left(), r.top(), r.right(), r.bottom());
    QVector<float> x_buff, y_buff;
    x_buff.append(r.left()); x_buff.append(r.left()); x_buff.append(r.right()); x_buff.append(r.right());
    y_buff.append(r.top()); y_buff.append(r.bottom()); y_buff.append(r.top()); y_buff.append(r.bottom());

    m_circleESProgram->bind();
    m_circleESVao->bind();

    m_circle_ES_x_Vbo->bind();
    m_circle_ES_x_Vbo->allocate(x_buff.data(), x_buff.count() * sizeof(float));
    f->glEnableVertexAttribArray(0);
    f->glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, sizeof(float), reinterpret_cast<void*>(0));

    m_circle_ES_y_Vbo->bind();
    m_circle_ES_y_Vbo->allocate(y_buff.data(), y_buff.count() * sizeof(float));
    f->glEnableVertexAttribArray(1);
    f->glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(float), reinterpret_cast<void*>(0));

    m_circleESProgram->setUniformValue(m_circle_ES_in_color_loc, col);
    m_circleESProgram->setUniformValue(m_circle_ES_in_params_loc, QVector4D(line_size, rect().width(), rect().height(), shade));
    QMatrix4x4 id; id.setToIdentity();
    m_circleESProgram->setUniformValue(m_circle_ES_transfMatrix_loc, id);
    m_circleESProgram->setUniformValue(m_circle_ES_pixelratio_loc, (float)this->screen()->devicePixelRatio());
    m_circleESProgram->setUniformValue(m_circle_ES_ext_points_loc, ext_points);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    m_circle_ES_x_Vbo->release();
    m_circle_ES_y_Vbo->release();
    m_circleESVao->release();
    m_circleESProgram->release();
    /*
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    QVector<float> point_data;
    r = checkQRect(r);
    point_data.append(r.left()); point_data.append(r.top());
    point_data.append(r.right()); point_data.append(r.bottom());

    m_circleProgram->bind();
    m_circleVao->bind();

    m_circle_Vbo->bind();
    m_circle_Vbo->allocate(point_data.data(), point_data.count() * sizeof(float));
    f->glEnableVertexAttribArray(0);
    f->glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), reinterpret_cast<void*>(0));

    m_circleProgram->setUniformValue(m_circle_in_color_loc, col);
    m_circleProgram->setUniformValue(m_circle_line_size_loc, QVector4D(line_size, rect().width(), rect().height(), shade));
    QMatrix4x4 id; id.setToIdentity();
    m_circleProgram->setUniformValue(m_circle_transfMatrix_loc, id);
    m_circleProgram->setUniformValue(m_circle_pixelratio_loc, (float)this->screen()->devicePixelRatio());
    glDrawArrays(GL_LINES, 0, 2);

    m_circle_Vbo->release();
    m_circleVao->release();
    m_circleProgram->release();*/
}

int glBoard::calculate_line_size(int line_size, float pressure, bool applypressure)
{
    float conv_fact = 4.0f;

    int out;

    if (applypressure)
        out = line_size + (int)(conv_fact * pressure);
    else
        out = line_size;

    //apply scale
    out = (int)((float)out * scale);
    if (out < 1)
        out = 1;

    return out;
}

QPainterPath glBoard::get_path(line_type line)
{
    QPainterPath path;

    if (line.points_x.count() <= 1)
        return path;
    path.moveTo(transfMatrix.map(QPointF(line.points_x[0], line.points_y[0])));
#if defined(LINE_RENDER) && defined(LINE_PATH)
    for (int i = 1; i < line.points.count(); i++)
        path.lineTo(transfMatrix.map(line.points[i]));
#endif

#ifdef QUAD_RENDER
    for (int i = 2; i < line.points.count() - 1; i+=2)
        path.quadTo(transfMatrix.map(line.points[i]), transfMatrix.map(line.points[i + 1]));
#endif

#ifdef CUBIC_RENDER
    for (int i = 2; i < line.points.count() - 2; i+=3)
        path.cubicTo(transfMatrix.map(line.points[i]), transfMatrix.map(line.points[i + 1]), transfMatrix.map(line.points[i + 2]));
#endif
    return path;
}

QMatrix4x4 glBoard::get_scale_matrix(QMatrix4x4 mat)
{
    QVector4D null(0, 0, 0, 0);
    mat.setColumn(3, null);
    mat.setRow(3, null);

    return mat;
}

void glBoard::save_scene_to_file(QString filename)
{
    if (filename.isEmpty())
    {
        filename = QFileDialog::getSaveFileName(this, "Save board", "", "Tux Board (*.tbd);; All Files (*)");
        if (filename.isEmpty())
            return;
    }

    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox::information(this, "Unable to open file", file.errorString());
        return;
    }

    QDataStream out(&file);

    out << scene.lines.size();

    for (int i = 0; i < scene.lines.size(); i++)
    {
        out << scene.lines[i].points_x.count();
        for (int j = 0; j < scene.lines[i].points_x.count(); j++)
        {
            QPointF p = QPointF(scene.lines[i].points_x[j], scene.lines[i].points_y[j]);
            out << p;
            out << scene.lines[i].pressures[j];
            out << scene.lines[i].colors[j];
            out << scene.lines[i].times[j];
        }
        out << scene.lines[i].apply_pressure;
        out << scene.lines[i].line_size;
        out << scene.lines[i].rect;
    }

    file.close();

    return;
}

void glBoard::save_scene_to_svg(QString filename)
{
    if (filename.isEmpty())
    {
        filename = QFileDialog::getSaveFileName(this, "Save board", "", "SVG file (*.svg);; All Files (*)");
        if (filename.isEmpty())
            return;
    }

    QMatrix4x4 prev_matrix = transfMatrix;

    QRectF s = get_scene_rect();
    int margin = 50;
    int marginW, marginH;
    if (s.width() > s.height())
    {
        marginW = margin; marginH = s.height() / s.width() * margin;
    }
    else
    {
        marginH = margin; marginW = s.width() / s.height() * margin;
    }
    s.setLeft(s.left() - marginW); s.setRight(s.right() + marginW);
    s.setTop(s.top() - marginH); s.setBottom(s.bottom() + marginH);

    transfMatrix.setToIdentity();
    transfMatrix.translate(-QVector3D(s.topLeft()));

    QSvgGenerator generator;
    generator.setFileName(filename);
    generator.setSize(fromQSizeFtoQSize(s.size()));
    generator.setResolution(96);
    generator.setTitle("tuxBoard");

    QPainter painter;
    painter.begin(&generator);
    if (properties.showGrid)
        renderGrid(painter);
    renderScene(painter);
    painter.end();

    transfMatrix = prev_matrix;
}

void glBoard::save_scene_to_pdf(QString filename)
{
    if (filename.isEmpty())
    {
        filename = QFileDialog::getSaveFileName(this, "Save board", "", "PDF file (*.pdf);; All Files (*)");
        if (filename.isEmpty())
            return;
    }

    QMatrix4x4 prev_matrix = transfMatrix;

    QRectF s = get_scene_rect();
    int margin = 50;
    int marginW, marginH;
    if (s.width() > s.height())
    {
        marginW = margin; marginH = s.height() / s.width() * margin;
    }
    else
    {
        marginH = margin; marginW = s.width() / s.height() * margin;
    }
    s.setLeft(s.left() - marginW); s.setRight(s.right() + marginW);
    s.setTop(s.top() - marginH); s.setBottom(s.bottom() + marginH);

    transfMatrix.setToIdentity();
    transfMatrix.translate(-QVector3D(s.topLeft()));

    QPdfWriter pdf(filename);

    int resolution = 72;
    pdf.setResolution(resolution);
    QSizeF sz = s.size();
    sz.setWidth(sz.width());
    sz.setHeight(sz.height());
    QPageSize p(fromQSizeFtoQSize(sz), QPageSize::Point, QString::Null(), QPageSize::ExactMatch);
    pdf.setPageSize(p);
    pdf.setPageMargins(QMarginsF(0,0,0,0));
    pdf.setTitle("tuxBoard");

    QPainter painter;

    painter.begin(&pdf);
    painter.setRenderHint(QPainter::Antialiasing);
    if (properties.showGrid)
        renderGrid(painter);
    renderScene(painter);
    painter.end();

    transfMatrix = prev_matrix;
}

void glBoard::load_scene_from_file(QString filename)
{
    if (filename.isEmpty())
    {
        filename = QFileDialog::getOpenFileName(this, "Load board", "", "Tux Board (*.tbd);; All Files (*)");
        if (filename.isEmpty())
            return;
    }

    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(this, "Unable to open file", file.errorString());
        return;
    }

    QDataStream in(&file);

    scene.lines.clear();

    int N_lines; int N_points;

    int s;
    in >> s;
    N_lines = s;

    for (int i = 0; i < N_lines; i++)
    {
        line_type line;
        in >> s;
        line.colors.resize(s); line.points_x.resize(s); line.points_y.resize(s); line.pressures.resize(s); line.times.resize(s);
        N_points = line.colors.count();
        for (int j = 0; j < N_points; j++)
        {
            QPointF p;
            in >> p >> line.pressures[j] >> line.colors[j] >> line.times[j];
            line.points_x[j] = p.x(); line.points_y[j] = p.y();
        }
        in >> line.apply_pressure >> line.line_size >> line.rect;
        scene.lines.append(line);
    }

    file.close();

    history.clear();
    history_index = 0;
    history_freeze tmp;
    tmp.event_type = NONE_OP;
    tmp.scene = scene;
    tmp.selected = selected;
    tmp.selectArea = QRect(0,0,0,0);
    tmp.selectedLineIndexes.clear();
    tmp.selectedLines.clear();
    history.append(tmp);

    update();

    return;
}

QRect glBoard::get_scene_rect()
{
    bool first = true;
    int right, left, top, bottom;
    right = 0; left = 0; top = 0; bottom = 0;

    for (int i = 0; i < scene.lines.count(); i++)
        for (int j = 0; j < scene.lines[i].points_x.count(); j++)
        {
            if (first == true)
            {
                right = scene.lines[i].points_x[j];
                left = scene.lines[i].points_x[j];
                top = scene.lines[i].points_y[j];
                bottom = scene.lines[i].points_y[j];
                first = false;
            }
            else
            {
                if (scene.lines[i].points_x[j] > right)
                    right = scene.lines[i].points_x[j];
                if (scene.lines[i].points_x[j] < left)
                    left = scene.lines[i].points_x[j];
                if (scene.lines[i].points_y[j] > bottom)
                    bottom = scene.lines[i].points_y[j];
                if (scene.lines[i].points_y[j] < top)
                    top = scene.lines[i].points_y[j];
            }
        }
    QRect r;
    r.setTopLeft(QPoint(left, top));
    r.setBottomRight(QPoint(right, bottom));

    return r;
}

QRectF glBoard::calculate_line_rect(line_t line)
{
    QRectF r;

    if (line.points_x.count() == 1)
    {
        r.setTopLeft(QPointF(line.points_x[0], line.points_y[0])); r.setBottomRight(QPointF(line.points_x[0], line.points_y[0]));
        return r;
    }
    r.setTopLeft(QPointF(line.points_x[0], line.points_y[0])); r.setBottomRight(QPointF(line.points_x[0], line.points_y[0]));
    for (int i = 1; i < line.points_x.count(); i++)
    {
        if (line.points_x[i] < r.left())
            r.setLeft(line.points_x[i]);
        if (line.points_x[i] > r.right())
            r.setRight(line.points_x[i]);
        if (line.points_y[i] < r.top())
            r.setTop(line.points_y[i]);
        if (line.points_y[i] > r.bottom())
            r.setBottom(line.points_y[i]);
    }
    return r;
}

void glBoard::erase_points(QPointF position)
{
    QPointF pos = transfMatrixInv.map(position);

    QRectF out_circle;
    float radius = (float)current_erase_size / 2.0f;
    out_circle.setTopLeft(QPointF(pos.x() - radius, pos.y() - radius));
    out_circle.setBottomRight(QPointF(pos.x() + radius, pos.y() + radius));

    int i = 0;
    while (i < scene.lines.count())
    {
        //check if the eraser is on this line
        if ((rects_intersect(out_circle, scene.lines[i].rect) == true) || (rects_intersect(scene.lines[i].rect, out_circle) == true))
        {
            //in this case, we check if some points are contained
            QVector<bool> toerase; toerase.resize(scene.lines[i].points_x.count());
            bool allerased = true; bool someerased = false;
            for (int j = 0; j < scene.lines[i].points_x.count(); j++)
            {
                if (circle_contains(out_circle, QPointF(scene.lines[i].points_x[j], scene.lines[i].points_y[j])) == true)
                {
                    toerase[j] = true;
                }
                else
                    toerase[j] = false;
                allerased = allerased & toerase[j];
                someerased = someerased | toerase[j];
            }
            if (someerased == true)  //something to do
            {
                if (allerased == true)  //we can delete the whole line
                {
                    scene.lines.removeAt(i);
                    i--;
                }
                else
                {
                    //delete points and split the line in more lines
                    QVector<line_t> newlines = split_line(scene.lines[i], toerase);
                    if (newlines.count() > 0)
                    {
                        generate_render_segments(&newlines[0]);
                        scene.lines[i] = newlines[0];
                        for (int k = newlines.count() - 1; k > 0; k--)
                        {
                            generate_render_segments(&newlines[k]);
                            scene.lines.insert(i, newlines[k]);
                        }
                    }
                    else //remove the line
                    {
                        scene.lines.removeAt(i);
                        i--;
                    }
                }
            }
        }
        i++;
    }
    //remove empty lines as security check (either 0 or 1 points)
    while (i < scene.lines.count())
    {
        if ((scene.lines[i].points_x.count() == 0) || (scene.lines[i].points_x.count() == 1))
        {
            scene.lines.removeAt(i);
            i--;
        }
        i++;
    }
}

void glBoard::erase_lines(QPointF position)
{
    QPointF pos = transfMatrixInv.map(position);

    QRectF out_circle;
    float radius = (float)current_erase_size / 2.0f;
    out_circle.setTopLeft(QPointF(pos.x() - radius, pos.y() - radius));
    out_circle.setBottomRight(QPointF(pos.x() + radius, pos.y() + radius));

    //check if out_circle intersects with a line and its points, if yes delete the line
    int i = 0;
    while (i < scene.lines.count())
    {
        //check if the eraser is on this line
        if ((rects_intersect(out_circle, scene.lines[i].rect) == true) || (rects_intersect(scene.lines[i].rect, out_circle) == true))
        {
            bool toerase = false;
            for (int j = 0; j < scene.lines[i].points_x.count(); j++)
            {
                if (circle_contains(out_circle, QPointF(scene.lines[i].points_x[j], scene.lines[i].points_y[j])) == true)
                    toerase = true;
            }
            if (toerase == false)
            {
                scene.lines.removeAt(i);
                i--;
            }
        }
        i++;
    }
}

QVector<line_t> glBoard::split_line(line_t line, QVector<bool> toerase)
{
    QVector<line_t> out;

    if (toerase.count() == 0)
        return out;

    line_t tmp;

    bool previous = toerase[0];
    if (previous == false)
    {
        tmp.apply_pressure = line.apply_pressure;
        tmp.colors.append(line.colors[0]);
        tmp.line_size = line.line_size;
        tmp.points_x.append(line.points_x[0]);
        tmp.points_y.append(line.points_y[0]);
        tmp.pressures.append(line.pressures[0]);
    }
    for (int i = 1; i < toerase.count(); i++)
    {
        if ((toerase[i] == true) && (previous == false))  //end of a line
        {
            tmp.rect = calculate_line_rect(tmp); out.append(tmp);
        }
        if ((toerase[i] == false) && (previous == true))  //new line
        {
            tmp.colors.clear(); tmp.points_x.clear(); tmp.points_y.clear(); tmp.pressures.clear();
            tmp.apply_pressure = line.apply_pressure;
            tmp.colors.append(line.colors[i]);
            tmp.line_size = line.line_size;
            tmp.points_x.append(line.points_x[i]);
            tmp.points_y.append(line.points_y[i]);
            tmp.pressures.append(line.pressures[i]);
        }
        if ((toerase[i] == false) && (previous == false) & (i != toerase.count() - 1))  //save value
        {
            tmp.apply_pressure = line.apply_pressure;
            tmp.colors.append(line.colors[i]);
            tmp.line_size = line.line_size;
            tmp.points_x.append(line.points_x[i]);
            tmp.points_y.append(line.points_y[i]);
            tmp.pressures.append(line.pressures[i]);
        }
        if ((toerase[i] == false) && (previous == false) && (i == toerase.count() - 1))  //save value and line
        {
            tmp.apply_pressure = line.apply_pressure;
            tmp.colors.append(line.colors[i]);
            tmp.line_size = line.line_size;
            tmp.points_x.append(line.points_x[i]);
            tmp.points_y.append(line.points_y[i]);
            tmp.pressures.append(line.pressures[i]);
            tmp.rect = calculate_line_rect(tmp); out.append(tmp);
        }
        previous = toerase[i];
    }

    //remove single points line
    int i = 0;
    while (i < out.count())
    {
        if ((out[i].points_x.count() == 0) || (out[i].points_x.count() == 1))
        {
            out.removeAt(i);
            i--;
        }
        i++;
    }

    return out;
}

bool glBoard::circle_contains(QRectF rect, QPointF point)
{
    //check if a point is within a circle defined by rect
    float center_x, center_y;
    center_x = rect.left() + (rect.width() / 2.0f);
    center_y = rect.top() + (rect.height() / 2.0f);

    float dx, dy;
    dx = qAbs(point.x() - center_x);
    dy = qAbs(point.y() - center_y);

    float radius = rect.width() / 2.0f; //assuming rect being a square

    if (((dx * dx) + (dy * dy)) <= (radius * radius))
        return true;
    else
        return false;
}

bool glBoard::rects_intersect(QRectF r1, QRectF r2)
{
    if ((r1.width() != 0) && (r1.height() != 0) && (r2.width() != 0) && (r2.height() != 0))
        return (r1.intersects(r2) | r2.intersects(r1));

    if ((r1.width() == 0) && (r1.height() == 0))  //r1 is a point
    {
        QPointF p1 = r1.topLeft();
        if ((r2.width() == 0) && (r2.height() == 0))  //r2 is a point too
        {
            QPointF p2 = r2.topLeft();
            if (p1 == p2)
                return true;
            else
                return false;
        }
        if ((r2.width() == 0) || (r2.height() == 0))  //r2 is a line
        {
            if (r2.width() == 0)  //same x coordinates => check x coordinates
            {
                if ((p1.x() == r2.left()) && (p1.y() < r2.bottom()) && (p1.y() > r2.top()))
                    return true;
                else
                    return false;
            }
            if (r2.height() == 0)  //same y coordinates => check y coordinate
            {
                if ((p1.y() == r2.top()) && (p1.x() < r2.right()) && (p1.x() > r2.left()))
                    return true;
                else
                    return false;
            }
        }
        if ((r2.width() != 0) && (r2.height() != 0))  //r2 is a rect
        {
            if (r2.contains(p1))
                return true;
            else
                return false;
        }
    }

    if ((r1.width() == 0) || (r1.height() == 0))  //r1 is a line
    {
        if ((r2.width() == 0) && (r2.height() != 0))  //vertical line
        {
            if (r1.width() == 0)  //vertical line
            {
                if (r1.left() != r2.left())
                    return false;
                else
                {
                    if ((r1.top() < r2.top()) && (r1.bottom() < r2.top()))  //r1 is above r2
                        return false;
                    if ((r1.top() > r2.bottom()) && (r1.bottom() > r2.bottom()))  //r1 is below r2
                        return false;
                    return true;
                }
            }
            if (r1.height() == 0)  //horizontal line
            {
                if ((r1.left() > r2.left()) && (r1.left() < r2.right()) && (r1.top() > r2.top()) && (r1.top() < r2.bottom()))
                    return true;
                else
                    return false;
            }
        }
        if ((r2.width() != 0) && (r2.height() == 0))  //horizontal line
        {
            if (r1.width() == 0)  //vertical line
            {
                if ((r2.left() > r1.left()) && (r2.left() < r1.right()) && (r2.top() > r1.top()) && (r2.top() < r1.bottom()))
                    return true;
                else
                    return false;
            }
            if (r1.height() == 0)  //horizontal line
            {
                if (r1.top()  != r2.top())
                    return false;
                else
                {
                    if ((r1.left() < r2.left()) && (r1.right() < r2.left()))  //r1 is at the left of r2
                        return false;
                    if ((r1.left() > r2.right()) && (r1.right() > r2.right()))  //r1 is at the right of r2
                        return false;
                    return true;
                }
            }
        }
    }

    if ((r1.width() != 0) || (r1.height() != 0))  //r1 is a rect
    {
        if ((r2.width() == 0) && (r2.height() == 0))  //r2 is a point
        {
            QPointF p2 = r2.topLeft();
            if (r1.contains(p2))
                return true;
            else
                return false;
        }
        if ((r2.width() == 0) || (r2.height() == 0))  //r2 is a line
        {
            if (r2.width() == 0)  //vertical line
            {
                if ((r2.left() < r1.left()) || (r2.left() > r1.right()))
                    return false;
                else
                {
                    if ((r2.top() < r1.top()) && (r2.bottom() < r1.top()))  //line above rect
                        return false;
                    if ((r2.top() > r1.bottom()) && (r2.bottom() > r1.bottom()))  //line below rect
                        return false;
                    return true;

                }
            }
            if (r2.height() == 0)  //horizontal line
            {
                if ((r2.top() < r1.top()) || (r2.top() > r2.bottom()))  //line y is out of box
                    return false;
                else
                {
                    if ((r2.left() < r1.left()) && (r2.right() < r1.left()))  //line is at the left of the box
                        return false;
                    if ((r2.left() > r1.right()) && (r2.right() > r2.right()))  //line is at the right of the box
                        return false;
                    return true;
                }
            }
        }
    }

    //if we are here, the case can be found by calling the same function again at inverter rects => little recursion
    return false;
}

QRectF glBoard::find_elements_in_selection()
{
    //finds all the elements included by the selection and returns the joined rect
    QRectF out;
    selectedLines.clear(); selectedLineIndexes.clear();
    for (int i = 0; i < scene.lines.count(); i++)
        if (rects_intersect(scene.lines[i].rect, selectArea) == true)
        {
            selectedLines.append(scene.lines[i]);
            selectedLineIndexes.append(i);
            out = out.united(scene.lines[i].rect);
        }

    return out;
}

mouse_location_enum glBoard::get_mouse_location(QPointF pos)
{
    //this doesn't handle the toolbox at the moment
    if (selected == true)
    {
        QRectF r = fromQRectFtoQRect(adjust_selection_area());
        int margin = 10;  //margin to determine the edge
        if (r.contains(pos))
        {
            QRectF m = r.marginsRemoved(QMargins(margin, margin, margin, margin));
            if (m.contains(pos))
                return SELECTION_MOVE;
            if ((pos.y() > r.top()) && (pos.y() < r.top() + margin))
            {
                if ((pos.x() > r.left()) && (pos.x() < r.left() + margin))
                    return SELECTION_TOPLEFT;
                if ((pos.x() > r.right() - margin) && (pos.x() < r.right()))
                    return SELECTION_TOPRIGHT;
                else
                    return SELECTION_UP;
            }
            if ((pos.y() > r.bottom() - margin) && (pos.y() < r.bottom()))
            {
                if ((pos.x() > r.left()) && (pos.x() < r.left() + margin))
                    return SELECTION_BOTTOMLEFT;
                if ((pos.x() > r.right() - margin) && (pos.x() < r.right()))
                    return SELECTION_BOTTOMRIGHT;
                else
                    return SELECTION_DOWN;
            }
            if ((pos.x() > r.left()) && (pos.x() < r.left() + margin))
                return SELECTION_LEFT;
            if ((pos.x() > r.right() - margin) && (pos.x() < r.right()))
                return SELECTION_RIGHT;
        }
    }
    return SCENE;
}

void glBoard::move_selected_lines(QPointF diff, bool update_points)
{
    for (int i = 0; i < selectedLines.count(); i++)
    {
        selectedLines[i].zoom_shift_applied = false;
        selectedLines[i].zoom_shift_transf.translate(diff.x(), diff.y());
        selectedLines[i].rect.setTopLeft(selectedLines[i].rect.topLeft() + diff);

        scene.lines[selectedLineIndexes[i]].rect = selectedLines[i].rect;
        scene.lines[selectedLineIndexes[i]].zoom_shift_applied = selectedLines[i].zoom_shift_applied;
        scene.lines[selectedLineIndexes[i]].zoom_shift_transf = selectedLines[i].zoom_shift_transf;
    }
    /*    for (int j = 0; j < selectedLines[i].points_x.count(); j++)
        {
            selectedLines[i].points_x[j] += diff.x();
            selectedLines[i].points_y[j] += diff.y();
        }
        selectedLines[i].rect = calculate_line_rect(selectedLines[i]);
        scene.lines[selectedLineIndexes[i]].points_x = selectedLines[i].points_x;
        scene.lines[selectedLineIndexes[i]].points_y = selectedLines[i].points_y;
        scene.lines[selectedLineIndexes[i]].rect = selectedLines[i].rect;
        for (int j = 0; j < selectedLines[i].rend_segments.count(); j++)
        {
            for (int k = 0; k < scene.lines[selectedLineIndexes[i]].rend_segments[j].points_x.count(); k++)
            {
                scene.lines[selectedLineIndexes[i]].rend_segments[j].points_x[k] += diff.x();
                scene.lines[selectedLineIndexes[i]].rend_segments[j].points_y[k] += diff.y();
            }
        }
        join_render_segments(&scene.lines[selectedLineIndexes[i]].rend_segments,
                &scene.lines[selectedLineIndexes[i]].rend_points_x, &scene.lines[selectedLineIndexes[i]].rend_points_y, true);
    //    generate_render_segments(&scene.lines[selectedLineIndexes[i]]);
    }*/
    //moves also the selection area accordingly
    selectArea.setTopLeft(selectArea.topLeft() + diff);
    selectArea.setBottomRight(selectArea.bottomRight() + diff);

    if (update_points == true)
    {
        for (int i = 0; i < selectedLines.count(); i++)
        {
            scene.lines[selectedLineIndexes[i]].zoom_shift_transf = selectedLines[i].zoom_shift_transf;
            for (int j = 0; j < selectedLines[i].points_x.count(); j++)
            {
                QPointF p;
                p.setX(selectedLines[i].points_x[j]); p.setY(selectedLines[i].points_y[j]);
                p = selectedLines[i].zoom_shift_transf * p;
                selectedLines[i].points_x[j] = p.x();
                selectedLines[i].points_y[j] = p.y();
            }
            selectedLines[i].rect = calculate_line_rect(selectedLines[i]);
            scene.lines[selectedLineIndexes[i]].points_x = selectedLines[i].points_x;
            scene.lines[selectedLineIndexes[i]].points_y = selectedLines[i].points_y;
            scene.lines[selectedLineIndexes[i]].rect = selectedLines[i].rect;
            for (int j = 0; j < selectedLines[i].rend_segments.count(); j++)
            {
                for (int k = 0; k < scene.lines[selectedLineIndexes[i]].rend_segments[j].points_x.count(); k++)
                {
                    QPointF p;
                    p.setX(scene.lines[selectedLineIndexes[i]].rend_segments[j].points_x[k]);
                    p.setY(scene.lines[selectedLineIndexes[i]].rend_segments[j].points_y[k]);
                    p = scene.lines[selectedLineIndexes[i]].zoom_shift_transf * p;
                    scene.lines[selectedLineIndexes[i]].rend_segments[j].points_x[k] = p.x();
                    scene.lines[selectedLineIndexes[i]].rend_segments[j].points_y[k] = p.y();
                }
            }
            join_render_segments(&scene.lines[selectedLineIndexes[i]].rend_segments,
                    &scene.lines[selectedLineIndexes[i]].rend_points_x, &scene.lines[selectedLineIndexes[i]].rend_points_y, true);
            scene.lines[selectedLineIndexes[i]].zoom_shift_applied = true;
            scene.lines[selectedLineIndexes[i]].zoom_shift_transf.setToIdentity();
            selectedLines[i].zoom_shift_transf.setToIdentity();
        }
    }
}

void glBoard::resize_selected_line(QPointF diff, mouse_location_enum edge, bool update_points)
{
    QRectF newselectArea;

    newselectArea = selectArea;

    switch(edge)
    {
    case SELECTION_TOPLEFT:
        newselectArea.setTopLeft(selectArea.topLeft() + diff);
        break;
    case SELECTION_BOTTOMLEFT:
        newselectArea.setBottom(selectArea.bottom() + diff.y());
        newselectArea.setLeft(selectArea.left() + diff.x());
        break;
    case SELECTION_TOPRIGHT:
        newselectArea.setTop(selectArea.top() + diff.y());
        newselectArea.setRight(selectArea.right() + diff.x());
        break;
    case SELECTION_BOTTOMRIGHT:
        newselectArea.setBottom(selectArea.bottom() + diff.y());
        newselectArea.setRight(selectArea.right() + diff.x());
        break;
    case SELECTION_UP:
        newselectArea.setTop(selectArea.top() + diff.y());
        break;
    case SELECTION_DOWN:
        newselectArea.setBottom(selectArea.bottom() + diff.y());
        break;
    case SELECTION_RIGHT:
        newselectArea.setRight(selectArea.right() + diff.x());
        break;
    case SELECTION_LEFT:
        newselectArea.setLeft(selectArea.left() + diff.x());
        break;
    default:
        return;
    }

    QMatrix4x4 mat;
    mat.setToIdentity();
    mat.translate(QVector3D(selectArea.topLeft()));
    mat.scale(newselectArea.width()/selectArea.width(), newselectArea.height()/selectArea.height());
    mat.translate(QVector3D(-selectArea.topLeft() + (newselectArea.topLeft() - selectArea.topLeft())));

    for (int i = 0; i < selectedLines.count(); i++)
    {
        selectedLines[i].zoom_shift_applied = false;
        selectedLines[i].zoom_shift_transf *= mat;
        scene.lines[selectedLineIndexes[i]].zoom_shift_applied = false;
        scene.lines[selectedLineIndexes[i]].zoom_shift_transf = selectedLines[i].zoom_shift_transf;
/*        for (int j = 0; j < selectedLines[i].points_x.count(); j++)
        {
            QPointF p(selectedLines[i].points_x[j], selectedLines[i].points_y[j]);
            p = mat.map(p);
            selectedLines[i].points_x[j] = p.x();
            selectedLines[i].points_y[j] = p.y();
            selectedLines[i].rect = calculate_line_rect(selectedLines[i]);
        }
        scene.lines[selectedLineIndexes[i]].points_x = selectedLines[i].points_x;
        scene.lines[selectedLineIndexes[i]].points_y = selectedLines[i].points_y;
        scene.lines[selectedLineIndexes[i]].rect = selectedLines[i].rect;
        generate_render_segments(&scene.lines[selectedLineIndexes[i]]);*/
    }

    selectArea = newselectArea;

    if (update_points == true)
    {
        for (int i = 0; i < selectedLines.count(); i++)
        {
            for (int j = 0; j < selectedLines[i].points_x.count(); j++)
            {
                QPointF p(selectedLines[i].points_x[j], selectedLines[i].points_y[j]);
                p = selectedLines[i].zoom_shift_transf.map(p);
                selectedLines[i].points_x[j] = p.x();
                selectedLines[i].points_y[j] = p.y();
                selectedLines[i].rect = calculate_line_rect(selectedLines[i]);
            }
            scene.lines[selectedLineIndexes[i]].points_x = selectedLines[i].points_x;
            scene.lines[selectedLineIndexes[i]].points_y = selectedLines[i].points_y;
            scene.lines[selectedLineIndexes[i]].rect = selectedLines[i].rect;
            scene.lines[selectedLineIndexes[i]].zoom_shift_applied = true;
            scene.lines[selectedLineIndexes[i]].zoom_shift_transf.setToIdentity();
            generate_render_segments(&scene.lines[selectedLineIndexes[i]]);
            selectedLines[i].rend_segments = scene.lines[selectedLineIndexes[i]].rend_segments;
            selectedLines[i].rend_points_x = scene.lines[selectedLineIndexes[i]].rend_points_x;
            selectedLines[i].rend_points_y = scene.lines[selectedLineIndexes[i]].rend_points_y;
            selectedLines[i].zoom_shift_transf.setToIdentity();
        }
    }
}

QRectF glBoard::map_to_widget(QRectF r)
{
    QRectF out;
    out = r;
    out.setTopLeft(transfMatrix.map(out.topLeft()));
    out.setBottomRight(transfMatrix.map(out.bottomRight()));
    return out;
}

QRectF glBoard::map_to_scene(QRectF r)
{
    QRectF out;
    out = r;
    out.setTopLeft(transfMatrixInv.map(out.topLeft()));
    out.setBottomRight(transfMatrixInv.map(out.bottomRight()));
    return out;
}

QRectF glBoard::adjust_selection_area()
{
    QSize minSize = QSize(50,50);  //minimum size on the select area on screen

    //select area is in scene coordinates => map to screen
    QRectF s = map_to_widget(selectArea);
    float dx, dy;
    dx = 20 * scale;
    dy = 20 * scale;
    s.setLeft(s.left() - dx);
    s.setRight(s.right() + dx);
    s.setTop(s.top() - dy);
    s.setBottom(s.bottom() + dy);

    //check if minimum size is respected
    if (s.width() < minSize.width())
    {
        dx = (minSize.width() - s.width()) / 2.0;
        s.setLeft(s.left() - dx); s.setRight(s.right() + dx);
    }
    if (s.height() < minSize.height())
    {
        dy = (minSize.height() - s.height()) / 2.0;
        s.setTop(s.top() - dy); s.setBottom(s.bottom() + dy);
    }

    //move selection area back to scene
    return s;
}

void glBoard::delete_selected_lines()
{
    if (selectedLineIndexes.count() == 0)
        return;
    int i = 0;
    QVector<bool> todelete; todelete.resize(scene.lines.count()); todelete.fill(false);
    for (i = 0; i < scene.lines.count(); i++)
    {
        bool found = false;
        for (int j = 0; j < selectedLineIndexes.count(); j++)
            if (i == selectedLineIndexes[j])
                found = true;
        todelete[i] = found;
    }
    i = 0;
    while (i < scene.lines.count())
    {
        if (todelete[i] == true)
        {
            scene.lines.removeAt(i);
            todelete.removeAt(i);
            i--;
        }
        i++;
    }
    selectedLines.clear();
    selectedLineIndexes.clear();
    selected = false;
}

QRect glBoard::fromQRectFtoQRect(QRectF r)
{
    QRect o;
    o.setLeft((int)r.left());
    o.setTop((int)r.top());
    o.setBottom((int)r.bottom());
    o.setRight((int)r.right());
    return o;
}

QSize glBoard::fromQSizeFtoQSize(QSizeF s)
{
    QSize o;
    o.setWidth(s.width());
    o.setHeight(s.height());
    return o;
}

QRect glBoard::checkQRect(QRect r)
{
    QRect o;
    o = r;
    if ((r.width() < 0) && (r.height() < 0))
    {
        o.setTopLeft(r.bottomRight());
        o.setBottomRight(r.topLeft());
    }
    if (o.width() < 0)
    {
        o.setLeft(r.right());
        o.setRight(r.left());
        o.setTop(r.top());
        o.setBottom(r.bottom());
    }
    if (o.height() < 0)
    {
        o.setBottom(r.top());
        o.setTop(r.bottom());
        o.setLeft(r.left());
        o.setRight(r.right());
    }
    return o;
}

void glBoard::add_to_history(operation type)
{
    //a new scene has been created
    //check if this is the last one, otherwise, insert it and delete all the other ones in front
    if (history_index < (history.count() - 1))
        history.remove(history_index + 1, history.count() - history_index - 1);
    history_freeze tmp;
    tmp.event_type = type;
    tmp.scene = scene;
    tmp.selectArea = selectArea;
    tmp.selectedLines = selectedLines;
    tmp.selectedLineIndexes = selectedLineIndexes;
    tmp.selected = selected;
    history.append(tmp);
    history_index = history.count() - 1;
}

void glBoard::restore_history_back()
{
    history_index--;
    if (history_index < 0)
        history_index = 0;
    scene = history[history_index].scene;
    selectArea = history[history_index].selectArea;
    selectedLineIndexes = history[history_index].selectedLineIndexes;
    selectedLines = history[history_index].selectedLines;
    selected = history[history_index].selected;

    repaint();
}

void glBoard::restore_history_forward()
{
    history_index++;
    if (history_index > history.count() - 1)
        history_index = history.count() - 1;
    scene = history[history_index].scene;
    selectArea = history[history_index].selectArea;
    selectedLineIndexes = history[history_index].selectedLineIndexes;
    selectedLines = history[history_index].selectedLines;
    selected = history[history_index].selected;

    repaint();
}

QString glBoard::get_tool_name(operation t)
{
    switch (t) {
    case NONE_OP:
        return "None";
        break;
    case PEN_OP:
        return "Pen";
        break;
    case ERASE_OP:
        return "Erase";
        break;
    case MOVE_OP:
        return "Move";
        break;
    case RESIZE_OP:
        return "Resize";
        break;
    case SELECT_OP:
        return "Select";
        break;
    case PASTE_OP:
        return "Paste";
        break;
    default:
        return "Non found";
        break;
    }
}

void glBoard::get_undo_redo_strings(QString &undo, QString &redo, bool &undo_active, bool &redo_active)
{
    if (history_index > 0)  //that is there is a possible undo operation
    {
        undo_active = true;
        undo = get_tool_name(history[history_index].event_type);
    }
    else
        undo_active = false;

    if (history_index < history.count() - 1)  //that is there is a possible redo operation
    {
        redo_active = true;
        redo = get_tool_name(history[history_index + 1].event_type);
    }
    else
        redo_active = false;
}

segment_t glBoard::create_segment(QPointF p1, QPointF p2, float line_size, float pressure1, float pressure2, bool apply_pressure, bool rounded)
{
    float alfa;
    float delta_x, delta_y;

    delta_x = p2.x() - p1.x();
    delta_y = p2.y() - p1.y();
    if (delta_x == 0)
        alfa = 0;
    else
        alfa = 1.5707963f - qAtan2(delta_y, delta_x);

    float dx0, dy0, dx1, dy1; float cA, sA; cA = qCos(alfa); sA = qSin(alfa);
    float LW0, LW1;
    if (apply_pressure == true)
    {
        LW0 = line_size + (line_size * pressure1 * 2.0f);
        LW1 = line_size + (line_size * pressure2 * 2.0f);
    }
    else
    {
        LW0 = line_size;
        LW1 = line_size;
    }

    if (apply_pressure == false)
    {
        dx0 = line_size * cA;
        dy0 = line_size * sA;
        dx1 = line_size * cA;
        dy1 = line_size * sA;
    }
    else
    {

        dx0 = LW0 * cA;
        dy0 = LW0 * sA;
        dx1 = LW1 * cA;
        dy1 = LW1 * sA;
    }

    segment_t seg;

    int N;
    float step;
    N = 10; step = 360.0f / (float)N;
    float radius;

    if (rounded == true)
    {
        //Create round centered on P1
        radius = LW0;
        for (int i = 0; i < N; i++)
        {
            //center point
            seg.points_x.append(p1.x()); seg.points_y.append(p1.y());
            //first point
            seg.points_x.append((radius * qCos(i*step/360.0 * 2.0 * 3.14159265359)) + p1.x());
            seg.points_y.append((radius * qSin(i*step/360.0 * 2.0 * 3.14159265359)) + p1.y());
            //second point
            seg.points_x.append((radius * qCos((i+1)*step/360.0 * 2.0 * 3.14159265359)) + p1.x());
            seg.points_y.append((radius * qSin((i+1)*step/360.0 * 2.0 * 3.14159265359)) + p1.y());
        }
        seg.points_x.append(p1.x()); seg.points_y.append(p1.y());
    }

    seg.points_x.append(p1.x() - dx0); seg.points_y.append(p1.y() + dy0);
    seg.points_x.append(p1.x() + dx0); seg.points_y.append(p1.y() - dy0);
    seg.points_x.append(p2.x() - dx1); seg.points_y.append(p2.y() + dy1);
    seg.points_x.append(p2.x() + dx1); seg.points_y.append(p2.y() - dy1);

    if (rounded == true)
    {
        //Create round centered on P2
        radius = LW1;
        for (int i = 0; i < N; i++)
        {
            //center point
            seg.points_x.append(p2.x()); seg.points_y.append(p2.y());
            //first point
            seg.points_x.append((radius * qCos(i*step/360.0 * 2.0 * 3.14159265359)) + p2.x());
            seg.points_y.append((radius * qSin(i*step/360.0 * 2.0 * 3.14159265359)) + p2.y());
            //second point
            seg.points_x.append((radius * qCos((i+1)*step/360.0 * 2.0 * 3.14159265359)) + p2.x());
            seg.points_y.append((radius * qSin((i+1)*step/360.0 * 2.0 * 3.14159265359)) + p2.y());
        }
    }

    return seg;
}

void glBoard::join_render_segments(QVector<segment_t> *segs, QVector<float> *x, QVector<float> *y, bool clear)
{
    if (clear == true)
    {
        x->clear(); y->clear();
    }
    for (int i = 0; i < segs->count(); i++)
    {
        for (int j = 0; j < segs->at(i).points_x.count(); j++)
        {
            x->append(segs->at(i).points_x[j]);
            y->append(segs->at(i).points_y[j]);
        }
    }
}

void glBoard::join_render_segments(QVector<segment_t> segs, QVector<float> *x, QVector<float> *y, bool clear)
{
    if (clear == true)
    {
        x->clear(); y->clear();
    }
    for (int i = 0; i < segs.count(); i++)
    {
        for (int j = 0; j < segs[i].points_x.count(); j++)
        {
            x->append(segs[i].points_x[j]);
            y->append(segs[i].points_y[j]);
        }
    }
}

void glBoard::generate_render_segments(line_t *input)
{
    if (input->points_x.count() <= 1)
        return;
    input->rend_segments.clear();
    QPointF p1, p2;
    for (int i = 1; i < input->points_x.count(); i++)
    {
        p1.setX(input->points_x[i-1]); p1.setY(input->points_y[i-1]);
        p2.setX(input->points_x[i]); p2.setY(input->points_y[i]);
        input->rend_segments.append(create_segment(p1, p2, input->line_size, input->pressures[i-1], input->pressures[i], input->apply_pressure, true));
    }
    join_render_segments(&input->rend_segments, &input->rend_points_x, &input->rend_points_y, true);
}

void glBoard::pen_function(QTabletEvent *event)
{
    QPointF pos;
#if QT_VERSION_MAJOR == 6
    pos = event->position();
#else
    pos = event->posF();
#endif
    if ((event->type() == QTabletEvent::TabletPress) && (event->buttons() == Qt::LeftButton) && (current_tool == PEN) && (event->modifiers() == Qt::NoModifier))
    {
        //creates a new line and append it to the scene
        busy_on_scene = true;
        line_type newline;
        newline.apply_pressure = apply_pressure;
        newline.colors.append(current_color);
        newline.line_size = current_line_size;
        QPointF p = transfMatrixInv.map(pos);
        newline.points_x.append(p.x());
        newline.points_y.append(p.y());
        newline.pressures.append(event->pressure());
        newline.times.append(0);
        newline.zoom_shift_transf.setToIdentity();
        newline.zoom_shift_applied = true;

        nsTimer.restart();

        scene.lines.append(newline);
    }
    if ((event->type() == QTabletEvent::TabletMove) && (event->buttons() == Qt::LeftButton) && (current_tool == PEN) && (event->modifiers() == Qt::NoModifier))
    {
        scene.lines[scene.lines.count() - 1].colors.append(current_color);
        QPointF p = transfMatrixInv.map(pos);
        QPointF p1, p2;
        line_t *line; line = &scene.lines[scene.lines.count() - 1];
        line->points_x.append(p.x());
        line->points_y.append(p.y());
        line->pressures.append(event->pressure());
        line->times.append(nsTimer.nsecsElapsed());
        p1.setX(line->points_x[line->points_x.count() - 2]); p1.setY(line->points_y[line->points_y.count() - 2]);
        p2 = p;
        line->rend_segments.append(create_segment(p1, p2, line->line_size, line->pressures[line->pressures.count() - 2], line->pressures[line->pressures.count() - 1], line->apply_pressure, true));
        join_render_segments(line->rend_segments.mid(line->rend_segments.count() - 1, 1), &line->rend_points_x, &line->rend_points_y, false);
    }
    if ((event->type() == QTabletEvent::TabletRelease) && (current_tool == PEN) && (event->modifiers() == Qt::NoModifier))
    {
        busy_on_scene = false;
        scene.lines[scene.lines.count() - 1].rect = calculate_line_rect(scene.lines[scene.lines.count() - 1]);
        add_to_history(PEN_OP);
    }
}

void glBoard::erase_function(QTabletEvent *event)
{
    QPointF pos;
#if QT_VERSION_MAJOR == 6
    pos = event->position();
#else
    pos = event->posF();
#endif
    //STYLUS ERASING
    if (((event->type() == QTabletEvent::TabletMove) || (event->type() == QTabletEvent::TabletPress)) && ((event->buttons() & Qt::LeftButton) != 0) && (current_tool == ERASE)
            && ((event->modifiers() == Qt::NoModifier) || (event->modifiers() == (Qt::ControlModifier | Qt::MetaModifier))))
    {
        busy_on_scene = true;
        if (event->modifiers() == Qt::NoModifier) //default erase mode
        {
            if (properties.eraser_default_mode == false)
                erase_points(pos);
            else
                erase_lines(pos);
        }
        if (event->modifiers() == (Qt::ControlModifier | Qt::MetaModifier))  //not default erase mode
        {
            if (properties.eraser_default_mode == false)
                erase_lines(pos);
            else
                erase_points(pos);
        }
    }
    if ((event->type() == QTabletEvent::TabletRelease) && (current_tool == ERASE)
        && ((event->modifiers() == Qt::NoModifier) || (event->modifiers() == (Qt::ControlModifier | Qt::MetaModifier))))
    {
        busy_on_scene = false;
        add_to_history(ERASE_OP);
    }
}

void glBoard::select_function(QTabletEvent *event)
{
    QPointF pos;
#if QT_VERSION_MAJOR == 6
    pos = event->position();
#else
    pos = event->posF();
#endif
    //STYLUS SELECTING
    if ((event->type() == QTabletEvent::TabletPress) &&
            ((event->buttons() &= Qt::LeftButton) != 0) && (current_tool == SELECT) && (event->modifiers() == Qt::NoModifier))
    {
        if ((selecting == false) && (selected == false))
        {
            selecting = true; selected = false;
            selectStartPoint = transfMatrixInv.map(pos);
            selectArea.setTopLeft(selectStartPoint); selectArea.setBottomRight(selectStartPoint);
        }
        if ((selecting == false) && (selected == true))
        {
            if (mouse_loc == SCENE)
            {
                selecting = true; selected = false;
                selectStartPoint = transfMatrixInv.map(pos);
                selectArea.setTopLeft(selectStartPoint); selectArea.setBottomRight(selectStartPoint);
            }
            if (mouse_loc == SELECTION_MOVE)
            {
                selectionStartMove = transfMatrixInv.map(pos);
                selectionMoving = true;
            }
            //if we are here, toolbox can't be, scene and selection move already processed => so
            if ((mouse_loc == SELECTION_BOTTOMLEFT) || (mouse_loc == SELECTION_BOTTOMRIGHT) || (mouse_loc == SELECTION_DOWN) || (mouse_loc == SELECTION_LEFT) ||
                    (mouse_loc == SELECTION_RIGHT) || (mouse_loc == SELECTION_TOPLEFT) || (mouse_loc == SELECTION_TOPRIGHT) || (mouse_loc == SELECTION_UP))
            {
                selectionResizingType = mouse_loc;
                selectionResizing = true;
                selectionResizeStart = transfMatrixInv.map(pos);
            }
        }
    }
    if ((event->type() == QTabletEvent::TabletMove) && ((event->buttons() &= Qt::LeftButton) != 0) && (current_tool == SELECT) && (event->modifiers() == Qt::NoModifier))
    {
        busy_on_scene = true;
        if (selecting == true)
        {
            selectArea.setBottomRight(transfMatrixInv.map(pos));
        }
        if ((selecting == false) && (selected == true) && (selectionMoving == true))
        {
            QPointF newpoint = transfMatrixInv.map(pos);
            QPointF diff = newpoint - selectionStartMove;
            selectionStartMove = newpoint;
            move_selected_lines(diff, false);
        }
        if ((selecting == false) && (selected == true) && (selectionResizing == true))
        {
            QPointF newpoint = transfMatrixInv.map(pos);
            QPointF diff = newpoint - selectionResizeStart;
            selectionResizeStart = newpoint;
            resize_selected_line(diff, selectionResizingType, false);
        }
    }
    if ((event->type() == QTabletEvent::TabletRelease) && (event->button() == Qt::LeftButton) && (current_tool == SELECT) && (event->modifiers() == Qt::NoModifier))
    {
        busy_on_scene = false;
        //we check if there are any elements in the selection area
        if (selecting == true)
        {
            QRectF s = find_elements_in_selection();
            if (selectedLines.count() != 0)
            {
                selectArea = s;
                selected = true; selecting = false;
                add_to_history(SELECT_OP);
            }
            else
            {
                selected = false; selecting = false;
            }
        }
        if (selectionMoving == true)
        {
            selectionMoving = false;
            move_selected_lines(QPointF(0,0), true);
            add_to_history(MOVE_OP);
        }
        if (selectionResizing == true)
        {
            selectionResizing = false;
            resize_selected_line(QPointF(0,0), selectionResizingType, true);
            add_to_history(RESIZE_OP);
        }
    }
}

void glBoard::zoom_shift_function(QTabletEvent *event)
{
    QPointF pos;
#if QT_VERSION_MAJOR == 6
    pos = event->position();
#else
    pos = event->posF();
#endif
    //STYLUS ZOOM AND SHIFTING
    if ((event->type() == QTabletEvent::TabletMove) && (event->buttons() == Qt::LeftButton) && (event->modifiers() == Qt::ControlModifier))
    {
        busy_on_scene = true;
        //bring the mouse position to the center
        QPointF offset;
        offset = transfMatrixInv.map(pos);
        //scale
        scale = transfMatrix.data()[0];
        QVector4D diff = 1.0f / scale * QVector4D(pos - startDrag);
        float tmp = (float)1.0f + ((float)diff.y() / 500.0f);
        if (tmp <= 0.0f)
            tmp = 0.1f;
        if (((scale < 0.25f) && (tmp < 1.0f)) || ((scale > 10.0f) && (tmp > 1.0f)))
            return;  //no more scaling further
        transfMatrix.translate(QVector3D(offset));
        transfMatrix.scale(tmp);  //this high value to have a slow zoom
        scale = transfMatrix.data()[0];
        //bring the center back
        transfMatrix.translate(-QVector3D(offset)); transfMatrixInv = transfMatrix.inverted();
        textpop->setText(QString::number((int)(scale * 100)) + "%");
        textpop->popup();
        startDrag = pos;
        updategrid = true;
    }
    if ((event->type() == QTabletEvent::TabletRelease) && (event->buttons() == Qt::LeftButton) && (event->modifiers() == Qt::ControlModifier))
        busy_on_scene = false;
    if ((event->type() == QTabletEvent::TabletPress) && (event->button() == Qt::LeftButton) && ((event->modifiers() == Qt::ShiftModifier) || (event->modifiers() == Qt::ControlModifier)))
    {
        setFocus();
        startDrag = current_mouse_position;
    }
    if ((event->type() == QTabletEvent::TabletMove) && (event->buttons() == Qt::LeftButton) && (event->modifiers() == Qt::ShiftModifier))
    {
        busy_on_scene = true;
        scale = transfMatrix.data()[0];
        QVector4D diff = 1.0f / scale * QVector4D(pos - startDrag);
        startDrag = pos;
        transfMatrix.translate(QVector3D(diff));
        transfMatrixInv = transfMatrix.inverted();
        updategrid = true;
    }
    if ((event->type() == QTabletEvent::TabletRelease) && (event->buttons() == Qt::LeftButton) && (event->modifiers() == Qt::ShiftModifier))
        busy_on_scene = false;
    if ((event->type() == QTabletEvent::TabletPress) && (event->buttons() == Qt::LeftButton) && (event->modifiers() == (Qt::ShiftModifier | Qt::ControlModifier)))
    {
        transfMatrix.setToIdentity(); transfMatrixInv.setToIdentity(); scale = 1.0f;
        textpop->setText(QString::number((int)(scale * 100)) + "%");
        textpop->popup();
        updategrid = true;
    }
}

void glBoard::zoom_shift_function(QWheelEvent *event)
{
    if (event->modifiers() == Qt::ControlModifier)
    {
        //bring the mouse position to the center
        QPointF offset;
        offset = transfMatrixInv.map(event->position());
        //scale
        float tmp = (float)1.0f + ((float)event->angleDelta().y() / 1500.0f);
        if (tmp <= 0.0f)
            tmp = 0.1f;
        if (((scale < 0.25f) && (tmp < 1.0f)) || ((scale > 10.0f) && (tmp > 1.0f)))
            return;  //no more scaling further
        transfMatrix.translate(QVector3D(offset));
        transfMatrix.scale(tmp);  //this high value to have a slow zoom
        scale = transfMatrix.data()[0];
        //bring the center back
        transfMatrix.translate(-QVector3D(offset)); transfMatrixInv = transfMatrix.inverted();
        textpop->setText(QString::number((int)(scale * 100)) + "%");
        textpop->popup();
        updategrid = true;
    }
    else{
        if (event->modifiers() == Qt::ShiftModifier)
            transfMatrix.translate(1.0f / transfMatrix.data()[0] * QVector3D(event->angleDelta().y(), 0, 0));
        else
            transfMatrix.translate(1.0f / transfMatrix.data()[0] * QVector3D(event->angleDelta()));
        transfMatrixInv = transfMatrix.inverted();
        updategrid = true;
    }
}
